// ignore_for_file: constant_identifier_names
class Routes {
  static const String INITIAL = '/initial';
  static const String SIGN_UP = '/sign-up';
  static const String LOGIN_FORM = '/login';
  static const String SIGN_UP_NUMBER = '/sign-up/number';
  static const String TAB_BAR = '/tab-bar';
  static const String SIGN_UP_FORM = '/sign-up/sign-up-form';
  static const String CHAT = '/chat';
  static const String ACCOUNT_DETAIL = '/account-detail';
}
