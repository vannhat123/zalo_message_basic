// ignore_for_file: constant_identifier_names
class ImageAsset{
  static const String ICON_MESSAGE = 'assets/icon_zalo/Chat.png';
  static const String ICON_MESSAGE_SHOW = 'assets/icon_zalo_show/Chat (1).png';
  static const String ICON_CONTACT = 'assets/icon_zalo/Phonebook.png';
  static const String ICON_CONTACT_SHOW = 'assets/icon_zalo_show/Phonebook (1).png';
  static const String ICON_DISCOVER = 'assets/icon_zalo/Category.png';
  static const String ICON_DISCOVER_SHOW = 'assets/icon_zalo_show/Category (1).png';
  static const String ICON_DIARY= 'assets/icon_zalo_show/Clock (1).png';
  static const String ICON_DIARY_SHOW= 'assets/icon_zalo/Clock.png';
  static const String ICON_PROFILE = 'assets/icon_zalo/Infomation.png';
  static const String ICON_PROFILE_SHOW = 'assets/icon_zalo_show/Infomation (1).png';
  static const String ICON_FOUR_REC = 'assets/icon_zalo/Vector.png';
  static const String ICON_ADD = 'assets/icon_zalo/add.png';
  static const String AVATAR_MESSAGE_MEDIA_BOX = 'assets/slider/boaa.jpg';
  static const String ICON_ADD_FRIEND = 'assets/icon_zalo/add_friend.png';
  static const String ICON_CHANGE = 'assets/icon_zalo/change.png';
  static const String ICON_NOTIFICATION = 'assets/icon_zalo/notification.png';
  static const String ICON_SETTING = 'assets/icon_zalo/setting.png';
  static const String ICON_CONTACT_ADD_FRIEND = 'assets/icon_zalo/2176623.png';
  static const String ICON_CONTACT_MACHINE = 'assets/icon_zalo/563898.png';
  static const String IMAGE_SLIDER1 = 'assets/slider/IMG_6939.JPG';
  static const String IMAGE_SLIDER2 = 'assets/slider/IMG_6958.JPG';
  static const String IMAGE_SLIDER3 = 'assets/slider/IMG_6968.JPG';
  static const String IMAGE_SLIDER4 = 'assets/slider/Artboard 10@4x.png';
  static const String IMAGE_SLIDER5 = 'assets/slider/Artboard 20@4x.png';
  static const String IMAGE_GAME_TIEN_LEN = 'assets/icon_zalo/512x512bb.jpg';
  static const String IMAGE_GAME_KHO_GAME= 'assets/icon_zalo/3011878837.webp';
  static const String IMAGE_GAME_VO_LAM = 'assets/icon_zalo/silkroad-online.jpg';
  static const String IMAGE_GAME_TA_LA = 'assets/icon_zalo/silkroad1.jpg';
  static const String IMAGE_GAME_SILK_ROAD = 'assets/icon_zalo/silkroad1.jpg';
  static const String IMAGE_ACCOUNT_DETAIL_BACKGROUND = 'assets/nhat_hero/z4027353202781_da1aca06b23dc8dc03ed4d05747a4040.jpg';
  static const String IMAGE_ACCOUNT_DETAIL_AVATAR = 'assets/nhat_hero/z4027353224988_e8a8d5679e4b8dad0c64ac4d12bc9f85.jpg';

}