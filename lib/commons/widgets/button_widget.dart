import 'package:flutter/material.dart';
import 'package:message_za_lo/commons/constants/theme.dart';
class ButtonWidget extends StatelessWidget {
  const ButtonWidget({
    Key? key,
     this.width,
     this.height,
    required this.text,
    this.icon,
    required this.textStyle,
    required this.buttonColor,
    required this.onTap,
  }) : super(key: key);
  final double? width;
  final double? height;
  final String? text;
  final Icon? icon;
  final TextStyle? textStyle;
  final Color buttonColor;
  final VoidCallback onTap;

  @override
  Widget build(BuildContext context) {
    Size size = MediaQuery.of(context).size;
    return InkWell(
      onTap: onTap,
      child: Container(
        width: width ?? size.width *0.75,
        height: height ?? size.width* 0.12,
        alignment: Alignment.center,
        decoration: BoxDecoration(
            color: buttonColor,
            border: Border.all(color: DefaultTheme.GREY_BORDER_BUTTON,width: 0.1,
            ),
          borderRadius: const BorderRadius.all(Radius.circular(100)),
        ),
        child: Text(
          text != null ? text! : '',
          style: textStyle,
          textAlign: TextAlign.center,
        ),
      ),
    );
  }
}
