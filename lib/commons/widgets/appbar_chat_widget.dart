import 'package:flutter/material.dart';
import 'package:get/get.dart';
import 'package:message_za_lo/commons/constants/numeral.dart';
import 'package:message_za_lo/commons/constants/theme.dart';
import 'package:message_za_lo/features/home/tabbar/controller/tabbar_controller.dart';

class AppBarChatWidget extends StatelessWidget implements PreferredSizeWidget {
  final String titlePrimary;
  final String titleSub;

  const AppBarChatWidget({Key? key, required this.titlePrimary,required this.titleSub})
      : preferredSize = const Size.fromHeight(Numeral.APPBAR_HEIGHT),
        super(key: key);
  @override
  final Size preferredSize;

  @override
  Widget build(BuildContext context) {
    return AppBar(
      backgroundColor: DefaultTheme.THEME_COLOR,
      elevation: 0,
      leading: IconButton(
        padding: const EdgeInsets.all(0),
        icon: const Icon(
          Icons.arrow_back_ios_new,
          size: Numeral.SIZE_ICON_BACK,
        ),
        onPressed: () {
          Navigator.of(context).pop();
        },
      ),
      title: Container(
        alignment: Alignment.centerLeft,
        child: Row(
          crossAxisAlignment: CrossAxisAlignment.center,
          children: [
            Column(
              crossAxisAlignment: CrossAxisAlignment.start,
              children: [
                Text(
                  titlePrimary,
                  style: DefaultTheme.TEXT_STYLE_APPBAR_NORMAL_WHITE,
                ),
                Container(
                  margin: EdgeInsets.symmetric(vertical: 2),
                  child: Text(
                    titleSub,
                    style: DefaultTheme.TEXT_STYLE_APPBAR_SUB_WHITE,
                  ),
                ),
              ],
            ),
            const Spacer(),
            GetBuilder<TabBarController>(
                init: TabBarController(),
                builder: (controller){
                  return Row(
                    crossAxisAlignment: CrossAxisAlignment.center,
                    children: const [
                      Icon(Icons.phone,size: 22,color: DefaultTheme.WHITE,),
                      SizedBox(width: 12,),
                      Icon(Icons.video_call,size: 30,color: DefaultTheme.WHITE,),
                      SizedBox(width: 12,),
                      Icon(Icons.view_list_outlined,size: 27,color: DefaultTheme.WHITE,),
                    ],
                  );
                }),
          ],
        ),
      ),
    );
  }
}
