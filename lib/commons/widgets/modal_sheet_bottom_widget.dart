import 'package:flutter/material.dart';
import 'package:image_picker/image_picker.dart';
import 'package:message_za_lo/commons/constants/numeral.dart';
import 'package:message_za_lo/commons/constants/theme.dart';
import 'package:message_za_lo/commons/utils/upload_image.dart';

class ModalSheetBottomWidget extends StatelessWidget {
  const ModalSheetBottomWidget(
      {Key? key,
      required this.size,
      required this.title1,
      required this.title2,
      required this.title3,
      required this.title4,
      required this.title5,
      required this.typeSelect1,
      required this.typeSelect2,
      required this.typeSelect3,
      required this.typeSelect4,
      required this.typeSelect5,required this.typeUpdateImage})
      : super(key: key);
  final Size size;
  final String title1;
  final String title2;
  final String title3;
  final String title4;
  final String title5;
  final int typeSelect1;
  final int typeSelect2;
  final int typeSelect3;
  final int typeSelect4;
  final int typeSelect5;
  final int typeUpdateImage;


  @override
  Widget build(BuildContext context) {
    return Container(
        margin: const EdgeInsets.only(left: 5, right: 5, bottom: 20),
        child: Column(
          mainAxisAlignment: MainAxisAlignment.end,
          children: [
            Expanded(
                child: GestureDetector(
              onTap: () => Navigator.pop(context),
            )),
            Column(
              children: [
                _boxComponentSnackBarWidget(
                    title1, 'first', typeSelect1, context),
                _boxComponentSnackBarWidget(
                    title2, 'mid-border', typeSelect2, context),
                if(title4.isNotEmpty && title5.isNotEmpty )_boxComponentSnackBarWidget(
                    title3, 'mid-not-border', typeSelect3, context),
                if(title4.isNotEmpty && title5.isEmpty )_boxComponentSnackBarWidget(
                    title3, 'mid-border-bottom', typeSelect3, context),
                if (title4.isNotEmpty && title5.isNotEmpty)
                  _boxComponentSnackBarWidget(
                      title4, 'mid-border', typeSelect4, context),
                 if (title4.isNotEmpty && title5.isEmpty)
                  _boxComponentSnackBarWidget(
                      title4, 'last', typeSelect4, context),
                if (title5.isNotEmpty)
                  _boxComponentSnackBarWidget(
                      title5, 'last', typeSelect5, context)
              ],
            ),
            _cancelComponentSnackBarWidget("Hủy", context)
          ],
        ));
  }

  Widget _boxComponentSnackBarWidget(
      String title, String type, int typeSelectImage, BuildContext context) {
    return ElevatedButton(
      style: ElevatedButton.styleFrom(
          primary: DefaultTheme.BACKGROUND_SNACK_BAR_SHOW_IMAGE,
          onPrimary: DefaultTheme.GREY.withOpacity(0.4),
          elevation: 0,
          shadowColor: DefaultTheme.BACKGROUND_SNACK_BAR_SHOW_IMAGE,
          tapTargetSize: MaterialTapTargetSize.shrinkWrap,
          padding: EdgeInsets.all(0),
          shape: (type == 'first')
              ? RoundedRectangleBorder(borderRadius: BorderRadius.vertical(top: Radius.circular(10)),)
                  : (type == 'mid-border' ||type == 'mid-not-border' || type == 'mid-border-bottom')
                      ? RoundedRectangleBorder() : RoundedRectangleBorder(borderRadius: BorderRadius.vertical(bottom: Radius.circular(10)))),
      onPressed: () async {
        if (typeSelectImage == Numeral.TYPE_SELECT_IMAGE_SHOW_AVATAR) {}
        if (typeSelectImage == Numeral.TYPE_SELECT_IMAGE_FROM_CAMERA) {}
        if (typeSelectImage == Numeral.TYPE_SELECT_IMAGE_FROM_MACHINE) {
          UploadImage.instance.getFromGalleryToApiShowScreen(ImagePicker(), context, size,typeUpdateImage);
        }
        if (typeSelectImage == Numeral.TYPE_SELECT_IMAGE_AVAILABLE) {}
      },
      child: Container(
        height: (type == 'first')
            ? Numeral.HEIGHT_SHOW_SNACK_BAR_IMAGE - 5
            : Numeral.HEIGHT_SHOW_SNACK_BAR_IMAGE,
        alignment: Alignment.center,
        decoration: (type == 'first')
            ? BoxDecoration(
                borderRadius: BorderRadius.vertical(top: Radius.circular(10)),
              )
            : (type == 'mid-border')
                ? BoxDecoration(
                    border: Border.symmetric(
                        horizontal: BorderSide(
                            width: 1,
                            color: DefaultTheme.GREY.withOpacity(
                                Numeral.OPACITY_COLOR_GREY * 1.5))))
                : (type == 'mid-border-bottom')
                    ? BoxDecoration(
                        border: Border(
                            bottom: BorderSide(
                                width: 1,
                                color: DefaultTheme.GREY.withOpacity(
                                    Numeral.OPACITY_COLOR_GREY * 1.5))))
                    : (type == 'mid-not-border')
                        ? BoxDecoration()
                        : BoxDecoration(
                            borderRadius: BorderRadius.vertical(
                                bottom: Radius.circular(10)),
                          ),
        child: Text(
          title,
          style: (type == 'first')
              ? DefaultTheme.TEXT_STYLE_SUB_SHOW_SNACK_IMAGE
              : DefaultTheme.TEXT_STYLE_PRIMARY_SHOW_SNACK_IMAGE,
        ),
      ),
    );
  }

  Widget _cancelComponentSnackBarWidget(String title, BuildContext context) {
    return Container(
      margin: EdgeInsets.only(top: 8),
      width: double.infinity,
      height: Numeral.HEIGHT_SHOW_SNACK_BAR_IMAGE,
      child: ElevatedButton(
        style: ElevatedButton.styleFrom(
            primary: DefaultTheme.WHITE,
            onPrimary: DefaultTheme.GREY,
            elevation: 0,
            shadowColor: DefaultTheme.BACKGROUND_SNACK_BAR_SHOW_IMAGE,
            tapTargetSize: MaterialTapTargetSize.shrinkWrap,
            padding: EdgeInsets.all(0),
            shape: RoundedRectangleBorder(
              borderRadius: BorderRadius.all(Radius.circular(10)),
            )),
        onPressed: () {
          Navigator.pop(context);
        },
        child:
            Text(title, style: DefaultTheme.TEXT_STYLE_CANCEL_SHOW_SNACK_IMAGE),
      ),
    );
  }
}
