import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:message_za_lo/commons/constants/numeral.dart';
import 'package:message_za_lo/commons/constants/theme.dart';

class DialogWidget {
  const DialogWidget._privateConstructor();

  static const DialogWidget _instance = DialogWidget._privateConstructor();

  static DialogWidget get instance => _instance;

  static bool _isShowDialog = false;

  get isShowDialog => _isShowDialog;

  openErrorDialog(
      {required BuildContext context,
      required String title,
      required String description,
      VoidCallback? voidCallback}) {
    Size size = MediaQuery.of(context).size;
    return showDialog(
      barrierDismissible: false,
      context: context,
      builder: (BuildContext context) {
        return Material(
          color: DefaultTheme.TRANSPARENT,
          child: Center(
            child: Container(
              width: size.width*0.7,
              height: 110,
              decoration: BoxDecoration(
                color: DefaultTheme.WHITE,
                borderRadius: BorderRadius.circular(10),
              ),
              child: Column(
                mainAxisAlignment: MainAxisAlignment.spaceAround,
                crossAxisAlignment: CrossAxisAlignment.center,
                children: <Widget>[
                  Expanded(
                    child: Container(
                      alignment: Alignment.center,
                      padding: EdgeInsets.symmetric(horizontal: 10),
                      child: Text(
                        description,
                        maxLines: 3,
                        textAlign: TextAlign.center,
                        style: DefaultTheme.TEXT_STYLE_DESCRIPTION_DIALOG,
                      ),
                    ),
                  ),
                  Container(
                    alignment: Alignment.center,
                    width: size.width*0.7,
                    height: 110 *0.4,
                    decoration: BoxDecoration(
                        border: Border(
                            top: BorderSide(width: 0.5,color: DefaultTheme.GREY.withOpacity(Numeral.OPACITY_COLOR_GREY_DIVIDE))
                        ),),
                    child: InkWell(
                      borderRadius: BorderRadius.circular(50),
                      onTap: () {
                        if (voidCallback != null) {
                          voidCallback();
                        }
                        Navigator.pop(context);
                        _isShowDialog = false;
                      },
                      child: const Text(
                        "Đóng",
                        style: DefaultTheme.TEXT_STYLE_CONFIRM_DIALOG_CANCEL,
                      ),
                    ),
                  ),
                ],
              ),
            ),
          ),
        );
      },
    );
  }

  openLoadingDialog({required BuildContext context}) {
    return showDialog(
      barrierDismissible: false,
      context: context,
      builder: (BuildContext context) {
        return const Material(
          color: DefaultTheme.TRANSPARENT,
          child: Center(
            child: SizedBox(
              width: 50,
              height: 50,
              child: CupertinoActivityIndicator(
                radius: 30,
                color: DefaultTheme.WHITE,
              ),
            ),
          ),
        );
      },
    );
  }

  openConfirmDialog(
      {required BuildContext context,
      required String title,
      String? titleCancel,
      String? titleOK,
      required String description,
      required Function function,
      Function? functionCancel}) {
    return showDialog(
      barrierDismissible: false,
      context: context,
      builder: (BuildContext context) {
       Size size =  MediaQuery.of(context).size;
        return Material(
          color: DefaultTheme.TRANSPARENT,
          child: Center(
            child: Container(
              padding: EdgeInsets.only(left:10,right: 10),
              width: size.width*0.7,
              height: 110,
              decoration: BoxDecoration(
                color: DefaultTheme.WHITE,
                borderRadius: BorderRadius.circular(10),
              ),
              child: Column(
                mainAxisAlignment: MainAxisAlignment.spaceAround,
                crossAxisAlignment: CrossAxisAlignment.center,
                children: <Widget>[
                  const Spacer(),
                  (title != '')
                      ? Text(
                          title,
                          maxLines: 1,
                          style: DefaultTheme.TEXT_SUB_BUTTON_BLACK,
                        )
                      : SizedBox(),
                  const Padding(padding: EdgeInsets.only(top: 10)),
                  Text(
                    description,
                    maxLines: 2,
                    textAlign: TextAlign.center,
                    style: DefaultTheme.TEXT_STYLE_DESCRIPTION_DIALOG,
                  ),
                  const Spacer(),
                  Container(
                    height: 110 *0.4,
                    decoration: BoxDecoration(
                      border: Border(
                        top: BorderSide(width: 0.5,color: DefaultTheme.GREY.withOpacity(Numeral.OPACITY_COLOR_GREY_DIVIDE))
                      )
                    ),
                    child: Row(
                      mainAxisAlignment: MainAxisAlignment.spaceBetween,
                      children: [
                        Expanded(
                          flex: 1,
                          child: InkWell(
                            onTap: () {
                              if (functionCancel != null) {
                                functionCancel();
                              }
                              Navigator.pop(context);
                            },
                            child: Container(
                              decoration: BoxDecoration(
                                border: Border(
                                  right: BorderSide(width: 0.5,color: DefaultTheme.GREY.withOpacity(Numeral.OPACITY_COLOR_GREY_DIVIDE))
                                )
                              ),
                              alignment: Alignment.center,
                              child: Text(
                                titleCancel ?? "Không",
                                style: DefaultTheme
                                    .TEXT_STYLE_CONFIRM_DIALOG_CANCEL,
                              ),
                            ),
                          ),
                        ),
                        Expanded(
                          flex: 1,
                          child: InkWell(
                            borderRadius: BorderRadius.circular(50),
                            onTap: () {
                              Navigator.pop(context);
                              function();
                            },
                            child: Center(
                              child: Text(
                                titleOK ?? "Có",
                                style: DefaultTheme
                                    .TEXT_STYLE_CONFIRM_DIALOG_AGREE,
                              ),
                            ),
                          ),
                        ),
                      ],
                    ),
                  ),
                ],
              ),
            ),
          ),
        );
      },
    );
  }

  void showToast(BuildContext context, String title) {
    final scaffold = ScaffoldMessenger.of(context);
    scaffold.showSnackBar(
      SnackBar(
       backgroundColor: DefaultTheme.WHITE,
        duration: const Duration(milliseconds: 2000),
        width: 200,
        padding: const EdgeInsets.symmetric(horizontal: 10, vertical: 10),
        behavior: SnackBarBehavior.floating,
        shape: RoundedRectangleBorder(
          borderRadius: BorderRadius.circular(10.0),
        ),
        content: Text(
          title,
          textAlign: TextAlign.center,
          style: DefaultTheme.TEXT_STYLE_TOAST_SUCCESS,
        ),
      ),
    );
  }

  void showToastConfirmMessage(
      BuildContext context, String title, String userName, int shopId,Size size) {
    final scaffold = ScaffoldMessenger.of(context);
    scaffold.showSnackBar(
      SnackBar(
        backgroundColor: DefaultTheme.GREY_BUTTON.withOpacity(Numeral.PADDING_LEFT_RIGHT_PRIMARY),
        duration: const Duration(milliseconds: 13000),
        width: size.width*0.4,
        padding: EdgeInsets.zero,
        behavior: SnackBarBehavior.floating,
        shape: RoundedRectangleBorder(
          borderRadius: BorderRadius.circular(10.0),
        ),
        content: InkWell(
          onTap: () {
            ScaffoldMessenger.of(context).hideCurrentSnackBar();
          },
          child: Container(
            decoration: BoxDecoration(
              borderRadius: BorderRadius.all(Radius.circular(10))
            ),
            padding: const EdgeInsets.symmetric(horizontal: 10, vertical: 10),
            width: size.width*0.4,
            child: Text(
              title,
              textAlign: TextAlign.center,
              style: DefaultTheme.TEXT_STYLE_SUB_BLACK_NORMAL,
            ),
          ),
        ),
      ),
    );
  }

  dismiss({required BuildContext context}) {
    _isShowDialog = false;
    Navigator.pop(context);
  }

  openDialog({required bool isShow}) {
    _isShowDialog = true;
  }
}
