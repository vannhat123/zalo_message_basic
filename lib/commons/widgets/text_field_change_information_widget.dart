import 'package:flutter/material.dart';
import 'package:message_za_lo/commons/constants/numeral.dart';
import 'package:message_za_lo/commons/constants/theme.dart';

class TextFieldChangeInformationWidget extends StatelessWidget {
  const TextFieldChangeInformationWidget(
      {Key? key,
      required this.controllerEditing,
      required this.hintText,
      required this.onChanged})
      : super(key: key);
  final TextEditingController controllerEditing;
  final String hintText;
  final Function(String) onChanged;

  @override
  Widget build(BuildContext context) {
    return SizedBox(
      height: Numeral.SIZE_HEIGHT_TEXT_FIELD_CHANGE_INFORMATION,
      child: TextField(
        onChanged: (value) => onChanged(value),
        controller: controllerEditing,
        style: DefaultTheme.TEXT_SUB_BUTTON_BLACK,
        decoration: InputDecoration(
          counterText: '',
          contentPadding: const EdgeInsets.only(right: 10),
          suffixIconConstraints: BoxConstraints(maxWidth: 35),
          suffixIcon: Icon(
            Icons.drive_file_rename_outline,
            color: DefaultTheme.GREY,
            size: 25,
          ),
          hintText: hintText,
          hintStyle: DefaultTheme.TEXT_STYLE_HINT_FIELD,
          disabledBorder: InputBorder.none,
          enabledBorder: UnderlineInputBorder(
            borderRadius: BorderRadius.zero,
            borderSide: BorderSide(
              width: 0.5,
              color: DefaultTheme.GREY
                  .withOpacity(Numeral.OPACITY_COLOR_GREY_DIVIDE),
            ),
          ),
          focusedBorder: const UnderlineInputBorder(
            borderRadius: BorderRadius.zero,
            borderSide: BorderSide(
              width: 1,
              color: DefaultTheme.THEME_COLOR,
            ),
          ),
        ),
      ),
    );
  }
}
