import 'package:flutter/material.dart';
import 'package:message_za_lo/commons/constants/numeral.dart';
import 'package:message_za_lo/commons/constants/theme.dart';

class TextFieldWidget extends StatelessWidget {
  final TextEditingController textEditingController;
  final String hintText;
  final double? width;
  final int? maxLine;
  final bool? obscureText;
  final double? height;
  final Widget? suffixIcon;
  final TextInputType textInputType;
  final Function? onChanged;
  final int? maxLength;
//  final Icon? icon;
  final double? marginLeftRight;
  const TextFieldWidget(
      {Key? key,
      required this.textEditingController,
      required this.hintText,
      this.width,
      this.marginLeftRight,
      this.maxLine,
      this.obscureText,
      this.height,
      this.suffixIcon,
      required this.textInputType,
      this.onChanged,
       this.maxLength})
      : super(key: key);

  @override
  Widget build(BuildContext context) {
    Size size = MediaQuery.of(context).size;
    return Container(
      height: height ?? Numeral.HEIGHT_TEXT_FIELD_NORMAL,
      width: width ?? size.width,
      margin:  EdgeInsets.symmetric(
          horizontal: marginLeftRight ?? 0),
      child: TextField(
        onChanged: (value) {
          if (onChanged != null) {
            onChanged!(value);
          }
        },
        autocorrect: false,
        enableSuggestions: false,
        maxLength: maxLength ?? 100,
        keyboardType: textInputType,
        obscureText: obscureText?? false,
        maxLines: maxLine ?? 1,
        controller: textEditingController,
        decoration: InputDecoration(
          counterText: '',
          contentPadding: const EdgeInsets.symmetric(vertical: 5),
          suffixIcon: suffixIcon ?? const SizedBox(),
          hintText: hintText,
          hintStyle: DefaultTheme.TEXT_STYLE_HINT_FIELD,
          disabledBorder: InputBorder.none,
          enabledBorder: const UnderlineInputBorder(
            borderRadius: BorderRadius.zero,
            borderSide: BorderSide(
              width: 0.5,
              color: DefaultTheme.BLACK,
            ),
          ),
          focusedBorder: const UnderlineInputBorder(
            borderRadius: BorderRadius.zero,
            borderSide: BorderSide(
              width: 1,
              color: DefaultTheme.THEME_COLOR,
            ),
          ),
        ),
      ),
    );
  }
}
