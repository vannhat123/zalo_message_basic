import 'package:flutter/material.dart';
import 'package:get/get.dart';
import 'package:message_za_lo/commons/constants/numeral.dart';
import 'package:message_za_lo/commons/constants/theme.dart';
import 'package:message_za_lo/features/home/tabbar/controller/tabbar_controller.dart';

class AppBarTabBarWidget extends StatelessWidget implements PreferredSizeWidget {
  final String title;

  const AppBarTabBarWidget({Key? key, required this.title})
      : preferredSize = const Size.fromHeight(Numeral.APPBAR_HEIGHT),
        super(key: key);
  @override
  final Size preferredSize;

  @override
  Widget build(BuildContext context) {
    return AppBar(
      backgroundColor: DefaultTheme.THEME_COLOR,
      elevation: 0,
      leading: IconButton(
        padding: const EdgeInsets.all(0),
        icon: const Icon(
          Icons.search_sharp,
          size: 27,
        ),
        onPressed: () {
          Navigator.of(context).pop();
        },
      ),
      title: Container(
        alignment: Alignment.centerLeft,
        child: Row(
          crossAxisAlignment: CrossAxisAlignment.center,
          children: [
            Text(
              title,
              style: DefaultTheme.TEXT_STYLE_APPBAR_NORMAL_WHITE,
            ),
            const Spacer(),
            GetBuilder<TabBarController>(
            init: TabBarController(),
                builder: (controller){
              int indexTypeTabBar = controller.indexTabBar;
              if(indexTypeTabBar == Numeral.INDEX_SELECT_MESSAGE){
                return Row(
                  children: const [
                    Icon(Icons.qr_code_scanner,size: 25,color: DefaultTheme.WHITE,),
                    SizedBox(width: 12,),
                    Icon(Icons.add,size: 33,color: DefaultTheme.WHITE,),
                  ],
                );
              }
              if(indexTypeTabBar == Numeral.INDEX_SELECT_CONTACT){
                return const Icon(Icons.person_add_alt_1,size: 30,color: DefaultTheme.WHITE,);
              }
              if(indexTypeTabBar == Numeral.INDEX_SELECT_DISCOVER){
                return const Icon(Icons.qr_code_scanner,size: 25,color: DefaultTheme.WHITE,);
              }
              if(indexTypeTabBar == Numeral.INDEX_SELECT_DIARY){
                return Row(
                  children: const [
                  Icon(Icons.drive_file_rename_outline,size: 25,color: DefaultTheme.WHITE,),
                    SizedBox(width: 12,),
                    Icon(Icons.notifications_none_sharp,size: 25,color: DefaultTheme.WHITE,),
                  ],
                );
              }
              if(indexTypeTabBar == Numeral.INDEX_SELECT_PROFILE){
                return const Icon(Icons.settings,size: 25,color: DefaultTheme.WHITE,);
              }
              return const SizedBox();
                }),
          ],
        ),
      ),
    );
  }
}
