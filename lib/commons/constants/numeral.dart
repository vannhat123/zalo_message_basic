// ignore_for_file: constant_identifier_names
class Numeral {
  static const double TEXT_ZA_LO = 35;
  static const double TEXT_SUB_BUTTON = 16;
  static const double TEXT_SUB_SLOGAN = 15;
  static const double TEXT_PRIMARY = 18;
  static const double TEXT_SUB_NORMAL = 16;
  static const double APPBAR_HEIGHT = 40;
  static const double TEXT_SUB_SMALL = 14;
  static const double TEXT_SUB_SMALL_MOMENT = 13;
  static const double PADDING_LEFT_RIGHT_PRIMARY = 15;
  static const double MARGIN_APP_BAR = 30;
  static const double HEIGHT_TEXT_FIELD_NORMAL = 40;
  static const int MAX_LENGTH_NUMBER_PHONE = 20;
  static const int MAX_LENGTH_PASSWORD = 12;
  static const double SIZE_HEIGHT_BUTTON_ONE = 0.1;
  static const double TEXT_POST_PRIMARY = 16;
  static const double TEXT_POST_SUB = 12;
  static const double TEXT_SUB_UTILITY_BOTTOM_ICON = 12;
  static const double TEXT_SUB_UTILITY_FIND_XO_SO = 13;
  static const double TEXT_SUB_TODAY_XO_SO = 15;
  static const double TEXT_SUB_BUTTON_TIEN_LEN = 14;
  static const double TEXT_SUB_DELICIOUS_BOTTOM_ICON = 9;
  static const double TEXT_SUB_APP_BAR_MESSAGE = 10;
  static const double TEXT_THREE_DOT_CHAT = 7;
  static const double TEXT_NAME_ACCOUNT_DETAIL = 13;
  static const double TEXT_INFORMATION_ACCOUNT_DETAIL = 12;
  static const double TEXT_SUB_DESCRIPTION_DIALOG = 13.2;
  static const double TEXT_SUB_CONFIRM_DIALOG = 14;
  static const double TEXT_PRIMARY_SHOW_SNACK_IMAGE = 18;
  static const double TEXT_SUB_SHOW_SNACK_IMAGE = 13;
  static const double TEXT_SUB_LIKE_OTHER = 12;
  static const double TEXT_SUB_LIKE_SUB = 8;
  static const double TEXT_STYLE_COMMENT_PRIMARY = 14;
  static const double TEXT_STYLE_COMMENT_SUB = 13;
  static const double TEXT_STYLE_ANSWER = 10;
  static const double TEXT_STYLE_COMMENT_DIARY = 14;
  // margin bottom
  static const double MARGIN_BOTTOM_FOOTER = 20;

  // height icon
  static const double HEIGHT_ICON = 25;

  // size text icon footer
  static const double TEXT_ICON_FOOTER = 12;

  // size text error password
  static const double TEXT_ERROR_PASSWORD = 12;

  // index select icon tab bar
  static const int INDEX_SELECT_MESSAGE = 0;
  static const int INDEX_SELECT_CONTACT = 1;
  static const int INDEX_SELECT_DISCOVER = 2;
  static const int INDEX_SELECT_DIARY = 3;
  static const int INDEX_SELECT_PROFILE = 4;


  // index select message chat
  static const int INDEX_SELECT_CHAT = 5;

  // font size text message
  static const double TEXT_MESSAGE_PRIMARY = 18;
  static const double TEXT_MESSAGE_SUB = 16;
  static const double TEXT_MESSAGE_NOTIFICATION = 12;
  static const double TEXT_MESSAGE_TIME_SEND = 14;

  // font size text contact tab bar
  static const double TEXT_CONTACT_TAB_BAR = 14;

  // opacity color GREY
  static const double OPACITY_COLOR_GREY = 0.08;

  // opacity color GREY DIVIDE
  static const double OPACITY_COLOR_GREY_DIVIDE = 0.25;

  // size icon profile information
  static const double SIZE_ICON_PROFILE_INFORMATION = 22;
  static const double TEXT_IMAGE_VIDEO_ALBUM = 10;
  static const double SIZE_ICON_IMAGE_VIDEO_ALBUM = 16;

  // height Divide container
  static const double HEIGHT_DIVIDE_CONTAINER = 8;
  static const double HEIGHT_DIVIDE_CONTAINER_DIARY = 9.5;

  static const double SIZE_ICON_UTILITY_DISCOVER = 20;
  static const double HEIGHT_CONTAINER_OUT_ICON_UTILITY =40;
  static const double SIZE_ICON_GAME_DISCOVER =15;
  static const double HEIGHT_CIRCLE_CONTAINER_ICON_UTILITY =20;
  static const double WIDTH_BORDER_FIND_TICKET =1;
  static const double SIZE_ICON_BACK =22;

  //value count line default in chat screen
  static const int VALUE_DEFAULT_COUNT_LINE_CHAT = 1;
  static const double SIZE_ICON_IN_CHAT = 25;
  static const double SIZE_TEXT_SUB_CHAT = 9;
  static const double SIZE_TEXT_PRIMARY_CHAT = 15;

  // height divide line account detail
  static const double SIZE_DIVIDE_LINE_ACCOUNT_DETAIL = 0.5;

 // height text field change information
  static const double SIZE_HEIGHT_TEXT_FIELD_CHANGE_INFORMATION = 50;

  // size image clip rect account change
  static const double SIZE_IMAGE_CLIP_RECT_ACCOUNT_CHANGE = 55;

  // value not exist
  static const int VALUE_NOT_EXIST = -9999;

  // select gender
  static const int VALUE_GENDER_MALE = 0;
  static const int VALUE_GENDER_FEMALE = 1;

  //height container show snack bar image
  static const double HEIGHT_SHOW_SNACK_BAR_IMAGE = 50;
  static const double HEIGHT_CONTAINER_ACCOUNT_DETAIL_INFORMATION = 275;

  // type select image
  static const int TYPE_SELECT_IMAGE_SHOW_AVATAR = 0;
  static const int TYPE_SELECT_IMAGE_FROM_CAMERA = 1;
  static const int TYPE_SELECT_IMAGE_FROM_MACHINE = 2;
  static const int TYPE_SELECT_IMAGE_AVAILABLE = 3;

  //type gender
  static const int TYPE_GENDER_MALE = 0;
  static const int TYPE_GENDER_FEMALE = 1;

  //value none exits
  static const int VALUE_NONE_EXITS = -9999;

  // type update profile
  static const int TYPE_UPDATE_PROFILE_NOT_IMAGE = 0;
  static const int TYPE_UPDATE_PROFILE_IMAGE_AVATAR = 1;
  static const int TYPE_UPDATE_PROFILE_IMAGE_BACKGROUND = 2;
  static const int TYPE_UPDATE_PROFILE_IMAGE_POSTS_DIARY = 3;

  static const double SIZE_LIKE_POSTS = 21;

  //length posts diary
  static const int LENGTH_POSTS_DIARY = 2000;
}
