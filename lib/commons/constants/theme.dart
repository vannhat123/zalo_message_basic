// ignore_for_file: constant_identifier_names, non_constant_identifier_names
import 'package:flutter/material.dart';
import 'package:message_za_lo/commons/constants/numeral.dart';

class DefaultTheme {
  //colors
  static const Color BLACK = Color(0xFF000000);
  static const Color BLACK_DETAIL = Color(0xFF3D3A39);
  static const Color WHITE = Color(0xFFFFFFFF);
  static const Color TRANSPARENT = Color(0x00000000);
  static const Color GREY = Color(0xFF666A72);
  static const Color GREY_BUTTON = Color(0xFFD9D9D9);
  static const Color GREY_BORDER = Color(0xFFF5F4F4);
  static const Color GREY_BORDER_INPUT = Color(0xFFEEEEEE);
  static const Color GREY_BORDER_BUTTON = Color(0xFFDAD7D7);
  static const Color GREY_BOX = Color(0xFFEEEFF3);
  static const Color GREY_FILL = Color(0xFFEFEDED);
  static const Color GREY_HINT_TEXT = Color(0xFFE8E3E3);
  static const Color GREY_TEXT_BUTTON = Color(0xFF9E9D9D);
  static const Color GREY_DISABLE = Color(0xFFEDF1F1);
  static const Color GREY_ICON = Color(0xFFDBD5D5);
  static const Color GREY_GROUP = Color(0xFFF1EDED);
  static const Color BLUE = Color(0xFF1F8DD7);
  static const Color BLUE_PASSWORD = Color(0xFF4F81E1);
  static const Color BLUE_TEXT = Color(0xFF84C6EB);
  static const Color GREEN = Color(0xFF80AB7F);
  static const Color GREEN_HEADER = Color(0xFF7FAF74);
  static const Color THEME_COLOR = Color(0xFF0586FD);
  static const Color RED = Color(0xFFED0000);
  static const Color ORANGE = Color(0xFFFB9702);
  static const Color YELLOW_ALARM = Color(0xFFF4F9C0);
  static const Color YELLOW_BUTTON = Color(0xFFF6D6A9);
  static const Color PINK_BUTTON = Color(0xFFF0C5C5);
  static const Color PINK_CIRCLE = Color(0xFFE1AEAE);
  static const Color PINK_ICON = Color(0xFFFF9D9D);
  static const Color TURQUOISE = Color(0xFFCCE9F9);
  static const Color THEME_GRADIENT0 = Color(0xFF0C7FE9);
  static const Color THEME_GRADIENT1 = Color(0xFF068EFC);
  static const Color THEME_GRADIENT2 = Color(0xFF089CF7);
  static const Color THEME_GRADIENT3 = Color(0xFF00ACF4);
  static const Color VIOLET = Color(0xFF8D0BF3);
  static const Color RED_BOLD = Color(0xFF85061D);
  static const Color ORANGE_BOLD = Color(0xFF8A670C);
  static const Color ORANGE_ALLOW = Color(0xFFFFE9c0);
  static const Color ORANGE_ALLOW1 = Color(0xFFF9ECC9);
  static const Color ORANGE_ALLOW2 = Color(0xFFDEC585);
  static const Color ORANGE_ALLOW0 = Color(0xFFFDEABF);
  static const Color BACKGROUND_CHAT = Color(0xFFE2E7F1);
  static const Color BACKGROUND_BOX_CHAT = Color(0xFFCFF0FE);
  static const Color BACKGROUND_BUTTON_WIDGET_HIDE = Color(0xFFBFD4E4);
  static const Color BACKGROUND_SNACK_BAR_SHOW_IMAGE = Color(0xFFEDEDED);
  static const Color BACKGROUND_GREY_CONTAINER = Color(0xFFBBBCBE);

  // text za lo
  static const TEXT_STYLE_ZA_LO = TextStyle(
    color: THEME_COLOR,
    fontSize: Numeral.TEXT_ZA_LO,
    fontWeight: FontWeight.normal,
  );

  // text sub button  white
  static const TEXT_SUB_BUTTON_WHITE = TextStyle(
    color: WHITE,
    fontSize: Numeral.TEXT_SUB_BUTTON,
    fontWeight: FontWeight.normal,
  );

  static const TEXT_SUB_GAME_TIEN_LEN = TextStyle(
    color: WHITE,
    fontSize: Numeral.TEXT_SUB_BUTTON_TIEN_LEN,
    fontWeight: FontWeight.normal,
  );

  // text sub button grey
  static const TEXT_SUB_BUTTON_GREY = TextStyle(
    color: GREY,
    fontSize: Numeral.TEXT_SUB_BUTTON,
    fontWeight: FontWeight.normal,
  );

  // text sub button black
  static const TEXT_SUB_BUTTON_BLACK = TextStyle(
    color: BLACK,
    fontSize: Numeral.TEXT_SUB_BUTTON,
    fontWeight: FontWeight.normal,
  );

  // text sub slogan bold
  static const TEXT_SUB_SLOGAN_BOLD = TextStyle(
    color: BLACK,
    fontSize: Numeral.TEXT_SUB_SLOGAN,
    fontWeight: FontWeight.bold,
  );

  // text sub slogan normal
  static const TEXT_SUB_SLOGAN_NORMAL = TextStyle(
    color: GREY,
    fontSize: Numeral.TEXT_SUB_SLOGAN,
    fontWeight: FontWeight.normal,
  );

  // text sytle dialog error
  static const TEXT_STYLE_DIALOG_ERROR = TextStyle(
    color: GREY,
    fontSize: Numeral.TEXT_SUB_NORMAL,
    fontWeight: FontWeight.normal,
  );

  static const TEXT_STYLE_APPBAR = TextStyle(
    color: GREY,
    fontSize: Numeral.TEXT_SUB_NORMAL,
    fontWeight: FontWeight.normal,
  );

  static const TEXT_STYLE_TOAST_SUCCESS = TextStyle(
    color: THEME_COLOR,
    fontSize: Numeral.TEXT_SUB_NORMAL,
    fontWeight: FontWeight.normal,
  );

  static const TEXT_STYLE_PRIMARY_WHITE = TextStyle(
    color: WHITE,
    fontSize: Numeral.TEXT_PRIMARY,
    fontWeight: FontWeight.normal,
  );

  static const TEXT_STYLE_THREE_DOT_CHAT = TextStyle(
    color: GREY,
    fontSize: Numeral.TEXT_THREE_DOT_CHAT,
    fontWeight: FontWeight.normal,
  );

  static const TEXT_STYLE_ERROR_DESCRIPTION = TextStyle(
    color: THEME_COLOR,
    fontSize: Numeral.TEXT_PRIMARY,
    fontWeight: FontWeight.normal,
  );

  static const TEXT_STYLE_ERROR_TITLE = TextStyle(
    color: RED,
    fontSize: Numeral.TEXT_PRIMARY +2,
    fontWeight: FontWeight.bold,
  );

  static const TEXT_STYLE_PRIMARY_BLACK = TextStyle(
    color: BLACK,
    fontSize: Numeral.TEXT_PRIMARY,
    fontWeight: FontWeight.normal,
  );


  // text style sub black normal
  static const TEXT_STYLE_SUB_BLACK_NORMAL = TextStyle(
    color: BLACK,
    fontSize: Numeral.TEXT_SUB_SMALL,
    fontWeight: FontWeight.normal,
  );

  // style hint text field
  static const TEXT_STYLE_HINT_FIELD = TextStyle(
    color: GREY,
    fontSize: Numeral.TEXT_SUB_SMALL,
    fontWeight: FontWeight.normal,
  );

  static const TEXT_STYLE_SMALL_THEME_COLOR = TextStyle(
    color: THEME_COLOR,
    fontSize: Numeral.TEXT_SUB_SMALL,
    fontWeight: FontWeight.normal,
  );

  static const TEXT_STYLE_SMALL_BLACK_COLOR = TextStyle(
    color: BLACK,
    fontSize: Numeral.TEXT_SUB_SMALL,
    fontWeight: FontWeight.normal,
  );

  static const TEXT_STYLE_SMALL_GREY_COLOR = TextStyle(
    color: GREY,
    fontSize: Numeral.TEXT_SUB_SMALL,
    fontWeight: FontWeight.normal,
  );

  static const TEXT_STYLE_NORMAL_THEME_COLOR = TextStyle(
    color: THEME_COLOR,
    fontSize: Numeral.TEXT_SUB_NORMAL,
    fontWeight: FontWeight.normal,
  );

  static final TEXT_STYLE_BUTTON_HINT = TextStyle(
    color: WHITE.withOpacity(0.7),
    fontSize: Numeral.TEXT_SUB_NORMAL,
    fontWeight: FontWeight.normal,
  );

  static const TEXT_STYLE_NORMAL_WHITE_COLOR = TextStyle(
    color: WHITE,
    fontSize: Numeral.TEXT_SUB_NORMAL,
    fontWeight: FontWeight.normal,
  );

  static const TEXT_STYLE_ICON_FOOTER_HIDE = TextStyle(
    color: GREY,
    fontSize: Numeral.TEXT_ICON_FOOTER,
    fontWeight: FontWeight.normal,
  );

  // theme text error password
  static const TEXT_STYLE_ERROR_PASSWORD = TextStyle(
    color: RED,
    fontSize: Numeral.TEXT_ERROR_PASSWORD,
    fontWeight: FontWeight.normal,
  );

  //style select index tab bar bottom
  static const TEXT_STYLE_ICON_FOOTER_SHOW = TextStyle(
    color: THEME_COLOR,
    fontSize: Numeral.TEXT_ICON_FOOTER,
    fontWeight: FontWeight.normal,
  );

  static const TEXT_STYLE_MESSAGE_PRIMARY = TextStyle(
    color: BLACK,
    fontSize: Numeral.TEXT_MESSAGE_PRIMARY,
    fontWeight: FontWeight.w600,
  );

  static const TEXT_STYLE_MESSAGE_SUB_NOT_SEEN = TextStyle(
    color: BLACK,
    fontSize: Numeral.TEXT_MESSAGE_SUB,
    fontWeight: FontWeight.w500,
  );

  static const TEXT_STYLE_MESSAGE_NOTIFICATION = TextStyle(
    color: WHITE,
    fontSize: Numeral.TEXT_MESSAGE_NOTIFICATION,
    fontWeight: FontWeight.w400,
  );

  static const TEXT_STYLE_MESSAGE_SUB_SEEN = TextStyle(
    color: GREY,
    fontSize: Numeral.TEXT_MESSAGE_NOTIFICATION,
    fontWeight: FontWeight.w400,
  );

  static const TEXT_STYLE_MESSAGE_TIME_SEND = TextStyle(
    color: GREY,
    fontSize: Numeral.TEXT_MESSAGE_TIME_SEND,
    fontWeight: FontWeight.w400,
  );

  static const TEXT_STYLE_SUB_NORMAL_GREY = TextStyle(
    color: GREY,
    fontSize: Numeral.TEXT_SUB_NORMAL,
    fontWeight: FontWeight.w400,
  );

  static const TEXT_STYLE_SUB_W500_BLACK = TextStyle(
    color: BLACK,
    fontSize: Numeral.TEXT_SUB_NORMAL,
    fontWeight: FontWeight.w500,
  );

  // text style friend , group, oa in contact tab bar
  static const TEXT_STYLE_TAB_BAR_CONTACT = TextStyle(
    fontSize: Numeral.TEXT_CONTACT_TAB_BAR,
    fontWeight: FontWeight.w400,
  );

  static const TEXT_STYLE_APPBAR_NORMAL_WHITE = TextStyle(
    color: WHITE,
    fontSize: Numeral.TEXT_PRIMARY,
    fontWeight: FontWeight.normal,
  );

  static const TEXT_STYLE_APPBAR_SUB_WHITE = TextStyle(
    color: WHITE,
    fontSize: Numeral.TEXT_SUB_APP_BAR_MESSAGE,
    fontWeight: FontWeight.normal,
  );

  static const TEXT_STYLE_MESSAGE_PRIMARY_W400_BLACK = TextStyle(
    color: BLACK,
    fontSize: Numeral.TEXT_MESSAGE_PRIMARY,
    fontWeight: FontWeight.w400,
  );

  static const TEXT_STYLE_MESSAGE_SUB_W300_GREY = TextStyle(
    color: GREY,
    fontSize: Numeral.TEXT_MESSAGE_SUB,
    fontWeight: FontWeight.w300,
  );

  static const TEXT_STYLE_IMAGE_VIDEO_ALBUM = TextStyle(
    color: BLACK,
    fontSize: Numeral.TEXT_IMAGE_VIDEO_ALBUM,
    fontWeight: FontWeight.w400,
  );

  static const TEXT_STYLE_SMALL_MOMENT_BLACK_COLOR_W300 = TextStyle(
    color: BLACK,
    fontSize: Numeral.TEXT_SUB_SMALL_MOMENT,
    fontWeight: FontWeight.w300,
  );

  static const TEXT_STYLE_POST_PRIMARY = TextStyle(
    color: BLACK,
    fontSize: Numeral.TEXT_POST_PRIMARY,
    fontWeight: FontWeight.w500,
  );

  static const TEXT_STYLE_POST_CONTENT = TextStyle(
    color: BLACK,
    fontSize: Numeral.TEXT_POST_PRIMARY-1,
    fontWeight: FontWeight.w300,
  );

  static const TEXT_STYLE_COMMENT_PRIMARY_W500 = TextStyle(
    color: BLACK,
    fontSize: Numeral.TEXT_STYLE_COMMENT_PRIMARY,
    fontWeight: FontWeight.w500,
  );

  static const TEXT_STYLE_COMMENT_PRIMARY_W300 = TextStyle(
    color: BLACK,
    fontSize: Numeral.TEXT_STYLE_COMMENT_SUB,
    fontWeight: FontWeight.w300,
  );

  static const TEXT_STYLE_POST_PRIMARY_GREY = TextStyle(
    color: BLACK,
    fontSize: Numeral.TEXT_POST_PRIMARY,
    fontWeight: FontWeight.w400,
  );

  static const TEXT_STYLE_POST_SUB = TextStyle(
    color: GREY,
    fontSize: Numeral.TEXT_POST_SUB,
    fontWeight: FontWeight.w300,
  );

  static const GRADIENT_THEME_COLOR = LinearGradient(
      colors: [
        THEME_GRADIENT0,
        THEME_GRADIENT1,
        THEME_GRADIENT2,
        THEME_GRADIENT3
      ],
      begin: Alignment.topLeft, //begin of the gradient color
      end: Alignment.bottomRight, //end of the gradient color
      stops: [0, 0.2, 0.5, 0.8] //stops for individual color
  );

  static const TEXT_STYLE_UTILITY_BOTTOM_ICON = TextStyle(
    color: BLACK,
    fontSize: Numeral.TEXT_SUB_UTILITY_BOTTOM_ICON,
    fontWeight: FontWeight.normal,
  );

  static const TEXT_STYLE_UTILITY_DELICIOUS_NEAR_YOU = TextStyle(
    color:GREY_TEXT_BUTTON,
    fontSize: Numeral.TEXT_SUB_UTILITY_BOTTOM_ICON,
    fontWeight: FontWeight.normal,
  );

  static const TEXT_STYLE_UTILITY_BOTTOM_ICON_RED_W700 = TextStyle(
    color: RED_BOLD,
    fontSize: Numeral.TEXT_SUB_TODAY_XO_SO,
    fontWeight: FontWeight.w700,
  );

  static const TEXT_STYLE_UTILITY_BOTTOM_ICON_RED_W300 = TextStyle(
    color: RED_BOLD,
    fontSize: Numeral.TEXT_SUB_TODAY_XO_SO,
    fontWeight: FontWeight.w300,
  );

  static const TEXT_STYLE_UTILITY_FIND_XO_SO_GREY_W300 = TextStyle(
    color: BLACK,
    fontSize: Numeral.TEXT_SUB_UTILITY_FIND_XO_SO,
    fontWeight: FontWeight.w300,
  );

  static const TEXT_STYLE_UTILITY_FIND_ORANGE_BOLD_W500 = TextStyle(
    color: ORANGE_BOLD,
    fontSize: Numeral.TEXT_SUB_UTILITY_FIND_XO_SO,
    fontWeight: FontWeight.w500,
  );

  static const TEXT_STYLE_UTILITY_BOTTOM_ICON_GREY = TextStyle(
    color: GREY,
    fontSize: Numeral.TEXT_SUB_UTILITY_BOTTOM_ICON,
    fontWeight: FontWeight.normal,
  );

  static const TEXT_STYLE_UTILITY_BOTTOM_ICON_WHITE = TextStyle(
    color: WHITE,
    fontSize: Numeral.TEXT_SUB_UTILITY_BOTTOM_ICON,
    fontWeight: FontWeight.normal,
  );

  static const TEXT_STYLE_SUB_ADS = TextStyle(
    color: GREY,
    fontSize: Numeral.TEXT_SUB_UTILITY_BOTTOM_ICON -2,
    fontWeight: FontWeight.normal,
  );

  static const TEXT_STYLE_BOTTOM_ICON_DELICIOUS = TextStyle(
    color: BLACK,
    fontSize: Numeral.TEXT_SUB_DELICIOUS_BOTTOM_ICON,
    fontWeight: FontWeight.normal
  );

  static const TEXT_STYLE_PRIMARY_BLACK_CHAT = TextStyle(
    color: BLACK,
    fontSize: Numeral.SIZE_TEXT_PRIMARY_CHAT,
    fontWeight: FontWeight.w300,
  );

  static const TEXT_STYLE_SUB_GREY_CHAT = TextStyle(
    color: GREY,
    fontSize: Numeral.SIZE_TEXT_SUB_CHAT,
    fontWeight: FontWeight.w400,
  );

  static const TEXT_STYLE_NAME_PRIMARY_ACCOUNT_DETAIL = TextStyle(
    color: WHITE,
    fontSize: Numeral.TEXT_NAME_ACCOUNT_DETAIL,
    fontWeight: FontWeight.w700,
  );

  static const TEXT_STYLE_INFORMATION_ACCOUNT_DETAIL_BLACK_W500 = TextStyle(
    color: BLACK,
    fontSize: Numeral.TEXT_INFORMATION_ACCOUNT_DETAIL +0.5,
    fontWeight: FontWeight.w500,
  );

  static const TEXT_STYLE_INFORMATION_ACCOUNT_DETAIL_NORMAL = TextStyle(
    color: BLACK,
    fontSize: Numeral.TEXT_INFORMATION_ACCOUNT_DETAIL,
    fontWeight: FontWeight.normal,
  );

  static const TEXT_STYLE_INFORMATION_ACCOUNT_DETAIL_GREY = TextStyle(
    color: GREY,
    fontSize: Numeral.TEXT_INFORMATION_ACCOUNT_DETAIL,
    fontWeight: FontWeight.normal,
  );

  static const TEXT_STYLE_INFORMATION_ACCOUNT_DETAIL_GREY_SUB = TextStyle(
    color: GREY,
    fontSize: Numeral.TEXT_INFORMATION_ACCOUNT_DETAIL-2,
    fontWeight: FontWeight.w300,
  );

  static const TEXT_STYLE_CONFIRM_DIALOG_CANCEL = TextStyle(
    color: THEME_COLOR,
    fontSize: Numeral.TEXT_SUB_CONFIRM_DIALOG,
    fontWeight: FontWeight.w500,
  );

  static const TEXT_STYLE_CONFIRM_DIALOG_AGREE = TextStyle(
    color: THEME_COLOR,
    fontSize: Numeral.TEXT_SUB_CONFIRM_DIALOG,
    fontWeight: FontWeight.w300,
  );

  static const TEXT_STYLE_DESCRIPTION_DIALOG = TextStyle(
    color: BLACK,
    fontSize: Numeral.TEXT_SUB_DESCRIPTION_DIALOG,
    fontWeight: FontWeight.w300,
  );

  static const TEXT_STYLE_PRIMARY_SHOW_SNACK_IMAGE = TextStyle(
    color: THEME_GRADIENT0,
    fontSize: Numeral.TEXT_PRIMARY_SHOW_SNACK_IMAGE,
    fontWeight: FontWeight.w400,
  );

  static const TEXT_STYLE_SUB_SHOW_SNACK_IMAGE = TextStyle(
    color: GREY,
    fontSize: Numeral.TEXT_SUB_SHOW_SNACK_IMAGE,
    fontWeight: FontWeight.w300,
  );

  static const TEXT_STYLE_CANCEL_SHOW_SNACK_IMAGE = TextStyle(
    color: THEME_GRADIENT0,
    fontSize: Numeral.TEXT_PRIMARY_SHOW_SNACK_IMAGE,
    fontWeight: FontWeight.w600,
  );

  static const TEXT_STYLE_LIKE_OTHER = TextStyle(
    color: GREY,
    fontSize: Numeral.TEXT_SUB_LIKE_OTHER,
    fontWeight: FontWeight.w300,
  );

  static const TEXT_STYLE_LIKE_SUB = TextStyle(
    color: GREY,
    fontSize: Numeral.TEXT_SUB_LIKE_SUB,
    fontWeight: FontWeight.w300,
  );

  static const TEXT_STYLE_LIKE_SUB_BOLD_ANSWER = TextStyle(
    color: BLACK,
    fontSize: Numeral.TEXT_STYLE_ANSWER,
    fontWeight: FontWeight.w400,
  );

  static const TEXT_STYLE_LIKE_SUB_NORMAL_ANSWER = TextStyle(
    color: GREY,
    fontSize: Numeral.TEXT_STYLE_ANSWER,
    fontWeight: FontWeight.normal,
  );

  static const TEXT_SUB_COMMENT_DIARY = TextStyle(
    color: BLACK,
    fontSize: Numeral.TEXT_STYLE_COMMENT_DIARY,
    fontWeight: FontWeight.normal,
  );
}
