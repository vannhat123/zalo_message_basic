import 'package:get/get.dart';
import 'package:message_za_lo/features/home/tabbar/binding/tabbar_binding.dart';
import 'package:message_za_lo/features/home/tabbar/view/tabbar_screen.dart';
import 'package:message_za_lo/features/initial/view/initial_screen.dart';

import '../routes/Routes.dart';

class Pages {
  static final pages = [
    GetPage(name: Routes.INITIAL, page: () => const InitialScreen()),
    GetPage(
        name: Routes.TAB_BAR,
        page: () => const TabBarScreen(),
        binding: TabBarBinding(),
        transition: Transition.leftToRight),
  ];
}
