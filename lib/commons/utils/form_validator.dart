import 'package:flutter/material.dart';
import 'package:get/get.dart';
import 'package:message_za_lo/commons/widgets/widgets.dart';

class FormValidator {
  const FormValidator._privateConstructor();

  static const FormValidator _instance = FormValidator._privateConstructor();

  static FormValidator get instance => _instance;

  bool isEmail(String email) {
    String p =
        r'^(([^<>()[\]\\.,;:\s@\"]+(\.[^<>()[\]\\.,;:\s@\"]+)*)|(\".+\"))@((\[[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\])|(([a-zA-Z\-0-9]+\.)+[a-zA-Z]{2,}))$';
    RegExp regExp = new RegExp(p);
    return regExp.hasMatch(email);
  }

  bool isCheckInteger(String string) {
    final numericRegex = RegExp(r'^(([0-9]*))$');
    return numericRegex.hasMatch(string);
  }

  bool checkNumberPhone(String string) {
    if (isCheckInteger(string)) {
      if (string.length == 10 ||
          string.length == 11 && string.replaceAll(' ', '') == string) {
        return true;
      }
      return false;
    }
    return false;
  }

  bool checkLengthString(String string) {
    if(string.trim().isNotEmpty){
      return true;
    }
    return false;
  }
}
