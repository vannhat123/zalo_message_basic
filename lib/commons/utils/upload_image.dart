import 'dart:io';
import 'package:flutter/material.dart';
import 'package:get/get.dart';
import 'package:image_cropper/image_cropper.dart';
import 'package:image_picker/image_picker.dart';
import 'package:message_za_lo/commons/constants/numeral.dart';
import 'package:message_za_lo/features/home/diary/controller/diary_controller.dart';
import 'package:message_za_lo/services/firebase/firestore_helper.dart';
import 'package:message_za_lo/services/permission/permission_helper.dart';

class UploadImage {
  const UploadImage._privateConstructor();

  static const UploadImage _instance = UploadImage._privateConstructor();

  static UploadImage get instance => _instance;

  void getFromGalleryToApiShowScreen(ImagePicker imagePicker,
      BuildContext context, Size size, int typeImage) async {
    PermissionHelper.instance.requestPermission(
      onGranted: () async {
        try {
          final XFile? pickedFile =
              await imagePicker.pickImage(source: ImageSource.gallery);
          if (pickedFile != null) {
            ImageCropper().cropImage(
              sourcePath: pickedFile.path,
              compressFormat: ImageCompressFormat.jpg,
              aspectRatio:
                  (typeImage == Numeral.TYPE_UPDATE_PROFILE_IMAGE_AVATAR)
                      ? CropAspectRatio(ratioX: 0.9, ratioY: 0.9)
                      : CropAspectRatio(ratioX: 1.6, ratioY: 0.9),
              maxHeight: (size.height * 0.9).round(),
              maxWidth: (size.width * 0.9).round(),
              cropStyle: CropStyle.rectangle,
              uiSettings: [
                AndroidUiSettings(
                    toolbarTitle: "Cắt ảnh",
                    toolbarColor: Colors.blue,
                    toolbarWidgetColor: Colors.white,
                    initAspectRatio: CropAspectRatioPreset.square,
                    lockAspectRatio: false),
                IOSUiSettings(
                    title: "Cắt ảnh",
                    hidesNavigationBar: true,
                    aspectRatioLockEnabled: true,
                    aspectRatioPickerButtonHidden: true,
                    rotateClockwiseButtonHidden: true,
                    rotateButtonsHidden: true),
              ],
            ).then(
              (response) {
                if (response != null) {
                  print('select');
                  File file = File(response.path);
                  FireStoreHelper()
                      .uploadFileToFirebaseStorage(file, context, typeImage);
                  Navigator.pop(context);
                } else {
                  print('cancel');
                }
              },
            );
          }
        } catch (e) {
          print("UploadImage: $e");
        }
      },
      context: context,
    );
  }

  Future<void> selectImagePostsDiary(BuildContext context) async {
    PermissionHelper.instance.requestPermission(
        onGranted: () async {
          final ImagePicker imagePicker = ImagePicker();
          final List<XFile> selectedImages = await imagePicker.pickMultiImage();
          if (selectedImages.isNotEmpty) {
            Get.put(DiaryController()).setListXFileSelectPostsDiary(selectedImages);
          }
        },
        context: context);
  }
}
