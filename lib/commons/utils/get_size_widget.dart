import 'package:flutter/material.dart';

class GetSizeWidget {
  const GetSizeWidget._privateConstructor();

  static const GetSizeWidget _instance = GetSizeWidget._privateConstructor();

  static GetSizeWidget get instance => _instance;

  Size getSize(GlobalKey key) {
    final sizePrimary = key.currentContext!.size;
    if (sizePrimary != null) {
      return sizePrimary;
    } else {
      return Size(0, 0);
    }
  }
}
