// // ignore_for_file: non_constant_identifier_names
//
// import 'package:logger/logger.dart';
//
// class LoggerUtil extends Logger {
//   LogPrinter printer;
//   Level level;
//
//   LoggerUtil({
//     required this.printer,
//     required this.level,
//   }) : super(
//           printer: printer,
//           level: level,
//         );
//
//   /// Log a message at level [Level.error].
//   void error(
//     dynamic message, [
//     dynamic error,
//     StackTrace? stackTrace,
//   ]) =>
//       e('[ERROR] $message', error, stackTrace);
//
//   /// Log a message at level [Level.warning].
//   void warn(dynamic message, [dynamic error, StackTrace? stackTrace]) =>
//       w('[WARN] $message', error, stackTrace);
//
//   /// Log a message at level [Level.info].
//   void info(dynamic message) => i('[INFO] $message');
//
//   /// Log a message at level [Level.debug].
//   void debug(dynamic message, [dynamic error, StackTrace? stackTrace]) =>
//       d('[DEBUG] $message', error, stackTrace);
//
//   /// Log a message at level [Level.verbose].
//   void verbose(dynamic message) => v('[VERBOSE]\n$message');
//
//   //set log level
//   static Level logLevel() {
//     Level level = Level.nothing;
//     if (!EnvConfig.isProduction()) {
//       level = Level.verbose;
//     }
//     return level;
//   }
// }
//
// //global variable
// final LoggerUtil LOG = LoggerUtil(
//   printer: PrettyPrinter(
//     methodCount: 0,
//     lineLength: 150,
//     printTime: true,
//     printEmojis: false,
//   ),
//   level: LoggerUtil.logLevel(),
// );
