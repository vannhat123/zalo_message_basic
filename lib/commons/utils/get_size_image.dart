import 'package:flutter/material.dart';

class GetSizeImage {
  const GetSizeImage._privateConstructor();

  static const GetSizeImage _instance = GetSizeImage._privateConstructor();

  static GetSizeImage get instance => _instance;

  Map<String, double> getSizeImage(int lengthListImage, Size size) {
    double width = 0;
    double height = 0;
    Map<String, double> result = {'width': width, 'height': height};
    if (lengthListImage == 1) {
      width = size.width;
      height = size.height * 0.3;
    } else if (lengthListImage == 2) {
      width = size.width * 0.5;
      height = size.height * 0.225;
    } else if (lengthListImage == 3) {
      width = size.width * 0.333;
      height = size.width * 0.66;
    } else if (lengthListImage > 3 && lengthListImage <= 6) {
      width = size.width * 0.333;
      height = size.width * 0.66;
    } else if (lengthListImage >= 7) {
      width = size.width * 0.333;
      height = size.width * 0.99;
    }
    result['width'] = width;
    result['height'] = height;
    return result;
  }
}
