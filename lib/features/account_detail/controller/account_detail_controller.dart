import 'package:flutter/material.dart';
import 'package:get/get.dart';
import 'package:message_za_lo/commons/constants/numeral.dart';
import 'package:message_za_lo/commons/widgets/widgets.dart';
import 'package:message_za_lo/models/profile_dto.dart';
import 'package:message_za_lo/services/firebase/firestore_helper.dart';
import 'package:message_za_lo/services/internet/internet_helper.dart';
import 'package:message_za_lo/services/shared_preferences/account_helper.dart';

class AccountDetailController extends GetxController {
  ProfileDTO profileDTO = ProfileDTO(
      dateBirth: '',
      fullName: '',
      gender: 0,
      phoneNumber: '',
      email: '',
      idDoc: 0,
      backgroundUrl: '',
      avatarUrl: '');
  bool checkShowAccountChange = false;
  final nameEditController = TextEditingController();
  final dateBirthEditController = TextEditingController();
  final Map<String, dynamic> valueOptionGender = {
    'male': Numeral.VALUE_GENDER_MALE,
    'female': Numeral.VALUE_GENDER_FEMALE,
  };
  int valueSelectedGender = Numeral.VALUE_NOT_EXIST;
  bool checkHideShowButtonSaveAccountChange = false;

  @override
  void onInit() {
    // TODO: implement onInit
    super.onInit();
    getAccount();
  }

  void getAccount() {
    FireStoreHelper.fireStore
        .collection('Profile')
        .snapshots()
        .listen((value) async {
      if (value.docs.isNotEmpty) {
        Stream<ProfileDTO> streamProfileDTO = await FireStoreHelper()
            .getProfileFirebase(AccountHelper.instance.getEmailAccount());
        streamProfileDTO.listen((event) {
          profileDTO = event;
          setValueNamePhoneGender();
          AccountHelper.instance.setAvatarUrl(profileDTO.avatarUrl);
          AccountHelper.instance.setBackgroundUrl(profileDTO.backgroundUrl);
          update();
        });
      }
    });
  }

  void setCheckShowAccountChange(bool value) {
    checkShowAccountChange = value;
    update();
  }

  void setValueSelectedGender(int checkFeMale) {
    if (valueSelectedGender == Numeral.VALUE_NOT_EXIST) {
      valueSelectedGender = valueOptionGender['male'];
    } else {
      if (checkFeMale == Numeral.VALUE_GENDER_MALE) {
        valueSelectedGender = valueOptionGender['male'];
      } else {
        valueSelectedGender = valueOptionGender['female'];
      }
    }
    update();
  }

  void checkChangeNamePhoneGender() {
    if (nameEditController.text.trim() == profileDTO.fullName &&
        dateBirthEditController.text.trim() == profileDTO.dateBirth &&
        valueSelectedGender == profileDTO.gender) {
      setCheckHideShowButtonSaveAccountChange(false);
    } else {
      if (!checkHideShowButtonSaveAccountChange) {
        setCheckHideShowButtonSaveAccountChange(true);
      }
    }
    update();
  }

  void setValueNamePhoneGender() {
    nameEditController.value =
        nameEditController.value.copyWith(text: profileDTO.fullName);
    dateBirthEditController.value =
        dateBirthEditController.value.copyWith(text: profileDTO.dateBirth);
    setValueSelectedGender(profileDTO.gender);
    update();
  }

  void setCheckHideShowButtonSaveAccountChange(bool value) {
    checkHideShowButtonSaveAccountChange = value;
    update();
  }

  Future<void> updateProfileToFirebase(BuildContext context) async {
    bool checkInternet = await InternetHelper.instance.checkInternet(context);
    if (checkInternet) {
      bool checkUpdateSuccess = false;
      DialogWidget.instance.openLoadingDialog(context: context);
      checkUpdateSuccess = await FireStoreHelper().updateProfileToFirebase(
          fullName: nameEditController.text,
          dateBirth: dateBirthEditController.text,
          gender: valueSelectedGender,
          urlImage: profileDTO.avatarUrl,
          email: profileDTO.email,
          type: Numeral.TYPE_UPDATE_PROFILE_NOT_IMAGE,
          urlBackground: '');
      DialogWidget.instance.dismiss(context: context);
      if (checkUpdateSuccess) {
        DialogWidget.instance.showToast(context, "Cập nhật thành công");
        setCheckHideShowButtonSaveAccountChange(false);
      }
    }
  }

  String getGender() {
    if (profileDTO.gender == Numeral.TYPE_GENDER_MALE) {
      return 'Nam';
    } else {
      return 'Nữ';
    }
  }
}
