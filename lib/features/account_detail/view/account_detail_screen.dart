import 'package:flutter/material.dart';
import 'package:message_za_lo/features/account_detail/view/component/account_detail_body.dart';
class AccountDetailScreen extends StatelessWidget {
  const AccountDetailScreen({Key? key}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      body: AccountDetailBody(),
    );
  }
}
