import 'package:flutter/material.dart';
import 'package:get/get.dart';
import 'package:message_za_lo/commons/constants/numeral.dart';
import 'package:message_za_lo/commons/constants/theme.dart';
import 'package:message_za_lo/commons/widgets/widgets.dart';
import 'package:message_za_lo/features/account_detail/controller/account_detail_controller.dart';
import 'package:message_za_lo/features/account_detail/view/component/account_detail_background.dart';
import 'package:message_za_lo/features/account_detail/view/component/account_detail_change_information.dart';
import 'package:message_za_lo/features/account_detail/view/component/account_detail_information.dart';

class AccountDetailBody extends StatelessWidget {
  const AccountDetailBody({Key? key}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    Size size = MediaQuery.of(context).size;
    return GetBuilder<AccountDetailController>(
        init: AccountDetailController(),
        builder: (controller) {
          return Stack(
            children: [
              Column(
                children: [
                  AccountDetailBackGround(
                    size: size,
                    accountDetailController: controller,
                  ),
                  AccountDetailInformation(
                    size: size,
                    accountDetailController: controller,
                  ),
                  Expanded(
                      child: Container(
                    color: DefaultTheme.GREY
                        .withOpacity(Numeral.OPACITY_COLOR_GREY),
                  )),
                ],
              ),
              AccountDetailChangeInformation(
                accountDetailController: controller,
                size: size,
              ),
              ((!controller.checkShowAccountChange))
                  ? Container(
                      alignment: Alignment.bottomCenter,
                      height: size.height * 0.27 +
                          Numeral.HEIGHT_CONTAINER_ACCOUNT_DETAIL_INFORMATION,
                      padding:
                          EdgeInsets.all(Numeral.PADDING_LEFT_RIGHT_PRIMARY),
                      child: _buttonChangeStack(controller, size),
                    )
                  : SizedBox(),
            ],
          );
        });
  }

  Widget _buttonChangeStack(
      AccountDetailController accountDetailController, Size size) {
    return ButtonWidget(
      buttonColor: DefaultTheme.GREY.withOpacity(Numeral.OPACITY_COLOR_GREY),
      text: 'Chỉnh sửa',
      onTap: () {
        accountDetailController.setCheckShowAccountChange(true);
      },
      textStyle: null,
      height: 30,
      width: size.width,
    );
  }
}
