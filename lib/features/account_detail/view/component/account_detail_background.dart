import 'package:flutter/material.dart';
import 'package:message_za_lo/commons/constants/numeral.dart';
import 'package:message_za_lo/commons/constants/theme.dart';
import 'package:message_za_lo/commons/routes/image_asset.dart';
import 'package:message_za_lo/commons/widgets/widgets.dart';
import 'package:message_za_lo/features/account_detail/controller/account_detail_controller.dart';
import 'package:message_za_lo/services/shared_preferences/account_helper.dart';

class AccountDetailBackGround extends StatelessWidget {
  const AccountDetailBackGround(
      {Key? key, required this.size, required this.accountDetailController})
      : super(key: key);
  final Size size;
  final AccountDetailController accountDetailController;

  @override
  Widget build(BuildContext context) {
    return Stack(
      children: [
        InkWell(
          onTap: () {
            modalSheetBackgroundImageWidget(context);
          },
          child: Image.network(AccountHelper.instance.getBackgroundUrl(),
            width: size.width,
            fit: BoxFit.cover,
            height: size.height * 0.27,
            errorBuilder: (BuildContext context, Object exception,
                StackTrace? stackTrace) {
              return const Text('😢');
            },
          ),
        ),
        Container(
          margin: EdgeInsets.only(
              top: size.height * 0.08,
              left: Numeral.PADDING_LEFT_RIGHT_PRIMARY - 7),
          child: InkWell(
              onTap: () {
                Navigator.of(context).pop();
              },
              child: Icon(
                Icons.arrow_back_ios_new,
                size: Numeral.SIZE_ICON_BACK,
                color: DefaultTheme.WHITE,
              )),
        ),
        Container(
          alignment: Alignment.bottomLeft,
          height: size.height * 0.27,
          padding: EdgeInsets.only(
              bottom: Numeral.PADDING_LEFT_RIGHT_PRIMARY,
              left: Numeral.PADDING_LEFT_RIGHT_PRIMARY),
          child: Row(
            crossAxisAlignment: CrossAxisAlignment.center,
            children: [
              InkWell(
                onTap: () {
                  modalSheetAvatarWidget(context);
                },
                child: Container(
                    height: 42,
                    width: 42,
                    margin: EdgeInsets.only(
                        right: Numeral.PADDING_LEFT_RIGHT_PRIMARY - 5),
                    decoration: BoxDecoration(
                      borderRadius: BorderRadius.all(Radius.circular(50)),
                      border: Border.all(
                          width: 2,
                          color: DefaultTheme.WHITE.withOpacity(0.97)),
                    ),
                    child: CircleAvatar(
                      backgroundImage: NetworkImage(
                          AccountHelper.instance.getAvatarUrl()),
                    )),
              ),
              Container(
                color: DefaultTheme.GREY.withOpacity(0.4),
                child: Text(
                  accountDetailController.profileDTO.fullName,
                  style: TextStyle(
                    shadows: <Shadow>[
                      Shadow(
                        offset: Offset(0.0, 0.0),
                        blurRadius: 5.0,
                        color: DefaultTheme.BLACK.withOpacity(0.2),
                      ),
                      Shadow(
                        offset: Offset(0.0, 0.0),
                        blurRadius: 5.0,
                        color: DefaultTheme.WHITE.withOpacity(0.2),
                      ),
                    ],
                    color: DefaultTheme.WHITE,
                    fontSize: Numeral.TEXT_NAME_ACCOUNT_DETAIL,
                    fontWeight: FontWeight.w700,
                  ),
                ),
              )
            ],
          ),
        ),
      ],
    );
  }

  modalSheetAvatarWidget(BuildContext context) {
    showModalBottomSheet(
        context: context,
        backgroundColor: DefaultTheme.WHITE.withOpacity(0),
        shape: RoundedRectangleBorder(
          borderRadius: BorderRadius.only(
              topLeft: Radius.circular(15.0), topRight: Radius.circular(15.0)),
        ),
        builder: (context) {
          return ModalSheetBottomWidget(
            size: size,
            title1: 'Ảnh đại diện',
            title2: 'Xem ảnh đại diện',
            title3: 'Chụp ảnh mới',
            title4: 'Chọn ảnh từ thiết bị',
            title5: 'Chọn ảnh đại diện có sẵn',
            typeSelect1: Numeral.VALUE_NOT_EXIST,
            typeSelect2: Numeral.TYPE_SELECT_IMAGE_SHOW_AVATAR,
            typeSelect3: Numeral.TYPE_SELECT_IMAGE_FROM_CAMERA,
            typeSelect4: Numeral.TYPE_SELECT_IMAGE_FROM_MACHINE,
            typeSelect5: Numeral.TYPE_SELECT_IMAGE_AVAILABLE,
            typeUpdateImage: Numeral.TYPE_UPDATE_PROFILE_IMAGE_AVATAR,
          );
        });
  }

  modalSheetBackgroundImageWidget(BuildContext context) {
    showModalBottomSheet(
        context: context,
        backgroundColor: DefaultTheme.WHITE.withOpacity(0),
        shape: RoundedRectangleBorder(
          borderRadius: BorderRadius.only(
              topLeft: Radius.circular(15.0), topRight: Radius.circular(15.0)),
        ),
        builder: (context) {
          return ModalSheetBottomWidget(
            size: size,
            title1: 'Ảnh bìa',
            title2: 'Xem ảnh bìa',
            title3: 'Chụp ảnh mới',
            title4: 'Chọn ảnh từ thiết bị',
            title5: '',
            typeSelect1: Numeral.VALUE_NOT_EXIST,
            typeSelect2: Numeral.TYPE_SELECT_IMAGE_SHOW_AVATAR,
            typeSelect3: Numeral.TYPE_SELECT_IMAGE_FROM_CAMERA,
            typeSelect4: Numeral.TYPE_SELECT_IMAGE_FROM_MACHINE,
            typeSelect5: Numeral.TYPE_SELECT_IMAGE_AVAILABLE,
            typeUpdateImage: Numeral.TYPE_UPDATE_PROFILE_IMAGE_BACKGROUND,
          );
        });
  }
}
