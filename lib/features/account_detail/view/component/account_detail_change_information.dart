import 'package:flutter/material.dart';
import 'package:message_za_lo/commons/constants/numeral.dart';
import 'package:message_za_lo/commons/constants/theme.dart';
import 'package:message_za_lo/commons/utils/form_validator.dart';
import 'package:message_za_lo/commons/widgets/widgets.dart';
import 'package:message_za_lo/features/account_detail/controller/account_detail_controller.dart';

class AccountDetailChangeInformation extends StatelessWidget {
  const AccountDetailChangeInformation(
      {Key? key, required this.accountDetailController, required this.size})
      : super(key: key);
  final AccountDetailController accountDetailController;
  final Size size;

  @override
  Widget build(BuildContext context) {
    return Container(
      alignment: Alignment.bottomCenter,
      child: AnimatedContainer(
        width: size.width,
        height:
            accountDetailController.checkShowAccountChange ? size.height : 400,
        color: accountDetailController.checkShowAccountChange
            ? DefaultTheme.WHITE
            : DefaultTheme.THEME_COLOR.withOpacity(0),
        duration: const Duration(milliseconds: 400),
        curve: Curves.fastOutSlowIn,
        child: (accountDetailController.checkShowAccountChange)
            ? InkWell(
                onTap: () {
                  FocusScope.of(context).unfocus();
                },
                child: Scaffold(
                  appBar: AppBar(
                      backgroundColor: DefaultTheme.THEME_COLOR,
                      elevation: 0,
                      leading: IconButton(
                        padding: const EdgeInsets.all(0),
                        icon: const Icon(
                          Icons.close,
                          size: 25,
                        ),
                        onPressed: () {
                          if (accountDetailController
                              .checkHideShowButtonSaveAccountChange) {
                            DialogWidget.instance.openConfirmDialog(
                                context: context,
                                title: '',
                                description: "Bạn có chắc chắn muốn hủy?",
                                function: () {
                                  accountDetailController
                                      .setCheckShowAccountChange(false);
                                  accountDetailController
                                      .setValueNamePhoneGender();
                                  accountDetailController
                                      .setCheckHideShowButtonSaveAccountChange(
                                          false);
                                });
                            ScaffoldMessenger.of(context).hideCurrentSnackBar();
                          } else {
                            ScaffoldMessenger.of(context).hideCurrentSnackBar();
                            accountDetailController
                                .setCheckShowAccountChange(false);
                          }
                        },
                      ),
                      title: Container(
                        alignment: Alignment.centerLeft,
                        child: Text("Thông tin cá nhân",
                            style: DefaultTheme.TEXT_STYLE_PRIMARY_WHITE),
                      )),
                  body: _bodyChangeAccountWidget(context),
                ),
              )
            : SizedBox(),
      ),
    );
  }

  Widget _bodyChangeAccountWidget(BuildContext context) {
    return Column(
      children: [
        Column(
          children: [
            SizedBox(
              height: 10,
            ),
            Padding(
              padding: const EdgeInsets.all(Numeral.PADDING_LEFT_RIGHT_PRIMARY),
              child: Row(
                crossAxisAlignment: CrossAxisAlignment.start,
                children: [
                  Container(
                      height: Numeral.SIZE_IMAGE_CLIP_RECT_ACCOUNT_CHANGE,
                      width: Numeral.SIZE_IMAGE_CLIP_RECT_ACCOUNT_CHANGE,
                      margin: EdgeInsets.only(
                          right: Numeral.PADDING_LEFT_RIGHT_PRIMARY),
                      child:  CircleAvatar(
                        backgroundImage:
                            NetworkImage(accountDetailController.profileDTO.avatarUrl),
                      )),
                  _columnChangeInformationWidget(context),
                ],
              ),
            ),
            Padding(
              padding: EdgeInsets.all(Numeral.PADDING_LEFT_RIGHT_PRIMARY),
              child:
                  (accountDetailController.checkHideShowButtonSaveAccountChange)
                      ? _buttonSaveChangeInformationShow(context)
                      : _buttonSaveChangeInformationHide(),
            ),
          ],
        ),
        Expanded(
            child: Container(
          color: DefaultTheme.GREY.withOpacity(Numeral.OPACITY_COLOR_GREY),
        )),
      ],
    );
  }

  Widget _columnChangeInformationWidget(BuildContext context) {
    return Expanded(
      child: Column(
        children: [
          TextFieldChangeInformationWidget(
            hintText: 'Nhập tên của bạn',
            controllerEditing: accountDetailController.nameEditController,
            onChanged: (value) {
              accountDetailController.checkChangeNamePhoneGender();
            },
          ),
          TextFieldChangeInformationWidget(
            hintText: 'Nhập ngày sinh của bạn',
            controllerEditing: accountDetailController.dateBirthEditController,
            onChanged: (value) {
              accountDetailController.checkChangeNamePhoneGender();
            },
          ),
          _optionGenderWidget(context)
        ],
      ),
    );
  }

  Widget _optionGenderWidget(BuildContext context) {
    return Container(
      margin: EdgeInsets.only(top: 10),
      child: Row(
        crossAxisAlignment: CrossAxisAlignment.center,
        children: [
          Row(
            crossAxisAlignment: CrossAxisAlignment.center,
            mainAxisAlignment: MainAxisAlignment.start,
            children: [
              Container(
                child: Radio(
                    visualDensity: const VisualDensity(
                      horizontal: VisualDensity.minimumDensity,
                      vertical: VisualDensity.minimumDensity,
                    ),
                    materialTapTargetSize: MaterialTapTargetSize.shrinkWrap,
                    value: accountDetailController.valueOptionGender['male'],
                    groupValue: accountDetailController.valueSelectedGender,
                    activeColor: DefaultTheme.THEME_COLOR,
                    onChanged: (dynamic value) {
                      FocusScope.of(context).unfocus();
                      accountDetailController
                          .setValueSelectedGender(Numeral.VALUE_GENDER_MALE);
                      accountDetailController.checkChangeNamePhoneGender();
                    }),
              ),
              SizedBox(
                width: 5,
              ),
              Text(
                "Nam",
                style: DefaultTheme.TEXT_SUB_BUTTON_BLACK,
              ),
            ],
          ),
          SizedBox(
            width: 20,
          ),
          Row(
            crossAxisAlignment: CrossAxisAlignment.center,
            children: [
              Radio(
                  visualDensity: const VisualDensity(
                    horizontal: VisualDensity.minimumDensity,
                    vertical: VisualDensity.minimumDensity,
                  ),
                  materialTapTargetSize: MaterialTapTargetSize.shrinkWrap,
                  value: accountDetailController.valueOptionGender['female'],
                  groupValue: accountDetailController.valueSelectedGender,
                  activeColor: DefaultTheme.THEME_COLOR,
                  onChanged: (dynamic value) {
                    FocusScope.of(context).unfocus();
                    accountDetailController
                        .setValueSelectedGender(Numeral.VALUE_GENDER_FEMALE);
                    accountDetailController.checkChangeNamePhoneGender();
                  }),
              SizedBox(
                width: 5,
              ),
              Text(
                "Nữ",
                style: DefaultTheme.TEXT_SUB_BUTTON_BLACK,
              ),
            ],
          ),
        ],
      ),
    );
  }

  Widget _buttonSaveChangeInformationHide() {
    return ButtonWidget(
      buttonColor: DefaultTheme.BACKGROUND_BUTTON_WIDGET_HIDE,
      text: 'Lưu',
      onTap: () {},
      textStyle: DefaultTheme.TEXT_SUB_BUTTON_WHITE,
      height: 35,
      width: size.width,
    );
  }

  Widget _buttonSaveChangeInformationShow(BuildContext context) {
    return ButtonWidget(
      buttonColor: DefaultTheme.THEME_COLOR,
      text: 'Lưu',
      onTap: () async {
        FocusScope.of(context).unfocus();
        if (!FormValidator.instance.checkLengthString(
            accountDetailController.nameEditController.text)) {
          DialogWidget.instance.openErrorDialog(
              context: context,
              title: "Lỗi",
              description: "Vui lòng nhập tên của bạn");
        } else if (accountDetailController
            .dateBirthEditController.text.isEmpty) {
          DialogWidget.instance.openErrorDialog(
              context: context,
              title: "Lỗi",
              description: "Vui lòng ngày sinh của bạn");
        } else {
          accountDetailController.updateProfileToFirebase(context);
        }
      },
      textStyle: DefaultTheme.TEXT_SUB_BUTTON_WHITE,
      height: 35,
      width: size.width,
    );
  }
}
