import 'package:flutter/material.dart';
import 'package:message_za_lo/commons/constants/numeral.dart';
import 'package:message_za_lo/commons/constants/theme.dart';
import 'package:message_za_lo/features/account_detail/controller/account_detail_controller.dart';

class AccountDetailInformation extends StatelessWidget {
  const AccountDetailInformation(
      {Key? key, required this.size, required this.accountDetailController})
      : super(key: key);
  final Size size;
  final AccountDetailController accountDetailController;

  @override
  Widget build(BuildContext context) {
    return Container(
      padding: EdgeInsets.all(Numeral.PADDING_LEFT_RIGHT_PRIMARY),
      width: size.width,
      color: DefaultTheme.WHITE,
      height: Numeral.HEIGHT_CONTAINER_ACCOUNT_DETAIL_INFORMATION,
      child: Column(
        crossAxisAlignment: CrossAxisAlignment.start,
        children: [
          Text(
            "Thông tin cá nhân",
            style:
                DefaultTheme.TEXT_STYLE_INFORMATION_ACCOUNT_DETAIL_BLACK_W500,
          ),
          SizedBox(
            height: 25,
          ),
          _boxGenderDateBirth("Giới tính",
              accountDetailController.getGender(), ''),
          SizedBox(
            height: 12,
          ),
          _boxGenderDateBirth("Ngày sinh",
              "${accountDetailController.profileDTO.dateBirth}", ''),
          SizedBox(
            height: 12,
          ),
          _boxGenderDateBirth(
              "Email", "${accountDetailController.profileDTO.email}", ''),
          SizedBox(
            height: 12,
          ),
          _boxGenderDateBirth(
              "Điện thoại",
              "${accountDetailController.profileDTO.phoneNumber}",
              'Số điện thoại chỉ hiển thị với người có lưu số bạn trong danh bạ máy'),
        ],
      ),
    );
  }

  Widget _boxGenderDateBirth(String title, String textPrimary, String textSub) {
    return Container(
      padding: EdgeInsets.only(bottom: 12),
      decoration: BoxDecoration(
          border: Border(
              bottom: (textSub == '')
                  ? BorderSide(
                      width: Numeral.SIZE_DIVIDE_LINE_ACCOUNT_DETAIL,
                      color: DefaultTheme.GREY
                          .withOpacity(Numeral.OPACITY_COLOR_GREY_DIVIDE))
                  : BorderSide.none)),
      child: Row(
        crossAxisAlignment: CrossAxisAlignment.start,
        children: [
          Container(
              width: 90,
              child: Text(
                title,
                style: DefaultTheme.TEXT_STYLE_INFORMATION_ACCOUNT_DETAIL_GREY,
              )),
          (textSub != '')
              ? Expanded(
                  child: Column(
                    crossAxisAlignment: CrossAxisAlignment.start,
                    children: [
                      Text(
                        textPrimary,
                        style: DefaultTheme
                            .TEXT_STYLE_INFORMATION_ACCOUNT_DETAIL_NORMAL,
                      ),
                      SizedBox(
                        height: 3,
                      ),
                      Text(
                        textSub,
                        style: DefaultTheme
                            .TEXT_STYLE_INFORMATION_ACCOUNT_DETAIL_GREY_SUB,
                      )
                    ],
                  ),
                )
              : Text(
                  textPrimary,
                  style:
                      DefaultTheme.TEXT_STYLE_INFORMATION_ACCOUNT_DETAIL_NORMAL,
                ),
        ],
      ),
    );
  }
}
