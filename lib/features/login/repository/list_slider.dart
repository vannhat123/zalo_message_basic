import 'package:message_za_lo/commons/routes/image_asset.dart';

class ListSlider{
  final List<String> imgList = [
    // 'assets/nhat_hero/DSC_0078aban.jpg',
    // 'assets/nhat_hero/DSC_0129aban.jpg',
    // 'assets/nhat_hero/DSC_0163aban.jpg',
    // 'assets/nhat_hero/DSC_0148aban.jpg',
    // 'assets/nhat_hero/DSC_0252aphong k trong album.jpeg',
    // 'assets/nhat_hero/DSC_0276aphong k trong album.jpeg',
    // 'assets/nhat_hero/DSC_0541aban.jpg',
    ImageAsset.IMAGE_SLIDER1,
    ImageAsset.IMAGE_SLIDER2,
    ImageAsset.IMAGE_SLIDER3,
    ImageAsset.IMAGE_SLIDER4,
    ImageAsset.IMAGE_SLIDER5,
  ];
}