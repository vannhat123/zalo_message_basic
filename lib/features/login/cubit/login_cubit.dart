import 'package:equatable/equatable.dart';
import 'package:flutter/cupertino.dart';
import 'package:flutter_bloc/flutter_bloc.dart';
import 'package:message_za_lo/commons/routes/routes.dart';
import 'package:message_za_lo/commons/widgets/dialog_widget.dart';
import 'package:message_za_lo/services/firebase/firestore_helper.dart';
import 'package:message_za_lo/services/internet/internet_helper.dart';
import 'package:message_za_lo/services/shared_preferences/account_helper.dart';

part 'login_state.dart';

class LoginCubit extends Cubit<LoginState> {
  LoginCubit() : super(LoginState.initial());

  void emailChanged(String value) {
    emit(state.copyWith(email: value));
  }

  void passwordChanged(String value) {
    emit(state.copyWith(password: value));
  }

  Future<void> loginEmailPassword(
      String email, String password, BuildContext context) async {
    bool checkLogin = false;
    bool checkInternet = await InternetHelper.instance.checkInternet(context);
    if (checkInternet) {
      emit(state.copyWith(status: LoginStatus.submitting));
      await Future.delayed(const Duration(milliseconds: 1000));
      checkLogin =
          await FireStoreHelper().signInWithEmailPassword(email, password);
      DialogWidget.instance.dismiss(context: context);
      if (checkLogin) {
        print("pass: ${AccountHelper.instance.getPasswordAccount()}");
        print("email: ${AccountHelper.instance.getEmailAccount()}");
        emit(state.copyWith(status: LoginStatus.authentication));
      } else {
        emit(state.copyWith(status: LoginStatus.error));
      }
    }
  }

  Future<void> logOutFirebase(BuildContext context) async {
    bool checkLogOut = false;
    bool checkInternet = await InternetHelper.instance.checkInternet(context);
    if (checkInternet) {
      checkLogOut = await FireStoreHelper().logOut();
      if (checkLogOut) {
        await AccountHelper.instance
            .setEmailPasswordAccount("", "");
        Navigator.of(context).pushNamedAndRemoveUntil(Routes.INITIAL, (route) => false);
        emit(state.copyWith(status: LoginStatus.unAuthentication));
      }
    }
  }
}
