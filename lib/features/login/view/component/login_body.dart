import 'package:flutter/material.dart';
import 'package:flutter_bloc/flutter_bloc.dart';
import 'package:get/get.dart';
import 'package:message_za_lo/commons/constants/numeral.dart';
import 'package:message_za_lo/commons/constants/theme.dart';
import 'package:message_za_lo/commons/routes/Routes.dart';
import 'package:message_za_lo/commons/widgets/text_field_widget.dart';
import 'package:message_za_lo/commons/widgets/widgets.dart';
import 'package:message_za_lo/features/login/controller/login_controller.dart';
import 'package:message_za_lo/features/login/cubit/login_cubit.dart';
import 'package:message_za_lo/features/login/view/component/login_footer.dart';
import 'package:flutter_gen/gen_l10n/app_localizations.dart';

class LoginBody extends StatelessWidget {
  const LoginBody({Key? key}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    Size size = MediaQuery.of(context).size;
    return GetBuilder<LoginController>(
        init: LoginController(),
        builder: (controller) {
          return BlocListener<LoginCubit, LoginState>(
            listener: (context, state) {
              if (state.status == LoginStatus.error) {
                controller.setCheckErrorPassword(true);
              }
              if (state.status == LoginStatus.authentication) {
                Navigator.of(context).pushNamedAndRemoveUntil(Routes.TAB_BAR, (route) => true);
              }
              if (state.status == LoginStatus.submitting) {
                DialogWidget.instance.openLoadingDialog(context: context);
              }
            },
            child: Column(
              children: [
                Container(
                    margin:
                        const EdgeInsets.only(bottom: Numeral.MARGIN_APP_BAR),
                    color: DefaultTheme.GREY_BUTTON,
                    padding: const EdgeInsets.only(
                        bottom: 7,
                        top: 7,
                        left: Numeral.PADDING_LEFT_RIGHT_PRIMARY),
                    alignment: Alignment.centerLeft,
                    child: const Text(
                      "Bạn có thể đăng nhập bằng số điện thoại",
                      style: DefaultTheme.TEXT_STYLE_SUB_BLACK_NORMAL,
                    )),
                Padding(
                  padding: const EdgeInsets.symmetric(
                      horizontal: Numeral.PADDING_LEFT_RIGHT_PRIMARY),
                  child: Column(
                    children: [
                      TextFieldWidget(
                        maxLength: Numeral.MAX_LENGTH_NUMBER_PHONE,
                        onChanged: (value) {
                          controller.setCheckErrorPassword(false);
                          if (value.isNotEmpty) {
                            controller.setValueCheckClearIconNumber(true);
                            controller.setCheckColorSignInButton(true);
                          } else {
                            controller.setCheckColorSignInButton(false);
                            controller.setValueCheckClearIconNumber(false);
                          }
                        },
                        suffixIcon: (controller.checkClearIconNumber)
                            ? InkWell(
                                onTap: () {
                                  controller.setCheckErrorPassword(false);
                                  controller.phoneNumberController.text = '';
                                  controller
                                      .setValueCheckClearIconNumber(false);
                                  controller.setCheckColorSignInButton(false);
                                },
                                child: const Icon(
                                  Icons.clear,
                                  size: 17,
                                ),
                              )
                            : const SizedBox(),
                        textInputType: TextInputType.text,
                        textEditingController: controller.phoneNumberController,
                        hintText: 'Số điện thoại hoặc email',
                      ),
                      const SizedBox(
                        height: 20,
                      ),
                      TextFieldWidget(
                        maxLength: Numeral.MAX_LENGTH_PASSWORD,
                        onChanged: (value) {
                          controller.setCheckErrorPassword(false);
                          if (value.isNotEmpty) {
                            controller.setCheckClearIconPassword(true);
                            controller.setCheckColorSignInButton(true);
                          } else {
                            controller.setCheckColorSignInButton(false);
                            controller.setCheckClearIconPassword(false);
                          }
                        },
                        textInputType: TextInputType.text,
                        obscureText: controller.checkObscureText,
                        textEditingController: controller.passwordController,
                        hintText: AppLocalizations.of(context).password,
                        suffixIcon: Container(
                          width: 85,
                          alignment: Alignment.centerRight,
                          child: Row(
                            children: [
                              SizedBox(
                                width: 20,
                                child: (controller.checkClearIconPassword)
                                    ? InkWell(
                                        onTap: () {
                                          controller
                                              .setCheckErrorPassword(false);
                                          controller.passwordController.text =
                                              '';
                                          controller
                                              .setCheckClearIconPassword(false);
                                          controller
                                              .setCheckColorSignInButton(false);
                                        },
                                        child: const Icon(
                                          Icons.clear,
                                          size: 17,
                                        ),
                                      )
                                    : const SizedBox(),
                              ),
                              const Spacer(),
                              InkWell(
                                  onTap: () {
                                    controller.setValueCheckObscureText(null);
                                  },
                                  child: Container(
                                      margin: const EdgeInsets.only(
                                          right: Numeral
                                              .PADDING_LEFT_RIGHT_PRIMARY),
                                      alignment: Alignment.centerRight,
                                      child: (controller.checkObscureText)
                                          ? const Text(
                                              'HIỆN',
                                              style: DefaultTheme
                                                  .TEXT_STYLE_HINT_FIELD,
                                            )
                                          : const Text(
                                              'ẨN',
                                              style: DefaultTheme
                                                  .TEXT_STYLE_HINT_FIELD,
                                            ))),
                            ],
                          ),
                        ),
                      ),
                      (controller.checkErrorPassword)
                          ? Container(
                              margin: const EdgeInsets.only(top: 10),
                              alignment: Alignment.centerLeft,
                              child: const Text(
                                "Có lỗi xảy ra. Hãy bấm \"Lấy lại mật khẩu\" để xác thực",
                                style: DefaultTheme.TEXT_STYLE_ERROR_PASSWORD,
                                overflow: TextOverflow.ellipsis,
                              ))
                          : const SizedBox(
                              height: 5,
                            ),
                      InkWell(
                        onTap: () {
                          FocusScope.of(context).unfocus();
                        },
                        child: Container(
                            margin: const EdgeInsets.symmetric(
                                vertical: Numeral.PADDING_LEFT_RIGHT_PRIMARY),
                            alignment: Alignment.centerLeft,
                            child: const Text(
                              "Lấy lại mật khẩu",
                              style: DefaultTheme.TEXT_STYLE_NORMAL_THEME_COLOR,
                            )),
                      ),
                      const SizedBox(
                        height: 20,
                      ),
                      (controller.checkColorSignInButton)
                          ? ButtonWidget(
                              width: size.width * 0.5,
                              height:
                                  size.width * Numeral.SIZE_HEIGHT_BUTTON_ONE,
                              text: "Đăng nhập",
                              textStyle:
                                  DefaultTheme.TEXT_STYLE_NORMAL_WHITE_COLOR,
                              buttonColor: DefaultTheme.THEME_COLOR,
                              onTap: () {
                                FocusScope.of(context)
                                    .requestFocus(FocusNode());
                                if (controller.passwordController.text
                                        .trim()
                                        .length <
                                    6) {
                                  controller.setCheckErrorPassword(true);
                                } else {
                                  controller.setCheckErrorPassword(false);
                                  context.read<LoginCubit>().loginEmailPassword(
                                      controller.phoneNumberController.text
                                          .trim(),
                                      controller.passwordController.text.trim(),
                                      context);
                                }
                              })
                          : ButtonWidget(
                              width: size.width * 0.5,
                              height:
                                  size.width * Numeral.SIZE_HEIGHT_BUTTON_ONE,
                              text: "Đăng nhập",
                              textStyle: DefaultTheme.TEXT_STYLE_BUTTON_HINT,
                              buttonColor:
                                  DefaultTheme.BACKGROUND_BUTTON_WIDGET_HIDE,
                              onTap: () {}),
                    ],
                  ),
                ),
                const Spacer(),
                InkWell(
                    onTap: () {
                      FocusScope.of(context).unfocus();
                    },
                    child: const LoginFooter())
              ],
            ),
          );
        });
  }
}
