import 'package:flutter/material.dart';
import 'package:message_za_lo/commons/constants/numeral.dart';
import 'package:message_za_lo/commons/constants/theme.dart';

class LoginFooter extends StatelessWidget {
  const LoginFooter({Key? key}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return Container(
      margin: const EdgeInsets.only(bottom: Numeral.MARGIN_BOTTOM_FOOTER),
      child: const Text(
        "Các câu hỏi thường gặp",
        style: TextStyle(
            decoration: TextDecoration.underline,
            fontSize: Numeral.TEXT_SUB_NORMAL,
            color: DefaultTheme.GREY_TEXT_BUTTON),
      ),
    );
  }
}
