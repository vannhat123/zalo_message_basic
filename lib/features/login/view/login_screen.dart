import 'package:flutter/material.dart';
import 'package:message_za_lo/commons/widgets/appbar_widget.dart';
import 'package:message_za_lo/features/login/view/component/login_body.dart';
import 'package:flutter_gen/gen_l10n/app_localizations.dart';

class LoginScreen extends StatelessWidget {
  const LoginScreen({Key? key}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return Scaffold(
        appBar: AppBarWidget(
          title: AppLocalizations.of(context).login,
        ),
        body: LoginBody()
    );
  }
}
