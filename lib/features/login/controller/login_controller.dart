import 'package:flutter/material.dart';
import 'package:get/get.dart';

class LoginController extends GetxController {
  final phoneNumberController = TextEditingController();
  final passwordController = TextEditingController();
  bool checkObscureText = true;
  bool checkClearIconNumber = false;
  bool checkClearIconPassword = false;
  bool checkColorSignInButton = false;
  bool checkErrorPassword = false;

  void setValueCheckObscureText(bool? value) {
    if (value != null) {
      checkObscureText = value;
    } else {
      checkObscureText = !checkObscureText;
    }
    update();
  }

  void setValueCheckClearIconNumber(bool value) {
    checkClearIconNumber = value;
    update();
  }

  void setCheckClearIconPassword(bool value) {
    checkClearIconPassword = value;
    update();
  }

  void setCheckColorSignInButton(bool? value) {
    if (phoneNumberController.text.trim().isNotEmpty &&
        passwordController.text.trim().isNotEmpty && value == true) {
      checkColorSignInButton = true;
    } else {
      checkColorSignInButton = false;
    }
    update();
  }

  void setCheckErrorPassword(bool value){
    checkErrorPassword = value;
    update();
  }

}
