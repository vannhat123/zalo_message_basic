import 'package:flutter/material.dart';
import 'package:message_za_lo/commons/constants/numeral.dart';
import 'package:message_za_lo/commons/constants/theme.dart';
import 'package:message_za_lo/commons/routes/image_asset.dart';

class FriendContactBody extends StatelessWidget {
  const FriendContactBody({Key? key}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return Column(
      children: [
        Container(
          child: _headWidget(),
        ),
        Expanded(
          child: ListView.builder(
              itemCount: 15,
              itemBuilder: (BuildContext context, int index) {
                return (index != 0) ? _listContact() : _selectTypeShowList();
              }),
        )
      ],
    );
  }

  Widget _headWidget() {
    return Column(
      children: [
        Padding(
          padding: const EdgeInsets.only(
              left: Numeral.PADDING_LEFT_RIGHT_PRIMARY,
              right: Numeral.PADDING_LEFT_RIGHT_PRIMARY,
              top: Numeral.PADDING_LEFT_RIGHT_PRIMARY),
          child: Column(
            children: [
              Row(
                crossAxisAlignment: CrossAxisAlignment.center,
                children: const [
                  Icon(Icons.account_box_rounded,size: 40,color: DefaultTheme.THEME_COLOR,),
                  SizedBox(
                    width: 10,
                  ),
                  Text(
                    "Lời mời kết bạn",
                    style: DefaultTheme.TEXT_SUB_BUTTON_BLACK,
                  )
                ],
              ),
              const SizedBox(
                height: 30,
              ),
              Row(
                crossAxisAlignment: CrossAxisAlignment.center,
                children: [
                  Icon(Icons.perm_contact_calendar_outlined,size: 40,color: DefaultTheme.THEME_COLOR,),
                  const SizedBox(
                    width: 10,
                  ),
                  Column(
                    crossAxisAlignment: CrossAxisAlignment.start,
                    children: const [
                      Text(
                        "Danh bạ máy",
                        style: DefaultTheme.TEXT_SUB_BUTTON_BLACK,
                      ),
                      SizedBox(
                        height: 5,
                      ),
                      Text(
                        "Liên hệ có dùng zalo",
                        style: DefaultTheme.TEXT_STYLE_SMALL_GREY_COLOR,
                      ),
                    ],
                  )
                ],
              ),
            ],
          ),
        ),
        Container(
          margin: const EdgeInsets.symmetric(vertical: 15),
          color: DefaultTheme.GREY.withOpacity(Numeral.OPACITY_COLOR_GREY),
          height: 7,
        ),
      ],
    );
  }

  Widget _listContact() {
    return Container(
      margin: const EdgeInsets.symmetric(
          horizontal: Numeral.PADDING_LEFT_RIGHT_PRIMARY, vertical: 13),
      child: Row(
        children: [
          Container(
              margin: const EdgeInsets.only(right: 10),
              height: 45,
              width: 45,
              child: const CircleAvatar(
                backgroundImage:
                    AssetImage(ImageAsset.AVATAR_MESSAGE_MEDIA_BOX),
              )),
          const SizedBox(
            width: 10,
          ),
          const Expanded(
              child: Text(
            "Hero Kalel",
            style: DefaultTheme.TEXT_SUB_BUTTON_BLACK,
            overflow: TextOverflow.ellipsis,
          )),
          const SizedBox(
            width: 20,
          ),
          const Icon(
            Icons.phone,
            size: 20,
            color: DefaultTheme.GREY,
          ),
          const SizedBox(
            width: 12,
          ),
          const Icon(Icons.video_call, size: 25, color: DefaultTheme.GREY),
        ],
      ),
    );
  }

  Widget _selectTypeShowList() {
    return Row(
      children: [
        Container(
          margin: const EdgeInsets.symmetric(horizontal: Numeral.PADDING_LEFT_RIGHT_PRIMARY),
          padding: const EdgeInsets.symmetric(horizontal: 11, vertical: 11),
          decoration: BoxDecoration(
              borderRadius: const BorderRadius.all(Radius.circular(30)),
              color: DefaultTheme.GREY.withOpacity(0.12),
          ),
          child: const Text(
            "Tất cả  215",
            style: DefaultTheme.TEXT_SUB_BUTTON_BLACK,
          ),
        ),
        Container(
          padding: const EdgeInsets.symmetric(horizontal: 11, vertical: 10.5),
          decoration: BoxDecoration(
              borderRadius: const BorderRadius.all(Radius.circular(30)),
              color: DefaultTheme.WHITE,
              border: Border.all(width: 1, color: DefaultTheme.GREY.withOpacity(0.12))),
          child: const Text(
            "Mới truy cập 30",
            style: DefaultTheme.TEXT_SUB_BUTTON_GREY,
          ),
        ),
      ],
    );
  }
}
