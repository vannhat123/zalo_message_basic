import 'package:flutter/material.dart';
import 'package:message_za_lo/features/home/contact/friend_contact/view/component/friend_contact_body.dart';
class FriendContactScreen extends StatelessWidget {
  const FriendContactScreen({Key? key}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return const FriendContactBody();
  }
}
