import 'package:flutter/material.dart';
import 'package:message_za_lo/features/home/contact/oa_contact/view/component/oa_contact_body.dart';
class OaContactScreen extends StatelessWidget {
  const OaContactScreen({Key? key}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return OaContactBody();
  }
}
