import 'package:flutter/material.dart';
import 'package:message_za_lo/commons/constants/numeral.dart';
import 'package:message_za_lo/commons/constants/theme.dart';
import 'package:message_za_lo/features/home/contact/friend_contact/view/friend_contact_screen.dart';
import 'package:message_za_lo/features/home/contact/group_contact/view/group_contact_screen.dart';
import 'package:message_za_lo/features/home/contact/oa_contact/view/oa_contact_screen.dart';

class ContactBody extends StatefulWidget {
  const ContactBody({Key? key}) : super(key: key);

  @override
  State<ContactBody> createState() => _ContactBodyState();
}

class _ContactBodyState extends State<ContactBody> {
  @override
  void initState() {
    super.initState();
  }

  @override
  Widget build(BuildContext context) {
    return DefaultTabController(
      length: 3,
      child: Scaffold(
        appBar: AppBar(
          shadowColor: DefaultTheme.GREY.withOpacity(0.12),
          backgroundColor: DefaultTheme.GREY_BORDER,
          toolbarHeight: 0,
          bottom: const TabBar(
            padding: EdgeInsets.symmetric(
                horizontal: Numeral.PADDING_LEFT_RIGHT_PRIMARY),
            indicatorColor: DefaultTheme.THEME_COLOR,
            unselectedLabelColor: DefaultTheme.GREY,
            labelColor: DefaultTheme.BLACK,
            tabs: [
              Tab(
                child: Text(
                  "BẠN BÈ",
                  style: DefaultTheme.TEXT_STYLE_TAB_BAR_CONTACT,
                ),
              ),
              Tab(
                child: Text("NHÓM",
                    style: DefaultTheme.TEXT_STYLE_TAB_BAR_CONTACT),
              ),
              Tab(
                child:
                    Text("OA", style: DefaultTheme.TEXT_STYLE_TAB_BAR_CONTACT),
              ),
            ],
          ),
        ),
        body: const TabBarView(
          children: [
            FriendContactScreen(),
            GroupContactScreen(),
            OaContactScreen(),
          ],
        ),
      ),
    );
  }
}
