import 'package:flutter/material.dart';
import 'package:message_za_lo/features/home/contact/view-contact/component/contact_body.dart';
class ContactScreen extends StatelessWidget {
  const ContactScreen({Key? key}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return const ContactBody();
  }
}
