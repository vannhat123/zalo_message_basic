import 'package:flutter/material.dart';
import 'package:get/get.dart';
import 'package:intl/intl.dart';
import 'package:message_za_lo/commons/constants/numeral.dart';
import 'package:message_za_lo/commons/constants/theme.dart';
import 'package:message_za_lo/features/home/chat/controller/chat_controller.dart';
import 'package:message_za_lo/models/chat_individual_dto.dart';
import 'package:message_za_lo/services/firebase/firestore_helper.dart';

class ChatFooter extends StatelessWidget {
  ChatFooter({Key? key, required this.size}) : super(key: key);
  final Size size;

  @override
  Widget build(BuildContext context) {
    Size size = MediaQuery.of(context).size;
    return GetBuilder<ChatController>(
        init: ChatController(),
        builder: (controller) {
          return Column(
            children: [
              Container(
                alignment: Alignment.bottomCenter,
                color: DefaultTheme.WHITE,
                height: controller.heightTextMessage + size.height * 0.05,
                padding: EdgeInsets.symmetric(
                    horizontal: Numeral.PADDING_LEFT_RIGHT_PRIMARY),
                child: Row(
                  crossAxisAlignment: CrossAxisAlignment.end,
                  children: [
                    Padding(
                      padding: const EdgeInsets.only(bottom: 10),
                      child: _iconChatWidget(Icons.mood, 25),
                    ),
                    Expanded(
                        child: Container(
                      height: size.height * 0.05 + controller.heightTextMessage,
                      margin: const EdgeInsets.symmetric(horizontal: 10),
                      child: Stack(
                        children: [
                          Container(
                            key: controller.keyContainerPrimary,
                          ),
                          Scrollbar(
                            controller: controller.chatScrollController,
                            child: TextField(
                              scrollController: controller.chatScrollController,
                              onTap: () {
                                if (!controller.checkFocusChatController) {
                                  controller.setCheckFocusChatController(true);
                                }
                              },
                              controller: controller.textChatController,
                              onChanged: (value) {
                                if (!controller.checkInputChat) {
                                  controller.setCheckInputChat(true);
                                }
                                if (value == '' && controller.checkInputChat) {
                                  controller.setCheckInputChat(false);
                                }
                                controller.numLines =
                                    '\n'.allMatches(value).length + 1;
                                if (controller.numLines >
                                    controller.copyNumLines) {
                                  controller
                                      .setValueCheckWidthTextAndSetHeightTextField(
                                    Numeral.TEXT_PRIMARY,
                                    true,
                                  );
                                } else if (controller.numLines <
                                    controller.copyNumLines) {
                                  controller
                                      .setValueCheckWidthTextAndSetHeightTextField(
                                    Numeral.TEXT_PRIMARY,
                                    false,
                                  );
                                } else {
                                  controller
                                      .setValueCheckWidthTextAndSetHeightTextField(
                                          Numeral.TEXT_PRIMARY, null);
                                }
                                controller.copyNumLines = controller.numLines;
                              },
                              maxLines: 4,
                              style: DefaultTheme.TEXT_STYLE_PRIMARY_BLACK,
                              keyboardType: TextInputType.multiline,
                              decoration: InputDecoration(
                                contentPadding:
                                    EdgeInsets.only(left: 10, top: 5),
                                hintText: "Tin nhắn",
                                disabledBorder: InputBorder.none,
                                enabledBorder: const UnderlineInputBorder(
                                    borderRadius: BorderRadius.zero,
                                    borderSide: BorderSide.none),
                                focusedBorder: const UnderlineInputBorder(
                                    borderRadius: BorderRadius.zero,
                                    borderSide: BorderSide.none),
                                suffixIcon: (controller.checkInputChat)
                                    ? Container(
                                        key: controller.keyIconSub,
                                        padding: EdgeInsets.only(bottom: 10),
                                        alignment: Alignment.bottomCenter,
                                        width: 10,
                                        child: Row(
                                          children: [
                                            Spacer(),
                                            InkWell(
                                              onTap: () {
                                                FocusScope.of(context)
                                                    .requestFocus(FocusNode());
                                                controller.dateTimeNow =
                                                    DateTime.now();
                                                controller.date = DateFormat(
                                                        'yyyy/MM/dd')
                                                    .format(
                                                        controller.dateTimeNow);
                                                if (controller
                                                        .dateTimeNow.hour <
                                                    10) {
                                                  controller.hour =
                                                      "0${controller.dateTimeNow.hour}";
                                                } else {
                                                  controller.hour =
                                                      "${controller.dateTimeNow.hour}";
                                                }
                                                if (controller
                                                        .dateTimeNow.minute <
                                                    10) {
                                                  controller.minutes =
                                                      "0${controller.dateTimeNow.minute}";
                                                } else {
                                                  controller.minutes =
                                                      "${controller.dateTimeNow.minute}";
                                                }
                                                controller.sendingTime =
                                                    controller.dateTimeNow
                                                        .millisecondsSinceEpoch;
                                                controller.hourFormat =
                                                    "${controller.hour}:${controller.minutes}";
                                                FireStoreHelper()
                                                    .sendMessageToFirebase(
                                                        ChatIndividualDTO(
                                                            date:
                                                                controller.date,
                                                            hourFormat:
                                                                controller
                                                                    .hourFormat,
                                                            sendingTime:
                                                                controller
                                                                    .sendingTime,
                                                            emailSend:
                                                                "emailSend",
                                                            messageContent:
                                                                controller
                                                                    .textChatController
                                                                    .text));
                                              },
                                              child: Icon(
                                                Icons.send_rounded,
                                                size: 25,
                                                color: DefaultTheme.THEME_COLOR,
                                              ),
                                            ),
                                          ],
                                        ),
                                      )
                                    : Container(
                                        padding: EdgeInsets.only(bottom: 10),
                                        alignment: Alignment.bottomCenter,
                                        width: 120,
                                        child: Row(
                                          crossAxisAlignment:
                                              CrossAxisAlignment.center,
                                          mainAxisAlignment:
                                              MainAxisAlignment.end,
                                          children: [
                                            Text(
                                              "○ ○ ○",
                                              style: DefaultTheme
                                                  .TEXT_STYLE_THREE_DOT_CHAT,
                                            ),
                                            Container(
                                                margin: EdgeInsets.symmetric(
                                                    horizontal: 10),
                                                child: _iconChatWidget(
                                                    Icons.mic, 25)),
                                            _iconChatWidget(Icons.image, 25),
                                          ],
                                        ),
                                      ),
                              ),
                            ),
                          ),
                        ],
                      ),
                    ))
                  ],
                ),
              ),
              (!controller.checkFocusChatController)
                  ? Container(
                      color: DefaultTheme.WHITE,
                      height: size.height * 0.05,
                    )
                  : const SizedBox(),
            ],
          );
        });
  }

  Widget _iconChatWidget(IconData iconData, double size) {
    return Icon(
      iconData,
      color: DefaultTheme.GREY_TEXT_BUTTON,
      size: 26,
    );
  }
}
