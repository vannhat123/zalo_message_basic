import 'package:flutter/material.dart';
import 'package:message_za_lo/features/home/chat/view/component/chat_footer.dart';
import 'package:message_za_lo/features/home/chat/view/component/chat_form_message.dart';

class ChatBody extends StatelessWidget {
  ChatBody({Key? key}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    Size size = MediaQuery.of(context).size;
    return Container(
      child: Column(
        children: [
          ChatFormMessage(),
          ChatFooter(size: size),
        ],
      ),
    );
  }
}
