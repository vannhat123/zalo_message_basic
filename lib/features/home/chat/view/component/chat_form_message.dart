import 'package:flutter/material.dart';
import 'package:get/get.dart';
import 'package:message_za_lo/commons/constants/numeral.dart';
import 'package:message_za_lo/commons/constants/theme.dart';
import 'package:message_za_lo/commons/routes/image_asset.dart';
import 'package:message_za_lo/features/home/chat/controller/chat_controller.dart';

class ChatFormMessage extends StatelessWidget {
  const ChatFormMessage({Key? key}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    Size size = MediaQuery.of(context).size;
    return Expanded(
        child: GestureDetector(
      onTap: () {
        Get.find<ChatController>().setCheckFocusChatController(false);
        FocusScope.of(context).requestFocus(FocusNode());
      },
      child: Container(
        padding: EdgeInsets.symmetric(
            horizontal: Numeral.PADDING_LEFT_RIGHT_PRIMARY),
        color: DefaultTheme.GREY.withOpacity(Numeral.OPACITY_COLOR_GREY),
        child: Column(
          children: [
            SizedBox(height: 10,),
            _boxMessageOtherWidget(
                size,
                "DateFormat is for formatting and parsing dates in a locale-sensitive manner. It allows the user to choose from a set of standard date time",
                "7:05",
                false),
            _boxMessageOtherWidget(
                size,
                "DateFormat is for formatting and parsing dates in a locale-sensitive manner. It allows the user to choose from a set of standard date time",
                "7:05",
                false),
            _boxMessageIndividualWidget(size, "It allows the user to choose from a set of standard ", "8:30", true),
            _boxMessageOtherWidget(
                size,
                "DateFormat is for formatting and parsing dates in a locale-sensitive manner. It allows the user to choose from a set of standard date time",
                "7:05",
                false),
            _boxMessageIndividualWidget(size, "It allows the user to choose from a set of standard ", "8:30", true)
          ],
        ),
      ),
    ));
  }

  Widget _boxMessageOtherWidget(Size size, String textMessagePrimary,
      String textTimeSub, bool checkMyMessage) {
    return Row(
      crossAxisAlignment: CrossAxisAlignment.start,
      children: [
        Container(
            margin: const EdgeInsets.only(right: 8),
            height: Numeral.SIZE_ICON_IN_CHAT,
            width: Numeral.SIZE_ICON_IN_CHAT,
            child: const CircleAvatar(
              backgroundImage: AssetImage(ImageAsset.AVATAR_MESSAGE_MEDIA_BOX),
            )),
        _boxMessageIndividualWidget(
            size, textMessagePrimary, textTimeSub, false),
      ],
    );
  }

  Widget _boxMessageIndividualWidget(Size size, String textMessagePrimary,
      String timeSub, bool checkMyMessage) {
    return Container(
      margin: EdgeInsets.only(bottom: 5),
      alignment:
          (checkMyMessage) ? Alignment.centerRight : Alignment.centerLeft,
      child: Container(
        constraints: BoxConstraints(maxWidth: size.width * 0.75),
        margin: EdgeInsets.only(top: 3),
        padding: EdgeInsets.all(8),
        decoration: BoxDecoration(
            border: Border.all(width: 1, color: DefaultTheme.GREY_BUTTON),
            color: (checkMyMessage)
                ? DefaultTheme.BACKGROUND_BOX_CHAT
                : DefaultTheme.WHITE,
            borderRadius: BorderRadius.all(Radius.circular(10))),
        child: Column(
          crossAxisAlignment: CrossAxisAlignment.start,
          children: [
            Text(
              textMessagePrimary,
              style: DefaultTheme.TEXT_STYLE_PRIMARY_BLACK_CHAT,
              overflow: TextOverflow.ellipsis,
              maxLines: 10,
            ),
            SizedBox(
              height: 4,
            ),
            Text(
              timeSub,
              style: DefaultTheme.TEXT_STYLE_SUB_GREY_CHAT,
            ),
          ],
        ),
      ),
    );
  }
}
