import 'package:flutter/material.dart';
import 'package:message_za_lo/commons/widgets/appbar_chat_widget.dart';
import 'package:message_za_lo/features/home/chat/view/component/chat_body.dart';
class ChatScreen extends StatelessWidget {
  const ChatScreen({Key? key}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return Scaffold(
     appBar: AppBarChatWidget(titleSub: 'Truy cập 24 phút trước', titlePrimary: 'Chopper ( Waiting) ',),
      body: ChatBody(),
    );
  }
}
