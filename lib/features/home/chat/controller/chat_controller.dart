import 'package:flutter/material.dart';
import 'package:get/get.dart';
import 'package:message_za_lo/commons/constants/numeral.dart';

class ChatController extends GetxController {
  bool checkInputChat = false;
  double heightTextMessage = 0;
  final textChatController = TextEditingController();
  bool checkFocusChatController = false;
  double valueCheckWidthTextAndSetHeightTextField = 0;
  int numLines = Numeral.VALUE_DEFAULT_COUNT_LINE_CHAT;
  final keyContainerPrimary = GlobalKey();
  final keyIconSub = GlobalKey();
  final chatScrollController = ScrollController();
  int copyNumLines = 1;
  double valueHeightEnterAdd = 0;
  DateTime dateTimeNow = DateTime.now();
  String date = '';
  String hour = '';
  String minutes = '';
  int sendingTime = 0;
  String hourFormat = '';

  void setCheckInputChat(bool value) {
    checkInputChat = value;
    update();
  }

  void setCheckFocusChatController(bool value) {
    checkFocusChatController = value;
    update();
  }

  void setValueCheckWidthTextAndSetHeightTextField(
      double fontSize, bool? checkEnter) {
    TextPainter textPainter = TextPainter()
      ..text = TextSpan(
          text: textChatController.text, style: TextStyle(fontSize: fontSize))
      ..textDirection = TextDirection.ltr
      ..layout(minWidth: 0, maxWidth: double.infinity);
    valueCheckWidthTextAndSetHeightTextField = textPainter.size.width;
    double resultDivisionWidthTextAndWidthWidget =
        valueCheckWidthTextAndSetHeightTextField / (getSize() - 10 - 7);
    if (checkEnter == true) {
      if (valueHeightEnterAdd < 60) {
        valueHeightEnterAdd += 20;
      }
    }
    if (checkEnter == false) {
      if (valueHeightEnterAdd > 0) {
        valueHeightEnterAdd -= 20;
      }
    }
    if (resultDivisionWidthTextAndWidthWidget < 1) {
      if (heightTextMessage + valueHeightEnterAdd <= 60) {
        heightTextMessage = 0 + valueHeightEnterAdd;
      } else {
        heightTextMessage = 60;
      }
    }
    if (resultDivisionWidthTextAndWidthWidget >= 1 &&
        resultDivisionWidthTextAndWidthWidget < 2) {
      if (heightTextMessage + valueHeightEnterAdd <= 60) {
        heightTextMessage = 20 + valueHeightEnterAdd;
      } else {
        heightTextMessage = 60;
      }
    }
    if (resultDivisionWidthTextAndWidthWidget >= 2 &&
        resultDivisionWidthTextAndWidthWidget < 3) {
      if (heightTextMessage + valueHeightEnterAdd <= 60) {
        heightTextMessage = 40 + valueHeightEnterAdd;
      } else {
        heightTextMessage = 60;
      }
    }
    if (resultDivisionWidthTextAndWidthWidget >= 3) {
      if (heightTextMessage < 60) {
        heightTextMessage = 60;
      }
    }
    update();
  }

  double getSize() {
    final sizePrimary = keyContainerPrimary.currentContext!.size;
    final sizeSub = keyIconSub.currentContext?.size;
    if (sizePrimary != null && sizeSub != null) {
      return sizePrimary.width - sizeSub.width;
    } else {
      return 0;
    }
  }

}
