import 'package:get/get.dart';

class TabBarController extends GetxController {
  int indexTabBar = 0;

  void setIndex(int index) {
    indexTabBar = index;
    update();
  }
}
