import 'package:flutter/material.dart';
import 'package:get/get.dart';
import 'package:message_za_lo/commons/constants/numeral.dart';
import 'package:message_za_lo/commons/constants/theme.dart';
import 'package:message_za_lo/commons/widgets/appbar_tabbar_widget.dart';
import 'package:message_za_lo/features/home/contact/view-contact/contact_screen.dart';
import 'package:message_za_lo/features/home/diary/view/diary_screen.dart';
import 'package:message_za_lo/features/home/discover/view/discover_screen.dart';
import 'package:message_za_lo/features/home/message/view/message_screen.dart';
import 'package:message_za_lo/features/home/profile/view/profile_screen.dart';
import 'package:message_za_lo/features/home/tabbar/controller/tabbar_controller.dart';
import 'package:message_za_lo/features/home/tabbar/view/component/tabbar_footer.dart';
import 'package:flutter_gen/gen_l10n/app_localizations.dart';

class TabBarScreen extends GetView<TabBarController> {
  const TabBarScreen({Key? key}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return Scaffold(
        appBar: const AppBarTabBarWidget(title: "Tìm kiếm"),
        body: Column(
          children: [
            GetBuilder<TabBarController>(
                init: TabBarController(),
                builder: (_) {
                  int index = controller.indexTabBar;
                  if (index == Numeral.INDEX_SELECT_MESSAGE) {
                    return const Expanded(child: MessageScreen());
                  }
                  if (index == Numeral.INDEX_SELECT_CONTACT) {
                    return const Expanded(child: ContactScreen());
                  }
                  if (index == Numeral.INDEX_SELECT_DISCOVER) {
                    return const Expanded(child: DiscoverScreen());
                  }
                  if (index == Numeral.INDEX_SELECT_DIARY) {
                    return const Expanded(child: DiaryScreen());
                  }
                  if (index == Numeral.INDEX_SELECT_PROFILE) {
                    return const Expanded(child: ProfileScreen());
                  }
                  return Center(
                    child: Text(
                      AppLocalizations.of(context)
                          .error_occurred_please_try_again,
                      style: DefaultTheme.TEXT_SUB_BUTTON_BLACK,
                    ),
                  );
                }),
             Divider(
              height: 1,
              color: DefaultTheme.GREY.withOpacity(Numeral.OPACITY_COLOR_GREY_DIVIDE),
            ),
            const SizedBox(height: 10),
            const TabBarFooter(),
          ],
        ));
  }
}
