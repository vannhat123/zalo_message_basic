import 'package:flutter/material.dart';
import 'package:get/get.dart';
import 'package:message_za_lo/commons/constants/numeral.dart';
import 'package:message_za_lo/commons/constants/theme.dart';
import 'package:message_za_lo/features/home/tabbar/controller/tabbar_controller.dart';
import 'package:flutter_gen/gen_l10n/app_localizations.dart';

class TabBarFooter extends GetView<TabBarController> {
  const TabBarFooter({Key? key}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    Size size = MediaQuery.of(context).size;
    return GetBuilder<TabBarController>(
        init: TabBarController(),
        builder: (controller) {
          return Container(
            height: size.height*0.1,
            padding: const EdgeInsets.symmetric(
                horizontal: Numeral.PADDING_LEFT_RIGHT_PRIMARY),
            child: Row(
              mainAxisAlignment: MainAxisAlignment.spaceBetween,
              children: [
                _iconAndTextWidget(
                    AppLocalizations.of(context).message,
                    (controller.indexTabBar == Numeral.INDEX_SELECT_MESSAGE)
                        ? const Icon(Icons.message,color: DefaultTheme.THEME_COLOR,)
                        : const Icon(Icons.message,color: DefaultTheme.GREY,),
                    Numeral.INDEX_SELECT_MESSAGE,
                    (controller.indexTabBar == Numeral.INDEX_SELECT_MESSAGE)
                        ?true: false),
                _iconAndTextWidget(
                    AppLocalizations.of(context).contact,
                    (controller.indexTabBar == Numeral.INDEX_SELECT_CONTACT)
                        ? const Icon(Icons.perm_contact_calendar_sharp,color: DefaultTheme.THEME_COLOR,)
                        : const Icon(Icons.perm_contact_calendar_sharp,color: DefaultTheme.GREY,),
                    Numeral.INDEX_SELECT_CONTACT,
                    (controller.indexTabBar == Numeral.INDEX_SELECT_CONTACT)
                        ?true: false),
                _iconAndTextWidget(
                    AppLocalizations.of(context).discover,
                    (controller.indexTabBar == Numeral.INDEX_SELECT_DISCOVER)
                        ? const Icon(Icons.auto_awesome_mosaic_outlined,color: DefaultTheme.THEME_COLOR,)
                        : const Icon(Icons.auto_awesome_mosaic_outlined,color: DefaultTheme.GREY,),
                    Numeral.INDEX_SELECT_DISCOVER,
                    (controller.indexTabBar == Numeral.INDEX_SELECT_DISCOVER)
                        ? true: false),
                _iconAndTextWidget(
                    AppLocalizations.of(context).diary,
                    (controller.indexTabBar == Numeral.INDEX_SELECT_DIARY)
                        ? const Icon(Icons.watch_later_outlined,color: DefaultTheme.THEME_COLOR,)
                        : const Icon(Icons.watch_later_outlined,color: DefaultTheme.GREY,),
                    Numeral.INDEX_SELECT_DIARY,
                    (controller.indexTabBar == Numeral.INDEX_SELECT_DIARY)
                        ? true:false),
                _iconAndTextWidget(
                    AppLocalizations.of(context).profile,
                    (controller.indexTabBar == Numeral.INDEX_SELECT_PROFILE)
                        ? const Icon(Icons.person,color: DefaultTheme.THEME_COLOR,)
                        : const Icon(Icons.person,color: DefaultTheme.GREY,),
                    Numeral.INDEX_SELECT_PROFILE,
                    (controller.indexTabBar == Numeral.INDEX_SELECT_PROFILE)
                        ? true: false),
              ],
            ),
          );
        });
  }

  Widget _iconAndTextWidget(
      String title, Icon icon, int index, bool checkSelect) {
    return InkWell(
      onTap: () {
        controller.setIndex(index);
      },
      child: Column(
        crossAxisAlignment: CrossAxisAlignment.center,
        children: [
          icon,
          const SizedBox(
            height: 5,
          ),
          Text(
            title,
            style: (checkSelect)?DefaultTheme.TEXT_STYLE_ICON_FOOTER_SHOW :DefaultTheme.TEXT_STYLE_ICON_FOOTER_HIDE,
          )
        ],
      ),
    );
  }
}
