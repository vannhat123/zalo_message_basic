import 'package:get/get.dart';
import 'package:message_za_lo/features/home/tabbar/controller/tabbar_controller.dart';

class TabBarBinding extends Bindings {
  @override
  void dependencies() {
    Get.put<TabBarController>(TabBarController());
  }
}
