import 'package:flutter/material.dart';
import 'package:message_za_lo/commons/constants/numeral.dart';
import 'package:message_za_lo/commons/constants/theme.dart';

class ClipRectImage extends StatelessWidget {
  ClipRectImage(
      {Key? key,
      required this.iconData,
      required this.title,
      required this.checkLast,
      required this.height,
      this.textStyle})
      : super(key: key);
  final String iconData;
  final String title;
  final bool checkLast;
  final double height;
  final TextStyle? textStyle;

  @override
  Widget build(BuildContext context) {
    return Stack(
      children: [
        ClipRRect(
          borderRadius: BorderRadius.circular(8.0),
          child: Image.asset(
            iconData,
            fit: BoxFit.cover,
            height: height,
            width: height,
          ),
        ),
        (checkLast)
            ? Container(
                alignment: Alignment.center,
                padding: EdgeInsets.symmetric(horizontal: 2, vertical: 3),
                height: height,
                width: height,
                decoration: BoxDecoration(
                  borderRadius: BorderRadius.all(Radius.circular(8)),
                  gradient: LinearGradient(
                      colors: [
                        Colors.black12,
                        Colors.black26,
                        Colors.black54,
                        Colors.black87
                      ],
                      begin: Alignment.topCenter,
                      end: Alignment.bottomCenter,
                      stops: [0, 0.2, 0.5, 0.8] //stops for individual color
                      //set the stops number equal to numbers of color
                      ),
                ),
                child: Column(
                  mainAxisAlignment: MainAxisAlignment.center,
                  children: [
                    Icon(
                      Icons.discord_outlined,
                      size: Numeral.SIZE_ICON_UTILITY_DISCOVER,
                      color: DefaultTheme.WHITE,
                    ),
                    Text(
                      title,
                      overflow: TextOverflow.ellipsis,
                      maxLines: 1,
                      style: textStyle ??
                          DefaultTheme.TEXT_STYLE_UTILITY_BOTTOM_ICON_WHITE,
                    )
                  ],
                ))
            : Container(
                alignment: Alignment.bottomCenter,
                height: height,
                width: height,
                child: Container(
                  alignment: Alignment.center,
                  padding: EdgeInsets.symmetric(horizontal: 4, vertical: 3),
                  height: height / 4,
                  width: height,
                  decoration: BoxDecoration(
                    borderRadius: BorderRadius.only(
                        bottomLeft: Radius.circular(8),
                        bottomRight: Radius.circular(8)),
                    gradient: LinearGradient(
                        colors: [
                          Colors.black12,
                          Colors.black26,
                          Colors.black54,
                          Colors.black87
                        ],
                        begin: Alignment.topCenter,
                        end: Alignment.bottomCenter,
                        stops: [0, 0.2, 0.5, 0.8] //stops for individual color
                        //set the stops number equal to numbers of color
                        ),
                  ),
                  child: Text(
                    title,
                    overflow: TextOverflow.ellipsis,
                    maxLines: 1,
                    style: textStyle ??
                        DefaultTheme.TEXT_STYLE_UTILITY_BOTTOM_ICON_WHITE,
                  ),
                ),
              )
      ],
    );
  }
}
