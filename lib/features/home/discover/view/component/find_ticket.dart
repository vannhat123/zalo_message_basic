import 'package:flutter/material.dart';
import 'package:message_za_lo/commons/constants/numeral.dart';
import 'package:message_za_lo/commons/constants/theme.dart';
import 'package:message_za_lo/features/home/discover/view/component/circle_background_icon.dart';

class FindTicket extends StatelessWidget {
  const FindTicket({Key? key, required this.size}) : super(key: key);
  final Size size;

  @override
  Widget build(BuildContext context) {
    return Container(
      padding: EdgeInsets.symmetric(
          horizontal: Numeral.PADDING_LEFT_RIGHT_PRIMARY,
          vertical: Numeral.PADDING_LEFT_RIGHT_PRIMARY),
      decoration: BoxDecoration(
        border: Border(
          bottom: BorderSide(
              width: Numeral.HEIGHT_DIVIDE_CONTAINER,
              color: DefaultTheme.GREY.withOpacity(Numeral.OPACITY_COLOR_GREY)),
        ),
      ),
      child: Column(
        children: [
          Row(
            children: [
              CircleBackgroundIcon(
                height: Numeral.HEIGHT_CIRCLE_CONTAINER_ICON_UTILITY,
                iconData: Icons.find_in_page_outlined,
                colorIconData: DefaultTheme.ORANGE,
                colorBackground: DefaultTheme.ORANGE.withOpacity(0.3),
              ),
              Container(
                margin: EdgeInsets.symmetric(horizontal: 8),
                child: Text("Dò vé số",
                    style: DefaultTheme.TEXT_STYLE_UTILITY_BOTTOM_ICON),
              ),
              Text("• Miền bắc, 8 tháng 1",
                  style: DefaultTheme.TEXT_STYLE_UTILITY_BOTTOM_ICON_GREY)
            ],
          ),
          Container(
            margin: EdgeInsets.only(top: Numeral.PADDING_LEFT_RIGHT_PRIMARY),
            height: size.width * 0.25,
            decoration: BoxDecoration(
                borderRadius: BorderRadius.all(Radius.circular(10)),
                border: Border.all(
                    width: Numeral.WIDTH_BORDER_FIND_TICKET,
                    color: DefaultTheme.ORANGE_ALLOW2),
                color: DefaultTheme.ORANGE_ALLOW1.withOpacity(0.7)),
            child: Column(
              mainAxisAlignment: MainAxisAlignment.spaceBetween,
              children: [
                Expanded(
                  flex: 1,
                  child: Row(
                    children: [
                      SizedBox(
                        width: 20,
                      ),
                      Text(
                        "18:35",
                        style: DefaultTheme
                            .TEXT_STYLE_UTILITY_BOTTOM_ICON_RED_W700,
                      ),
                      Text(
                        " hôm nay có kết quả xổ số",
                        style: DefaultTheme
                            .TEXT_STYLE_UTILITY_BOTTOM_ICON_RED_W300,
                      )
                    ],
                  ),
                ),
                Expanded(
                  flex: 1,
                  child: Container(
                    padding: EdgeInsets.symmetric(horizontal: 10),
                    margin: EdgeInsets.only(left: 10, right: 10, bottom: 10),
                    decoration: BoxDecoration(
                        color: DefaultTheme.ORANGE_ALLOW2.withOpacity(0.5),
                        borderRadius: BorderRadius.all(Radius.circular(10))),
                    child: Row(
                      crossAxisAlignment: CrossAxisAlignment.center,
                      children: [
                        Text(
                          "Dò kết quả xổ số hằng ngày",
                          style: DefaultTheme
                              .TEXT_STYLE_UTILITY_FIND_XO_SO_GREY_W300,
                        ),
                        Spacer(),
                        Text(
                          "Dò ngay  ",
                          style: DefaultTheme
                              .TEXT_STYLE_UTILITY_FIND_ORANGE_BOLD_W500,
                        ),
                        Icon(
                          Icons.arrow_forward_ios_sharp,
                          size: 10,
                          color: DefaultTheme.ORANGE_BOLD,
                        )
                      ],
                    ),
                  ),
                )
              ],
            ),
          )
        ],
      ),
    );
  }
}
