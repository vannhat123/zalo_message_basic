import 'package:flutter/material.dart';
import 'package:message_za_lo/commons/constants/numeral.dart';

class CircleBackgroundIcon extends StatelessWidget {
  const CircleBackgroundIcon(
      {Key? key,
      required this.iconData,
      required this.height,
      required this.colorBackground,
      required this.colorIconData})
      : super(key: key);
  final Color colorBackground;
  final IconData iconData;
  final double height;
  final Color colorIconData;

  @override
  Widget build(BuildContext context) {
    return Container(
        height: height,
        width: height,
        decoration: BoxDecoration(
            color: colorBackground,
            borderRadius: BorderRadius.all(Radius.circular(20))),
        child: Icon(
          iconData,
          size: Numeral.SIZE_ICON_GAME_DISCOVER,
          color: colorIconData,
        ));
  }
}
