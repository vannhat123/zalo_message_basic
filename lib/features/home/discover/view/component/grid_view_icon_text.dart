import 'package:flutter/material.dart';
import 'package:message_za_lo/commons/constants/numeral.dart';
import 'package:message_za_lo/commons/constants/theme.dart';
import 'package:message_za_lo/features/home/discover/view/component/icon_text_utility.dart';
class GridViewIconText extends StatelessWidget {
  const GridViewIconText({Key? key}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return Container(
      margin: const EdgeInsets.symmetric(
          horizontal: Numeral.PADDING_LEFT_RIGHT_PRIMARY),
      child: GridView.count(
        crossAxisCount: 4,
        children: [
          IconTextUtility(
            iconData: Icons.shopping_cart_sharp,
            title: 'Shop',
            colorIcon: DefaultTheme.RED.withOpacity(0.5),
          ),
          IconTextUtility(
            iconData: Icons.maps_home_work_rounded,
            title: 'Home & Car',
            colorIcon: DefaultTheme.BLUE,
          ),
          IconTextUtility(
            iconData: Icons.sentiment_satisfied_alt_outlined,
            title: 'Sticker',
            colorIcon: DefaultTheme.VIOLET.withOpacity(0.8),
          ),
          IconTextUtility(
            iconData: Icons.door_back_door,
            title: 'Dịch vụ công',
            colorIcon: DefaultTheme.THEME_COLOR,
          ),
          IconTextUtility(
            iconData: Icons.payment_outlined,
            title: 'Ví ZaloPay',
            colorIcon: DefaultTheme.THEME_GRADIENT2,
          ),
          IconTextUtility(
            iconData: Icons.phone_iphone_outlined,
            title: 'Nạp tiền ĐT',
            colorIcon: DefaultTheme.THEME_GRADIENT0,
          ),
          IconTextUtility(
            iconData: Icons.monetization_on_outlined,
            title: 'Trả Hóa Đơn',
            colorIcon: DefaultTheme.ORANGE,
          ),
          IconTextUtility(
            iconData: Icons.sentiment_satisfied_alt_outlined,
            title: 'Fiza',
            colorIcon: DefaultTheme.VIOLET.withOpacity(0.4),
          ),
          IconTextUtility(
            iconData: Icons.yard_outlined,
            title: 'Tích lũy',
            colorIcon: DefaultTheme.PINK_ICON,
          ),
          IconTextUtility(
            iconData: Icons.living_outlined,
            title: 'Mini Apps',
            colorIcon: DefaultTheme.THEME_GRADIENT2,
          ),
        ],
        shrinkWrap: true,
        mainAxisSpacing: 10,
        crossAxisSpacing: 22,
      ),
    );
  }
}
