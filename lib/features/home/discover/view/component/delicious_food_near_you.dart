import 'package:flutter/material.dart';
import 'package:message_za_lo/commons/constants/numeral.dart';
import 'package:message_za_lo/commons/constants/theme.dart';
import 'package:message_za_lo/features/home/discover/view/component/circle_background_icon.dart';
import 'package:message_za_lo/features/home/discover/view/component/icon_text_utility.dart';

class DeliciousFoodNearYou extends StatelessWidget {
  const DeliciousFoodNearYou({Key? key, required this.size}) : super(key: key);
  final Size size;

  @override
  Widget build(BuildContext context) {
    return Container(
      padding: EdgeInsets.symmetric(
          horizontal: Numeral.PADDING_LEFT_RIGHT_PRIMARY,vertical: Numeral.PADDING_LEFT_RIGHT_PRIMARY),
      decoration: BoxDecoration(
        border: Border(
          bottom: BorderSide(
              width: Numeral.HEIGHT_DIVIDE_CONTAINER,
              color: DefaultTheme.GREY.withOpacity(Numeral.OPACITY_COLOR_GREY)),
        ),
      ),
      child: Column(
        children: [
          Row(
            children: [
              CircleBackgroundIcon(
                height: Numeral.HEIGHT_CIRCLE_CONTAINER_ICON_UTILITY,
                iconData: Icons.add_home_sharp,
                colorIconData: DefaultTheme.ORANGE,
                colorBackground: DefaultTheme.ORANGE.withOpacity(0.3),
              ),
              Container(
                margin: EdgeInsets.symmetric(horizontal: 8),
                child: Text("Món ngon gần bạn trên Zalo Connect",
                    style: DefaultTheme.TEXT_STYLE_UTILITY_BOTTOM_ICON),
              ),
            ],
          ),
          Container(
            margin: EdgeInsets.only(top: Numeral.PADDING_LEFT_RIGHT_PRIMARY),
            height: size.width * 0.31,
            decoration: BoxDecoration(
              borderRadius: BorderRadius.all(Radius.circular(10)),
              color: Colors.grey.withOpacity(0.3),
            ),
            child: Stack(
              children: [
                // ClipRRect(
                //   borderRadius: BorderRadius.circular(8.0),
                //   child: Image.asset(
                //     ImageAsset.IMAGE_SLIDER3,
                //     fit: BoxFit.cover,
                //     height: size.width * 0.3,
                //     width: size.width,
                //   ),
                // ),
                Column(
                  children: [
                    Container(
                      padding: EdgeInsets.only(left: 10),
                      alignment: Alignment.centerLeft,
                      margin:
                          EdgeInsets.only(left:Numeral.PADDING_LEFT_RIGHT_PRIMARY,right: Numeral.PADDING_LEFT_RIGHT_PRIMARY,top: Numeral.PADDING_LEFT_RIGHT_PRIMARY),
                      width: size.width,
                      height: 30,
                      decoration: BoxDecoration(
                          borderRadius: BorderRadius.all(Radius.circular(20)),
                          color: DefaultTheme.WHITE),
                      child: Row(
                        crossAxisAlignment: CrossAxisAlignment.center,
                        children: [
                          Icon(
                            Icons.search_sharp,
                            size: 20,
                            color: DefaultTheme.GREY,
                          ),
                          SizedBox(
                            width: 10,
                          ),
                          Text(
                            "Lạp xưởng",
                            style: DefaultTheme
                                .TEXT_STYLE_UTILITY_DELICIOUS_NEAR_YOU,
                          ),
                        ],
                      ),
                    ),
                    Expanded(
                      child: Padding(
                       padding: const EdgeInsets.symmetric(horizontal: Numeral.PADDING_LEFT_RIGHT_PRIMARY*2),
                        child: Column(
                          children: [
                            Spacer(),
                            _listIconDeliciousWidget(),
                            Spacer(),
                          ],
                        ),
                      ),
                    )
                  ],
                )
              ],
            ),
          )
        ],
      ),
    );
  }

  Widget _listIconDeliciousWidget(){
    return Row(
      mainAxisAlignment: MainAxisAlignment.spaceBetween,
      children: [
        IconTextUtility(
          title: 'Gần bạn',
          iconData: Icons.location_pin,
          colorIcon: DefaultTheme.VIOLET,
          backgroundColor: DefaultTheme.WHITE,
          height: 30,
          textStyle: DefaultTheme.TEXT_STYLE_BOTTOM_ICON_DELICIOUS,
        ),
        IconTextUtility(
          title: 'Thực phẩm',
          iconData: Icons.fastfood_sharp,
          colorIcon: DefaultTheme.ORANGE_ALLOW2,
          backgroundColor: DefaultTheme.WHITE,
          height: 30,
          textStyle: DefaultTheme.TEXT_STYLE_BOTTOM_ICON_DELICIOUS,
        ),
        IconTextUtility(
          title: 'Sức khỏe',
          iconData: Icons.favorite,
          colorIcon: DefaultTheme.THEME_GRADIENT2,
          backgroundColor: DefaultTheme.WHITE,
          height: 30,
          textStyle: DefaultTheme.TEXT_STYLE_BOTTOM_ICON_DELICIOUS,
        ),
        IconTextUtility(
          title: 'Đặc sản',
          iconData: Icons.location_pin,
          colorIcon: DefaultTheme.ORANGE,
          backgroundColor: DefaultTheme.WHITE,
          height: 30,
          textStyle: DefaultTheme.TEXT_STYLE_BOTTOM_ICON_DELICIOUS,
        ),

      ],
    );
  }

}
