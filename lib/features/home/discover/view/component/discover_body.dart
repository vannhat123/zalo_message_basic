import 'package:flutter/material.dart';
import 'package:message_za_lo/features/home/discover/view/component/delicious_food_near_you.dart';
import 'package:message_za_lo/features/home/discover/view/component/discover_game_box.dart';
import 'package:message_za_lo/features/home/discover/view/component/find_ticket.dart';
import 'package:message_za_lo/features/home/discover/view/component/grid_view_icon_text.dart';
import 'package:message_za_lo/features/home/discover/view/component/utility_for_you.dart';

class DiscoverBody extends StatelessWidget {
  const DiscoverBody({Key? key}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    Size size = MediaQuery.of(context).size;
    return ListView(
      children: [
        UtilityForYou(),
        GridViewIconText(),
        DiscoverGameBox(size: size),
        FindTicket(size:size),
        DeliciousFoodNearYou(size:size)
      ],
    );
  }
}
