import 'package:flutter/material.dart';
import 'package:message_za_lo/commons/constants/numeral.dart';
import 'package:message_za_lo/commons/constants/theme.dart';
class UtilityForYou extends StatelessWidget {
  const UtilityForYou({Key? key}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return Container(
        alignment: Alignment.centerLeft,
        margin: EdgeInsets.only(
            left: Numeral.PADDING_LEFT_RIGHT_PRIMARY,
            top: Numeral.PADDING_LEFT_RIGHT_PRIMARY,bottom: 10),
        child: Text(
          "Tiện ích cho bạn",
          style: DefaultTheme.TEXT_STYLE_PRIMARY_BLACK,
        ));
  }
}
