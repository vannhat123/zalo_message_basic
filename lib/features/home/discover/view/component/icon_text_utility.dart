import 'package:flutter/material.dart';
import 'package:message_za_lo/commons/constants/numeral.dart';
import 'package:message_za_lo/commons/constants/theme.dart';

class IconTextUtility extends StatelessWidget {
  const IconTextUtility(
      {Key? key,
      required this.iconData,
      required this.title,
      required this.colorIcon,
      this.backgroundColor,
      this.height,
      this.textStyle})
      : super(key: key);
  final IconData iconData;
  final String title;
  final Color colorIcon;
  final Color? backgroundColor;
  final double? height;
  final TextStyle? textStyle;

  @override
  Widget build(BuildContext context) {
    return Column(
      crossAxisAlignment: CrossAxisAlignment.center,
      children: [
        Container(
          height: height ?? Numeral.HEIGHT_CONTAINER_OUT_ICON_UTILITY,
          width: height ?? Numeral.HEIGHT_CONTAINER_OUT_ICON_UTILITY,
          decoration: BoxDecoration(
              borderRadius: BorderRadius.all(Radius.circular(50)),
              color: backgroundColor ??
                  DefaultTheme.GREY.withOpacity(Numeral.OPACITY_COLOR_GREY)),
          child: Icon(
            iconData,
            size: Numeral.SIZE_ICON_UTILITY_DISCOVER,
            color: colorIcon,
          ),
        ),
        const SizedBox(
          height: 5,
        ),
        Text(
          title,
          style: textStyle ?? DefaultTheme.TEXT_STYLE_UTILITY_BOTTOM_ICON,textAlign: TextAlign.center,
        )
      ],
    );
  }
}
