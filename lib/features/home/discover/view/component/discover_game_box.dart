import 'package:flutter/material.dart';
import 'package:message_za_lo/commons/constants/numeral.dart';
import 'package:message_za_lo/commons/constants/theme.dart';
import 'package:message_za_lo/commons/routes/image_asset.dart';
import 'package:message_za_lo/features/home/discover/view/component/circle_background_icon.dart';
import 'package:message_za_lo/features/home/discover/view/component/clip_rect_image.dart';
class DiscoverGameBox extends StatelessWidget {
  const DiscoverGameBox({Key? key, required this.size}) : super(key: key);
  final Size size;
  @override
  Widget build(BuildContext context) {
    return Container(
      decoration: BoxDecoration(
          border: Border.symmetric(
              horizontal: BorderSide(
                  width: Numeral.HEIGHT_DIVIDE_CONTAINER,
                  color: DefaultTheme.GREY
                      .withOpacity(Numeral.OPACITY_COLOR_GREY)))),
      padding: EdgeInsets.symmetric(
          vertical: 10, horizontal: Numeral.PADDING_LEFT_RIGHT_PRIMARY),
      child: Column(
        children: [
          Row(
            children: [
              CircleBackgroundIcon(
                height: Numeral.HEIGHT_CIRCLE_CONTAINER_ICON_UTILITY,
                iconData: Icons.volume_down_outlined,
                colorIconData: DefaultTheme.ORANGE,
                colorBackground: DefaultTheme.ORANGE.withOpacity(0.3),
              ),
              SizedBox(width: 8,),
              Text("Khám phá Game hay",
                  style: DefaultTheme.TEXT_STYLE_UTILITY_BOTTOM_ICON),
              Container(
                  margin: EdgeInsets.only(left: 8),
                  padding: EdgeInsets.symmetric(vertical: 3, horizontal: 6),
                  alignment: Alignment.center,
                  decoration: BoxDecoration(
                      borderRadius: BorderRadius.all(Radius.circular(10)),
                      color: DefaultTheme.GREY
                          .withOpacity(Numeral.OPACITY_COLOR_GREY)),
                  child: Text("Ads", style: DefaultTheme.TEXT_STYLE_SUB_ADS))
            ],
          ),
          Container(
            margin: EdgeInsets.only(top: 10),
            height: size.width * 0.45,
            child: Row(
              mainAxisAlignment: MainAxisAlignment.spaceBetween,
              children: [
                ClipRectImage(
                  iconData: ImageAsset.IMAGE_GAME_TIEN_LEN,
                  checkLast: false,
                  title: "Tiến Lên Miền Nam",
                  height: size.width * 0.45,
                  textStyle: DefaultTheme.TEXT_SUB_GAME_TIEN_LEN,
                ),
                Container(
                    width: size.width * 0.45,
                    child: Column(
                      mainAxisAlignment: MainAxisAlignment.spaceBetween,
                      children: [
                        SizedBox(
                          width: size.width * 0.44,
                          height: size.width * 0.205,
                          child: Row(
                            mainAxisAlignment: MainAxisAlignment.spaceBetween,
                            children: [
                              ClipRectImage(
                                iconData: ImageAsset.IMAGE_GAME_KHO_GAME,
                                checkLast: false,
                                title: "Võ Lâm",
                                height: size.width * 0.205,
                              ),
                              ClipRectImage(
                                iconData: ImageAsset.IMAGE_GAME_VO_LAM,
                                checkLast: false,
                                title: "Tá lả",
                                height: size.width * 0.205,
                              ),
                            ],
                          ),
                        ),
                        SizedBox(
                          width: size.width * 0.44,
                          height: size.width * 0.205,
                          child: Row(
                            mainAxisAlignment: MainAxisAlignment.spaceBetween,
                            children: [
                              ClipRectImage(
                                iconData: ImageAsset.IMAGE_GAME_TA_LA,
                                checkLast: false,
                                title: "Silkroad",
                                height: size.width * 0.205,
                              ),
                              ClipRectImage(
                                iconData: ImageAsset.IMAGE_GAME_SILK_ROAD,
                                checkLast: true,
                                title: "Kho game",
                                height: size.width * 0.205,
                              ),
                            ],
                          ),
                        ),
                      ],
                    )),
              ],
            ),
          )
        ],
      ),
    );
  }
}
