import 'package:flutter/material.dart';
import 'package:message_za_lo/features/home/discover/view/component/discover_body.dart';
class DiscoverScreen extends StatelessWidget {
  const DiscoverScreen({Key? key}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return DiscoverBody();
  }
}
