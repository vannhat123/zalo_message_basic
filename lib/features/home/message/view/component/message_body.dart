import 'package:flutter/material.dart';
import 'package:get/get.dart';
import 'package:message_za_lo/commons/constants/numeral.dart';
import 'package:message_za_lo/commons/constants/theme.dart';
import 'package:message_za_lo/commons/routes/image_asset.dart';
import 'package:message_za_lo/commons/routes/routes.dart';
import 'package:message_za_lo/features/home/tabbar/controller/tabbar_controller.dart';

class MessageBody extends StatelessWidget {
  const MessageBody({Key? key}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    final controller= Get.find<TabBarController>();
    Size size = MediaQuery.of(context).size;
    return ListView.builder(
        itemCount: 15,
        itemBuilder: (BuildContext context, int index) {
          return (index != 14)
              ? _boxMessageWidget((){},context,controller)
              : _addSearchFriendWidget(size);
        });
  }

  Widget _boxMessageWidget(Function function,BuildContext context,TabBarController controller) {
    return InkWell(
      onTap: (){
        Navigator.of(context).pushNamedAndRemoveUntil(Routes.CHAT, (route) => true);
      },
      child: Container(
        margin: const EdgeInsets.symmetric(
            horizontal: Numeral.PADDING_LEFT_RIGHT_PRIMARY, vertical: 12),
        child: Column(
          children: [
            Row(
              crossAxisAlignment: CrossAxisAlignment.center,
              children: [
                Container(
                    margin: const EdgeInsets.only(right: 10),
                    height: 55,
                    width: 55,
                    child: const CircleAvatar(
                      backgroundImage:
                          AssetImage(ImageAsset.AVATAR_MESSAGE_MEDIA_BOX),
                    )),
                Expanded(
                  child: Column(
                    crossAxisAlignment: CrossAxisAlignment.start,
                    children: [
                      Row(
                        mainAxisAlignment: MainAxisAlignment.spaceBetween,
                        children: const [
                          Text(
                            "Media Box (Waiting)",
                            style: DefaultTheme.TEXT_STYLE_MESSAGE_PRIMARY,
                          ),
                          Text(
                            "5 giờ",
                            style: DefaultTheme.TEXT_STYLE_MESSAGE_TIME_SEND,
                          )
                        ],
                      ),
                      const SizedBox(height: 3,),
                      Row(
                        mainAxisAlignment: MainAxisAlignment.spaceBetween,
                        children: [
                          const Expanded(
                            child: Text(
                              "This is considered an error condition because it indicates that",
                              style: DefaultTheme.TEXT_STYLE_MESSAGE_SUB_NOT_SEEN,
                              overflow: TextOverflow.ellipsis,
                            ),
                          ),
                          Container(
                            width: 20,
                            height: 16.5,
                            alignment: Alignment.center,
                            decoration: const BoxDecoration(
                                color: DefaultTheme.RED,
                                borderRadius:
                                    BorderRadius.all(Radius.circular(15))),
                            child: const Text(
                              "1",
                              style: DefaultTheme.TEXT_STYLE_MESSAGE_NOTIFICATION,
                            ),
                          )
                        ],
                      ),
                    ],
                  ),
                )
              ],
            ),
            const SizedBox(
              height: 15,
            ),
            Padding(
                padding: const EdgeInsets.only(left: 60),
                child: Divider(
                  height: 0.7,
                  color: DefaultTheme.GREY.withOpacity(Numeral.OPACITY_COLOR_GREY_DIVIDE),
                ))
          ],
        ),
      ),
    );
  }

  Widget _addSearchFriendWidget(Size size) {
    return Container(
      margin:const EdgeInsets.only(top: 20,bottom: 5),
      height: 65,
      alignment: Alignment.center,
      child: Column(
        mainAxisAlignment: MainAxisAlignment.spaceBetween,
        children: [
          const Text(
            "Dễ dàng tìm kiếm và trò chuyện với bạn bè",
            style: DefaultTheme.TEXT_STYLE_SUB_NORMAL_GREY,
          ),
          Container(
            padding: const EdgeInsets.symmetric(vertical: 8),
            alignment: Alignment.center,
            width: size.width * 0.45,
            decoration: const BoxDecoration(
                borderRadius: BorderRadius.all(Radius.circular(50)),
                color: DefaultTheme.THEME_COLOR),
            child: const Text(
              "Tìm thêm bạn",
              style: DefaultTheme.TEXT_SUB_BUTTON_WHITE,
            ),
          )
        ],
      ),
    );
  }
}
