import 'package:flutter/material.dart';
import 'package:message_za_lo/features/home/message/view/component/message_body.dart';
class MessageScreen extends StatelessWidget {
  const MessageScreen({Key? key}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return const MessageBody();
  }
}
