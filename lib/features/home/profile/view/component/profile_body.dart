import 'package:flutter/material.dart';
import 'package:get/get.dart';
import 'package:message_za_lo/features/account_detail/controller/account_detail_controller.dart';
import 'package:message_za_lo/features/home/profile/view/component/box_account_change.dart';
import 'package:message_za_lo/features/home/profile/view/component/box_information_full.dart';
import 'package:message_za_lo/features/home/profile/view/component/button_log_out.dart';

class ProfileBody extends StatelessWidget {
  const ProfileBody({Key? key}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return GetBuilder<AccountDetailController>(
      init: AccountDetailController(),
      builder: (controller) {
        return Column(
          children: [
            BoxAccountChange(accountDetailController: controller,),
            BoxInformationFull(),
            SizedBox(
              height: 30,
            ),
            ButtonLogOut(),
          ],
        );
      }
    );
  }
}
