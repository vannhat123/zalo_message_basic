import 'package:flutter/material.dart';
import 'package:flutter_bloc/flutter_bloc.dart';
import 'package:message_za_lo/commons/constants/numeral.dart';
import 'package:message_za_lo/commons/constants/theme.dart';
import 'package:message_za_lo/commons/widgets/widgets.dart';
import 'package:message_za_lo/features/login/cubit/login_cubit.dart';

class ButtonLogOut extends StatelessWidget {
  const ButtonLogOut({Key? key}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return ButtonWidget(
      buttonColor: DefaultTheme.GREY.withOpacity(Numeral.OPACITY_COLOR_GREY),
      onTap: () {
        context.read<LoginCubit>().logOutFirebase(context);
      },
      text: 'Đăng xuất',
      textStyle: DefaultTheme.TEXT_SUB_BUTTON_BLACK,
    );
  }
}
