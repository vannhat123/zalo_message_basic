import 'package:flutter/material.dart';
import 'package:message_za_lo/commons/constants/numeral.dart';
import 'package:message_za_lo/commons/constants/theme.dart';
import 'package:message_za_lo/features/account_detail/controller/account_detail_controller.dart';
import 'package:message_za_lo/services/shared_preferences/account_helper.dart';

class BoxAccountChange extends StatelessWidget {
  const BoxAccountChange({Key? key, required this.accountDetailController})
      : super(key: key);
  final AccountDetailController accountDetailController;

  @override
  Widget build(BuildContext context) {
    return Container(
      margin: const EdgeInsets.symmetric(
          horizontal: Numeral.PADDING_LEFT_RIGHT_PRIMARY, vertical: 15),
      child: Column(
        children: [
          Row(
            crossAxisAlignment: CrossAxisAlignment.center,
            children: [
              Container(
                  margin: const EdgeInsets.only(right: 10),
                  height: 50,
                  width: 50,
                  child: CircleAvatar(
                    backgroundImage: NetworkImage(
                        AccountHelper.instance.getAvatarUrl()),
                  )),
              Expanded(
                child: Row(
                  crossAxisAlignment: CrossAxisAlignment.center,
                  mainAxisAlignment: MainAxisAlignment.spaceBetween,
                  children: [
                    Column(
                      crossAxisAlignment: CrossAxisAlignment.start,
                      children: [
                        Text(
                          accountDetailController.profileDTO.fullName,
                          style: DefaultTheme
                              .TEXT_STYLE_MESSAGE_PRIMARY_W400_BLACK,
                          overflow: TextOverflow.ellipsis,
                        ),
                        SizedBox(
                          height: 6,
                        ),
                        Text(
                          "Xem trang cá nhân",
                          style: DefaultTheme.TEXT_STYLE_MESSAGE_SUB_W300_GREY,
                          overflow: TextOverflow.ellipsis,
                        ),
                      ],
                    ),
                    Icon(
                      Icons.change_circle_rounded,
                      size: Numeral.SIZE_ICON_PROFILE_INFORMATION,
                      color: DefaultTheme.THEME_COLOR,
                    )
                  ],
                ),
              )
            ],
          ),
        ],
      ),
    );
  }
}
