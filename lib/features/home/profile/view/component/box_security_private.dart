import 'package:flutter/material.dart';
import 'package:message_za_lo/commons/constants/numeral.dart';
import 'package:message_za_lo/commons/constants/theme.dart';
class BoxSecurityPrivate extends StatelessWidget {
  const BoxSecurityPrivate({Key? key, required this.iconFirst, required this.title,required this.function}) : super(key: key);
  final IconData iconFirst;
  final String title;
  final Function function;
  @override
  Widget build(BuildContext context) {
    return InkWell(
      onTap: (){
        function();
      },
      child: Container(
        padding: const EdgeInsets.symmetric(
            horizontal: Numeral.PADDING_LEFT_RIGHT_PRIMARY, vertical: 12),
        child: Row(
          crossAxisAlignment: CrossAxisAlignment.center,
          children: [
            Icon(
              iconFirst,
              size: Numeral.SIZE_ICON_PROFILE_INFORMATION,
              color: DefaultTheme.THEME_COLOR,
            ),
            const SizedBox(
              width: 15,
            ),
            Expanded(
              child: Text(
                title,
                style: DefaultTheme.TEXT_STYLE_MESSAGE_PRIMARY_W400_BLACK,
                overflow: TextOverflow.ellipsis,
              ),
            ),
            const Icon(
              Icons.arrow_forward_ios_sharp,
              color: DefaultTheme.GREY,
              size: 15,
            )
          ],
        ),
      ),
    );
  }
}
