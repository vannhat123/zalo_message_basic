import 'package:flutter/material.dart';
import 'package:message_za_lo/commons/constants/numeral.dart';
import 'package:message_za_lo/commons/constants/theme.dart';

class BoxCloudData extends StatelessWidget {
  const BoxCloudData(
      {Key? key,
      required this.iconFirst,
      required this.titlePrimary,
      required this.titleSub})
      : super(key: key);
  final IconData iconFirst;
  final String titlePrimary;
  final String titleSub;

  @override
  Widget build(BuildContext context) {
    return Padding(
      padding: const EdgeInsets.symmetric(
          horizontal: Numeral.PADDING_LEFT_RIGHT_PRIMARY, vertical: 12),
      child: Row(
        crossAxisAlignment: CrossAxisAlignment.center,
        children: [
          Icon(
            iconFirst,
            size: Numeral.SIZE_ICON_PROFILE_INFORMATION,
            color: DefaultTheme.THEME_COLOR,
          ),
          const SizedBox(
            width: 15,
          ),
          Expanded(
            child: Column(
              crossAxisAlignment: CrossAxisAlignment.start,
              children: [
                Text(
                  titlePrimary,
                  style: DefaultTheme.TEXT_STYLE_MESSAGE_PRIMARY_W400_BLACK,
                  overflow: TextOverflow.ellipsis,
                ),
                const SizedBox(
                  height: 3,
                ),
                Text(
                  titleSub,
                  style: DefaultTheme.TEXT_STYLE_MESSAGE_SUB_W300_GREY,
                  overflow: TextOverflow.ellipsis,
                ),
              ],
            ),
          ),
          const Icon(
            Icons.arrow_forward_ios_sharp,
            color: DefaultTheme.GREY,
            size: 15,
          )
        ],
      ),
    );
  }
}
