import 'package:flutter/material.dart';
import 'package:message_za_lo/commons/constants/numeral.dart';
import 'package:message_za_lo/commons/constants/theme.dart';
import 'package:message_za_lo/commons/routes/routes.dart';
import 'package:message_za_lo/features/home/profile/view/component/box_cloud_data.dart';
import 'package:message_za_lo/features/home/profile/view/component/box_security_private.dart';

class BoxInformationFull extends StatelessWidget {
  const BoxInformationFull({Key? key}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return Column(
      children: [
        Container(
          decoration: BoxDecoration(
              border: Border.symmetric(
                  horizontal: BorderSide(
                      width: Numeral.HEIGHT_DIVIDE_CONTAINER,
                      color: DefaultTheme.GREY
                          .withOpacity(Numeral.OPACITY_COLOR_GREY)))),
          child: Padding(
            padding: const EdgeInsets.symmetric(
                horizontal: Numeral.PADDING_LEFT_RIGHT_PRIMARY, vertical: 12),
            child: Row(
              crossAxisAlignment: CrossAxisAlignment.center,
              children: [
                Icon(
                  Icons.account_balance_wallet_outlined,
                  size: Numeral.SIZE_ICON_PROFILE_INFORMATION,
                  color: DefaultTheme.THEME_COLOR,
                ),
                const SizedBox(
                  width: 15,
                ),
                Expanded(
                  child: Column(
                    crossAxisAlignment: CrossAxisAlignment.start,
                    children: const [
                      Text(
                        "Ví QR (chưa có)",
                        style:
                            DefaultTheme.TEXT_STYLE_MESSAGE_PRIMARY_W400_BLACK,
                        overflow: TextOverflow.ellipsis,
                      ),
                      SizedBox(
                        height: 3,
                      ),
                      Text(
                        "Lưu trữ và xuất trình các mã QR quan trọng (chưa có)",
                        style: DefaultTheme.TEXT_STYLE_MESSAGE_SUB_W300_GREY,
                        overflow: TextOverflow.ellipsis,
                      ),
                    ],
                  ),
                ),
              ],
            ),
          ),
        ),
        Container(
          decoration: BoxDecoration(
              border: Border(
                  bottom: BorderSide(
                      width: Numeral.HEIGHT_DIVIDE_CONTAINER,
                      color: DefaultTheme.GREY
                          .withOpacity(Numeral.OPACITY_COLOR_GREY)))),
          child: Column(
            children: [
              BoxCloudData(
                titlePrimary: "Cloud của tôi (chưa có)",
                titleSub: "Lưu trữ các tin nhắn quan trọng (chưa có)",
                iconFirst: Icons.wb_cloudy_outlined,
              ),
              Row(
                crossAxisAlignment: CrossAxisAlignment.center,
                children: [
                  const SizedBox(
                    width: 55,
                  ),
                  Flexible(
                    child: Divider(
                      height: 0.7,
                      color: DefaultTheme.GREY
                          .withOpacity(Numeral.OPACITY_COLOR_GREY_DIVIDE),
                    ),
                  ),
                ],
              ),
              BoxCloudData(
                titlePrimary: "Dữ liệu và bộ nhớ (chưa có)",
                titleSub: "Quản lý dữ liệu Zalo của bạn (chưa có)",
                iconFirst: Icons.lightbulb_circle,
              ),
            ],
          ),
        ),
        BoxSecurityPrivate(
          iconFirst: Icons.security_outlined,
          title: 'Thông tin tài khoản',
          function: () {
            Navigator.of(context).pushNamedAndRemoveUntil(
                Routes.ACCOUNT_DETAIL, (route) => true);
          },
        ),
        Row(
          crossAxisAlignment: CrossAxisAlignment.center,
          children: [
            const SizedBox(
              width: 55,
            ),
            Flexible(
              child: Divider(
                height: 0.7,
                color: DefaultTheme.GREY
                    .withOpacity(Numeral.OPACITY_COLOR_GREY_DIVIDE),
              ),
            ),
          ],
        ),
        BoxSecurityPrivate(
          iconFirst: Icons.lock_outline,
          title: 'Quyền riêng tư',
          function: () {},
        ),
        Container(
          height: Numeral.HEIGHT_DIVIDE_CONTAINER+0.5,
          color: DefaultTheme.GREY.withOpacity(Numeral.OPACITY_COLOR_GREY),
        )
      ],
    );
  }
}
