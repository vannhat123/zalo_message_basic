import 'package:flutter/material.dart';
import 'package:message_za_lo/features/home/profile/view/component/profile_body.dart';
class ProfileScreen extends StatelessWidget {
  const ProfileScreen({Key? key}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return const ProfileBody();
  }
}
