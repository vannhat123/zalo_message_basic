import 'package:flutter/material.dart';
import 'package:get/get.dart';
import 'package:message_za_lo/features/home/diary/controller/diary_controller.dart';
import 'package:message_za_lo/features/home/diary/view/component/diary_body.dart';
class DiaryScreen extends StatelessWidget {
  const DiaryScreen({Key? key}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return GetBuilder<DiaryController>(
      init: DiaryController(),
      builder: (controller) {
        return DiaryBody(diaryController: controller,);
      }
    );
  }
}
