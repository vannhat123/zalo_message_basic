import 'package:flutter/material.dart';
import 'package:get/get.dart';
import 'package:intl/intl.dart';
import 'package:message_za_lo/commons/constants/numeral.dart';
import 'package:message_za_lo/commons/constants/theme.dart';
import 'package:message_za_lo/features/home/diary/controller/diary_controller.dart';
import 'package:message_za_lo/models/diary_posts_dto.dart';
import 'package:message_za_lo/services/firebase/firestore_helper.dart';

class DiaryCommentFooter extends StatelessWidget {
  DiaryCommentFooter({Key? key, required this.size,required this.index}) : super(key: key);
  final Size size;
  final int index;

  @override
  Widget build(BuildContext context) {
    Size size = MediaQuery.of(context).size;
    return GetBuilder<DiaryController>(
        init: DiaryController(),
        builder: (controller) {
          return Column(
            children: [
              Container(
                decoration: BoxDecoration(
                  border: Border(
                      top: BorderSide(
                          width: 1,
                          color: DefaultTheme.GREY
                              .withOpacity(Numeral.OPACITY_COLOR_GREY_DIVIDE))),
                  color: DefaultTheme.WHITE,
                ),
                alignment: Alignment.bottomCenter,
                height: controller.heightTextMessage + size.height * 0.05,
                padding: EdgeInsets.symmetric(
                    horizontal: Numeral.PADDING_LEFT_RIGHT_PRIMARY),
                child: Row(
                  crossAxisAlignment: CrossAxisAlignment.end,
                  children: [
                    Padding(
                      padding: const EdgeInsets.only(bottom: 10),
                      child: _iconChatWidget(Icons.mood, 25),
                    ),
                    Expanded(
                        child: Container(
                      height: size.height * 0.05 + controller.heightTextMessage,
                      child: Stack(
                        children: [
                          Container(
                            key: controller.keyContainerPrimary,
                          ),
                          Scrollbar(
                            controller: controller.chatScrollController,
                            child: TextField(
                              scrollController: controller.chatScrollController,
                              onTap: () {
                                if (!controller.checkFocusChatController) {
                                  controller.setCheckFocusChatController(true);
                                }
                                if (controller.heightTextMessage !=
                                    size.height) {
                                  Get.find<DiaryController>()
                                      .setHeightInputComment(size.height * 0.9);
                                }
                              },
                              controller: controller.textCommentController,
                              onChanged: (value) {
                                if (!controller.checkInputComment) {
                                  controller.setCheckInputComment(true);
                                }
                                if (value == '' &&
                                    controller.checkInputComment) {
                                  controller.setCheckInputComment(false);
                                }
                                controller.numLines =
                                    '\n'.allMatches(value).length + 1;
                                if (controller.numLines >
                                    controller.copyNumLines) {
                                  controller
                                      .setValueCheckWidthTextAndSetHeightTextField(
                                    Numeral.TEXT_PRIMARY,
                                    true,
                                  );
                                } else if (controller.numLines <
                                    controller.copyNumLines) {
                                  controller
                                      .setValueCheckWidthTextAndSetHeightTextField(
                                    Numeral.TEXT_PRIMARY,
                                    false,
                                  );
                                } else {
                                  controller
                                      .setValueCheckWidthTextAndSetHeightTextField(
                                          Numeral.TEXT_PRIMARY, null);
                                }
                                controller.copyNumLines = controller.numLines;
                              },
                              maxLines: 4,
                              style: DefaultTheme.TEXT_SUB_COMMENT_DIARY,
                              keyboardType: TextInputType.multiline,
                              decoration: InputDecoration(
                                  contentPadding:
                                      EdgeInsets.only(left: 10, top: 5),
                                  hintText: "Nhập bình luận",
                                  disabledBorder: InputBorder.none,
                                  enabledBorder: const UnderlineInputBorder(
                                      borderRadius: BorderRadius.zero,
                                      borderSide: BorderSide.none),
                                  focusedBorder: const UnderlineInputBorder(
                                      borderRadius: BorderRadius.zero,
                                      borderSide: BorderSide.none),
                                  suffixIcon: Container(
                                    key: controller.keyIconSub,
                                    alignment: Alignment.centerRight,
                                    width: 10,
                                    child: Row(
                                      children: [
                                        Spacer(),
                                        if (controller.checkInputComment)
                                          InkWell(
                                            onTap: () {
                                              FocusScope.of(context)
                                                  .requestFocus(FocusNode());
                                              controller.dateTimeNow =
                                                  DateTime.now();
                                              controller.date = DateFormat(
                                                      'dd/MM/yyyy')
                                                  .format(
                                                      controller.dateTimeNow);
                                              if (controller.dateTimeNow.hour <
                                                  10) {
                                                controller.hour =
                                                    "0${controller.dateTimeNow.hour}";
                                              } else {
                                                controller.hour =
                                                    "${controller.dateTimeNow.hour}";
                                              }
                                              if (controller
                                                      .dateTimeNow.minute <
                                                  10) {
                                                controller.minutes =
                                                    "0${controller.dateTimeNow.minute}";
                                              } else {
                                                controller.minutes =
                                                    "${controller.dateTimeNow.minute}";
                                              }
                                              controller.sendingTime =
                                                  controller.dateTimeNow
                                                      .millisecondsSinceEpoch;
                                              controller.hourFormat =
                                                  "${controller.hour}:${controller.minutes}";
                                              FireStoreHelper().commentPostsDiary(
                                                  PostsComment(
                                                      commentContent: controller
                                                          .textCommentController
                                                          .text
                                                          .trim(),
                                                      commentDate:
                                                          controller.date,
                                                      commentHour:
                                                          controller.hourFormat,
                                                      commentImageUrl:
                                                          controller.profileDTO
                                                              .avatarUrl,
                                                      commentUserName:
                                                          controller.profileDTO
                                                              .fullName,
                                                      commentUserEmail:
                                                          controller.profileDTO
                                                              .email),
                                                  controller.listDiaryPostsDTO[index].sendingTime);
                                              controller.textCommentController
                                                  .text = '';
                                              controller
                                                  .setCheckInputComment(false);
                                            },
                                            child: Icon(
                                              Icons.send_rounded,
                                              size: 25,
                                              color: DefaultTheme.THEME_COLOR,
                                            ),
                                          )
                                        else
                                          Icon(
                                            Icons.send_rounded,
                                            size: 25,
                                            color: DefaultTheme.GREY,
                                          ),
                                      ],
                                    ),
                                  )),
                            ),
                          ),
                        ],
                      ),
                    ))
                  ],
                ),
              ),
              (!controller.checkFocusChatController)
                  ? Container(
                      color: DefaultTheme.WHITE,
                      height: size.height * 0.05,
                    )
                  : const SizedBox(),
            ],
          );
        });
  }

  Widget _iconChatWidget(IconData iconData, double size) {
    return Icon(
      iconData,
      color: DefaultTheme.GREY_TEXT_BUTTON,
      size: 26,
    );
  }
}
