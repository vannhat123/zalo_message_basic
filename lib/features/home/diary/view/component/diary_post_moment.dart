import 'package:flutter/material.dart';
import 'package:message_za_lo/commons/constants/numeral.dart';
import 'package:message_za_lo/commons/constants/theme.dart';
class DiaryPostMoment extends StatelessWidget {
  const DiaryPostMoment({Key? key,required this.size, required this.title, required this.image}) : super(key: key);
  final Size size;
  final String title;
  final String image;
  @override
  Widget build(BuildContext context) {
    return Container(
      padding: const EdgeInsets.only(
          left: Numeral.PADDING_LEFT_RIGHT_PRIMARY, top: 10),
      margin: EdgeInsets.only(bottom: 5, right: 2),
      child: Row(
        children: [
          Column(
            children: [
              Container(
                  width: size.width * 0.1,
                  height: size.width * 0.13,
                  margin: EdgeInsets.only(bottom: 8),
                  decoration: BoxDecoration(
                    borderRadius: BorderRadius.all(
                      Radius.circular(5),
                    ),
                    gradient: LinearGradient(
                      colors: [
                        DefaultTheme.THEME_GRADIENT3,
                        DefaultTheme.PINK_BUTTON,
                      ],
                      begin: Alignment.topLeft,
                      end: Alignment.topRight,
                      tileMode: TileMode.repeated,
                    ),
                  ),
                  padding: EdgeInsets.all(1.5),
                  child: Container(
                    decoration: BoxDecoration(
                        border: Border.all(width: 1, color: DefaultTheme.WHITE),
                        borderRadius: BorderRadius.all(Radius.circular(5))),
                    child: ClipRRect(
                      borderRadius: BorderRadius.circular(5.0),
                      //add border radius
                      child: Image.asset(
                        image,
                        fit: BoxFit.cover,
                      ),
                    ),
                  )),
              Text(
                title,
                style: DefaultTheme.TEXT_STYLE_SMALL_MOMENT_BLACK_COLOR_W300,
              )
            ],
          ),
        ],
      ),
    );
  }
}
