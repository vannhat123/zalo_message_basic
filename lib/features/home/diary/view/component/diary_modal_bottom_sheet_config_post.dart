import 'package:flutter/material.dart';
import 'package:get/get.dart';
import 'package:message_za_lo/commons/constants/numeral.dart';
import 'package:message_za_lo/commons/constants/theme.dart';
import 'package:message_za_lo/commons/widgets/widgets.dart';
import 'package:message_za_lo/features/home/diary/controller/diary_controller.dart';
import 'package:message_za_lo/features/home/diary/view/component/diary_posts_status/diary_posts_status.dart';
import 'package:message_za_lo/models/diary_posts_dto.dart';
import 'package:message_za_lo/services/firebase/firestore_helper.dart';

class DiaryModalBottomSheetConfigPost extends StatelessWidget {
  const DiaryModalBottomSheetConfigPost(
      {Key? key,
      required this.size,
      required this.index,
      required this.diaryController,
      required this.diaryPostsDTO})
      : super(key: key);
  final Size size;
  final int index;
  final DiaryController diaryController;
  final DiaryPostsDTO diaryPostsDTO;

  @override
  Widget build(BuildContext context) {
    return Container(
      height: 240,
      child: Column(
        children: [
          _boxSelectWidget(Icons.people_outline,
              'Chỉnh sửa quyền xem (chưa có) ', 'Tất cả bạn bè', false, () {}),
          _boxSelectWidget(Icons.mode_edit_outline_rounded,
              'Chỉnh sửa bài đăng', 'Bao gồm nội dung, ảnh', false, () {
            Get.back();
            Get.put(DiaryController()).setDiaryPostsDTO(diaryPostsDTO);
            Get.put(DiaryController()).setCheckConfigPosts(true);
            modalSheetConfigPostsWidget(context, size);
          }),
          _boxSelectWidget(Icons.delete_outline, 'Xóa bài đăng',
              'Xóa bài đăng này của bạn', true, () {
            DialogWidget.instance.openConfirmDialog(
                context: context,
                title: '',
                description: 'Bạn có chắc chắn muốn xóa?',
                function: () async {
                  FireStoreHelper().deleteYourDiary(diaryPostsDTO);
                  Get.back();
                });
          }),
        ],
      ),
    );
  }

  Widget _boxSelectWidget(IconData iconData, String titlePrimary,
      String titleSub, bool checkBottom, Function function) {
    return InkWell(
      onTap: () {
        function();
      },
      child: Container(
        alignment: Alignment.centerLeft,
        height: 70,
        padding: EdgeInsets.symmetric(horizontal: 10),
        child: Row(
          crossAxisAlignment: CrossAxisAlignment.center,
          children: [
            Icon(
              iconData,
              color: DefaultTheme.GREY,
              size: 20,
            ),
            SizedBox(
              width: 10,
            ),
            Expanded(
              child: Container(
                decoration: BoxDecoration(
                    border: Border(
                        bottom: checkBottom
                            ? BorderSide.none
                            : BorderSide(
                                width: 0.6,
                                color: DefaultTheme.GREY.withOpacity(
                                    Numeral.OPACITY_COLOR_GREY_DIVIDE)))),
                child: Column(
                  crossAxisAlignment: CrossAxisAlignment.start,
                  mainAxisAlignment: MainAxisAlignment.center,
                  children: [
                    Text(
                      titlePrimary,
                      style: TextStyle(fontSize: 14, color: DefaultTheme.BLACK),
                    ),
                    SizedBox(
                      height: 4,
                    ),
                    Text(
                      titleSub,
                      style: TextStyle(fontSize: 12, color: DefaultTheme.GREY),
                    ),
                  ],
                ),
              ),
            )
          ],
        ),
      ),
    );
  }

  modalSheetConfigPostsWidget(BuildContext context, Size size) {
    showModalBottomSheet(
        isScrollControlled: true,
        context: context,
        backgroundColor: DefaultTheme.WHITE,
        shape: RoundedRectangleBorder(
          borderRadius: BorderRadius.only(
              topLeft: Radius.circular(15.0), topRight: Radius.circular(15.0)),
        ),
        builder: (context) {
          return DiaryPostsStatus(size: size, diaryController: diaryController, index: index);
        });
  }
}
