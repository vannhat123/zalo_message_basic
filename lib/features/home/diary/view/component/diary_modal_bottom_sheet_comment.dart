import 'package:flutter/material.dart';
import 'package:get/get.dart';
import 'package:message_za_lo/commons/constants/numeral.dart';
import 'package:message_za_lo/commons/constants/theme.dart';
import 'package:message_za_lo/commons/widgets/widgets.dart';
import 'package:message_za_lo/features/home/diary/controller/diary_controller.dart';
import 'package:message_za_lo/features/home/diary/view/component/diary_comment_footer.dart';
import 'package:message_za_lo/models/diary_posts_dto.dart';
import 'package:message_za_lo/services/firebase/firestore_helper.dart';

class DiaryModalBottomSheetComment extends StatelessWidget {
  const DiaryModalBottomSheetComment(
      {Key? key,
      required this.size,
      required this.diaryController,
      required this.index})
      : super(key: key);
  final Size size;
  final int index;
  final DiaryController diaryController;

  bool checkLikeYourPosts() {
    bool checkLike = false;
    for (var item in diaryController.listDiaryPostsDTO[index].postsLike) {
      if (diaryController.profileDTO.fullName == item.likeUserName) {
        checkLike = true;
        break;
      } else {
        checkLike = false;
      }
    }
    return checkLike;
  }

  @override
  Widget build(BuildContext context) {
    return GetBuilder<DiaryController>(
        init: DiaryController(),
        builder: (controller) {
          return Container(
            height: controller.heightInputComment,
            width: size.width,
            decoration: BoxDecoration(
                borderRadius: BorderRadius.only(
                    topRight: Radius.circular(10),
                    topLeft: Radius.circular(10)),
                color: DefaultTheme.WHITE),
            child: Column(
              children: [
                _headerCommentAndCloseWidget(context),
                _likeByOtherWidget(),
                Expanded(
                    child: GestureDetector(
                  onTap: () {
                    Get.find<DiaryController>()
                        .setCheckFocusChatController(false);
                    FocusScope.of(context).requestFocus(FocusNode());
                  },
                  child: ListView.builder(
                      itemCount: diaryController
                          .listDiaryPostsDTO[index].postsComment.length,
                      itemBuilder: (context, indexBuilder) {
                        return _personCommentWidget(
                            diaryController.listDiaryPostsDTO[index]
                                .postsComment[indexBuilder],
                            context);
                      }),
                )),
                AnimatedPadding(
                  padding: MediaQuery.of(context).viewInsets,
                  duration: const Duration(milliseconds: 100),
                  child: DiaryCommentFooter(size: size, index: index),
                )
              ],
            ),
          );
        });
  }

  Widget _headerCommentAndCloseWidget(BuildContext context) {
    return Container(
      padding: EdgeInsets.symmetric(horizontal: 10),
      decoration: BoxDecoration(
        borderRadius: BorderRadius.only(
            topRight: Radius.circular(10), topLeft: Radius.circular(10)),
        color: DefaultTheme.GREY.withOpacity(Numeral.OPACITY_COLOR_GREY),
      ),
      height: size.height * 0.055,
      child: Row(
        mainAxisAlignment: MainAxisAlignment.spaceBetween,
        children: [
          Text(
            "Bình luận",
            style: DefaultTheme.TEXT_STYLE_SUB_W500_BLACK,
          ),
          InkWell(
            onTap: () => Navigator.pop(context),
            child: Icon(
              Icons.close,
              size: 22,
            ),
          ),
        ],
      ),
    );
  }

  Widget _likeByOtherWidget() {
    return Container(
      margin: EdgeInsets.symmetric(horizontal: 10, vertical: 6),
      child: Row(
        crossAxisAlignment: CrossAxisAlignment.center,
        children: [
          !checkLikeYourPosts()
              ? GestureDetector(
                  onTap: () {
                    FireStoreHelper().likePostsDiary(
                        PostsLike(
                            likeUserName: diaryController.profileDTO.fullName,
                            likeImageUrl: diaryController.profileDTO.avatarUrl),
                        diaryController.listDiaryPostsDTO[index].sendingTime);
                  },
                  child: Icon(
                    Icons.favorite_border_rounded,
                    color: DefaultTheme.BLACK,
                  ),
                )
              : GestureDetector(
                  onTap: () {
                    FireStoreHelper().removeLikePostsDiary(
                        PostsLike(
                            likeUserName: diaryController.profileDTO.fullName,
                            likeImageUrl: diaryController.profileDTO.avatarUrl),
                        diaryController.listDiaryPostsDTO[index].sendingTime);
                  },
                  child: Icon(
                    Icons.favorite,
                    color: DefaultTheme.RED,
                  ),
                ),
          SizedBox(
            width: 5,
          ),
          Text(
            (diaryController.listDiaryPostsDTO[index].postsLike.isNotEmpty)
                ? diaryController.listDiaryPostsDTO[index].postsLike.length
                    .toString()
                : '0',
            style: DefaultTheme.TEXT_STYLE_POST_PRIMARY_GREY,
          ),
          Spacer(),
          if (diaryController.listDiaryPostsDTO[index].postsLike.isNotEmpty)
            Text(
              "Thích bởi",
              style: DefaultTheme.TEXT_STYLE_LIKE_OTHER,
            ),
          SizedBox(
            width: 5,
          ),
          if (diaryController.listDiaryPostsDTO[index].postsLike.isNotEmpty)
            Stack(
              alignment: AlignmentDirectional.centerEnd,
              children: [
                Container(
                  alignment: Alignment.centerRight,
                  margin: (diaryController
                              .listDiaryPostsDTO[index].postsLike.length >
                          1)
                      ? EdgeInsets.only(right: 10)
                      : null,
                  width: Numeral.SIZE_LIKE_POSTS,
                  height: Numeral.SIZE_LIKE_POSTS,
                  child: CircleAvatar(
                    backgroundImage: NetworkImage(diaryController
                        .listDiaryPostsDTO[index].postsLike[0].likeImageUrl),
                  ),
                ),
                if (diaryController.listDiaryPostsDTO[index].postsLike.length >
                    1)
                  Container(
                    alignment: Alignment.centerRight,
                    width: Numeral.SIZE_LIKE_POSTS - 5,
                    height: Numeral.SIZE_LIKE_POSTS - 5,
                    decoration: BoxDecoration(
                        border: Border.all(width: 1, color: DefaultTheme.WHITE),
                        color: DefaultTheme.GREY_BORDER,
                        borderRadius: BorderRadius.all(Radius.circular(30))),
                    child: Center(
                        child: Text(
                      "+${(diaryController.listDiaryPostsDTO[index].postsLike.length - 1)}",
                      style: DefaultTheme.TEXT_STYLE_LIKE_SUB,
                    )),
                  ),
              ],
            ),
        ],
      ),
    );
  }

  Widget _personCommentWidget(PostsComment postsComment, BuildContext context) {
    return Container(
      margin: EdgeInsets.symmetric(horizontal: 10, vertical: 10),
      child: Row(
        crossAxisAlignment: CrossAxisAlignment.start,
        children: [
          Container(
            height: 30,
            width: 30,
            margin: EdgeInsets.only(right: 15),
            child: CircleAvatar(
              backgroundImage: NetworkImage(postsComment.commentImageUrl),
            ),
          ),
          Expanded(
            child: Column(
              crossAxisAlignment: CrossAxisAlignment.start,
              children: [
                Row(
                  mainAxisAlignment: MainAxisAlignment.spaceBetween,
                  children: [
                    Text(
                      postsComment.commentUserName,
                      style: DefaultTheme.TEXT_STYLE_COMMENT_PRIMARY_W500,
                    ),
                    InkWell(
                      onTap: () {
                        DialogWidget.instance.openConfirmDialog(
                            context: context,
                            title: '',
                            description:
                                'Bạn có chắc chắn muốn xóa bình luận này',
                            function: () {
                              FireStoreHelper().deleteCommentPostsDiary(
                                  postsComment,
                                  diaryController
                                      .listDiaryPostsDTO[index].sendingTime);
                            });
                      },
                      child: Icon(
                        Icons.delete_outline_outlined,
                        size: 18,
                      ),
                    )
                  ],
                ),
                Container(
                  margin: EdgeInsets.symmetric(vertical: 2),
                  width: size.width * 0.8,
                  child: Text(
                    postsComment.commentContent,
                    style: DefaultTheme.TEXT_STYLE_COMMENT_PRIMARY_W300,
                    maxLines: 3,
                    overflow: TextOverflow.ellipsis,
                  ),
                ),
                Row(
                  children: [
                    Text(
                      "${postsComment.commentHour}  ${postsComment.commentDate}",
                      style: DefaultTheme.TEXT_STYLE_LIKE_SUB_NORMAL_ANSWER,
                    ),
                    SizedBox(
                      width: 8,
                    ),
                    Text(
                      "Trả lời",
                      style: DefaultTheme.TEXT_STYLE_LIKE_SUB_BOLD_ANSWER,
                    )
                  ],
                ),
              ],
            ),
          ),
        ],
      ),
    );
  }
}
