import 'package:flutter/material.dart';
import 'package:message_za_lo/commons/constants/numeral.dart';
import 'package:message_za_lo/commons/constants/theme.dart';
import 'package:message_za_lo/commons/widgets/widgets.dart';
import 'package:message_za_lo/features/home/diary/controller/diary_controller.dart';
import 'package:message_za_lo/features/home/diary/view/component/diary_posts_status/diary_posts_status_app_bar.dart';
import 'package:message_za_lo/features/home/diary/view/component/diary_posts_status/diary_posts_status_body.dart';
import 'package:message_za_lo/features/home/diary/view/component/diary_posts_status/diary_posts_status_footer.dart';

class DiaryPostsStatus extends StatelessWidget {
  const DiaryPostsStatus(
      {Key? key, required this.size, required this.diaryController,required this.index})
      : super(key: key);
  final Size size;
  final DiaryController diaryController;
  final int index;

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      body: GestureDetector(
        onTap: () => FocusScope.of(context).unfocus(),
        child: SingleChildScrollView(
          child: Column(
            mainAxisAlignment: MainAxisAlignment.start,
            children: [
              DiaryPostsStatusAppBar(
                diaryController: diaryController,
                size: size,
                index:index
              ),
              _inputPostsTextWidget(context),
              DiaryPostsStatusBody(
                diaryController: diaryController,
                size: size,
              ),
            ],
          ),
        ),
      ),
      bottomSheet: DiaryPostsStatusFooter(size: size),
    );
  }

  Widget _inputPostsTextWidget(BuildContext context) {
    return Container(
        height: size.height * 0.3,
        child: ListView(
          children: [
            Container(
              height: size.height * 0.3,
              alignment: Alignment.topLeft,
              padding: EdgeInsets.all(10),
              child: Scrollbar(
                child: TextField(
                  controller: diaryController.textPostsController,
                  onChanged: (value) {
                    if (value.trim().length > Numeral.LENGTH_POSTS_DIARY) {
                      DialogWidget.instance.openErrorDialog(
                          context: context,
                          title: 'Lỗi',
                          description: 'Vui lòng nhập không quá 2000 ký tự');
                    }
                    if (value.trim().isNotEmpty ||
                        !diaryController.checkShowHideIconPostsDiary) {
                      diaryController.setCheckShowHideIconPostsDiary(true);
                    }
                    if (value.trim().isEmpty &&
                        diaryController.listXFileSelectPostsDiary.isEmpty) {
                      diaryController.setCheckShowHideIconPostsDiary(false);
                    }
                  },
                  maxLength: Numeral.LENGTH_POSTS_DIARY,
                  keyboardType: TextInputType.multiline,
                  minLines: 1,
                  maxLines: 100,
                  decoration: InputDecoration(
                    counterText: '',
                    hintText: 'Bạn đang nghĩ gì',
                    hintStyle:
                        TextStyle(color: DefaultTheme.GREY, fontSize: 13),
                    enabledBorder: const UnderlineInputBorder(
                        borderRadius: BorderRadius.zero,
                        borderSide: BorderSide.none),
                    focusedBorder: const UnderlineInputBorder(
                        borderRadius: BorderRadius.zero,
                        borderSide: BorderSide.none),
                  ),
                ),
              ),
            ),
          ],
        ));
  }
}
