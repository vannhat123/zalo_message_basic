import 'package:flutter/material.dart';
import 'package:message_za_lo/commons/constants/numeral.dart';
import 'package:message_za_lo/commons/constants/theme.dart';
import 'package:message_za_lo/commons/utils/upload_image.dart';
class DiaryPostsStatusFooter extends StatelessWidget {
  const DiaryPostsStatusFooter({Key? key,required this.size}) : super(key: key);
  final Size size;
  @override
  Widget build(BuildContext context) {
    return Container(
      alignment: Alignment.topCenter,
      height: size.height * 0.1,
      decoration: BoxDecoration(
          border: Border.symmetric(
              horizontal: BorderSide(
                  width: 1,
                  color: DefaultTheme.GREY
                      .withOpacity(Numeral.OPACITY_COLOR_GREY_DIVIDE)))),
      padding: EdgeInsets.all(10),
      child: Row(
        children: [
          Icon(
            Icons.sentiment_satisfied_alt_outlined,
            size: 22,
            color: DefaultTheme.GREY,
          ),
          Spacer(),
          InkWell(
              onTap: () async {
                UploadImage.instance.selectImagePostsDiary(context);
              },
              child: Icon(
                Icons.image_outlined,
                size: 22,
                color: DefaultTheme.GREY,
              )),
          SizedBox(
            width: 15,
          ),
          Icon(
            Icons.ondemand_video_outlined,
            size: 22,
            color: DefaultTheme.GREY,
          ),
          SizedBox(
            width: 13,
          ),
          Icon(
            Icons.animation_outlined,
            size: 22,
            color: DefaultTheme.GREY,
          ),
          SizedBox(
            width: 10,
          ),
          Icon(
            Icons.location_pin,
            size: 22,
            color: DefaultTheme.GREY,
          ),
        ],
      ),
    );
  }
}
