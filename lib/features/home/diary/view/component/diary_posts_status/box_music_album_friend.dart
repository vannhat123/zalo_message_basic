import 'dart:math';

import 'package:flutter/material.dart';
import 'package:message_za_lo/commons/constants/numeral.dart';
import 'package:message_za_lo/commons/constants/theme.dart';
class BoxMusicAlbumFriend extends StatelessWidget {
  const BoxMusicAlbumFriend({Key? key,required this.title, required this.iconData, required this.checkRotate}) : super(key: key);
  final String title;
  final IconData iconData;
  final bool checkRotate;
  @override
  Widget build(BuildContext context) {
    return Container(
      padding: EdgeInsets.all(7),
      decoration: BoxDecoration(
          borderRadius: BorderRadius.all(Radius.circular(6)),
          border: Border.all(
              width: 1,
              color: DefaultTheme.GREY
                  .withOpacity(Numeral.OPACITY_COLOR_GREY_DIVIDE)),
          color: DefaultTheme.WHITE),
      alignment: Alignment.center,
      child: Row(
        children: [
          if (checkRotate)
            Transform.rotate(
              angle: 90 * pi / 180,
              child: Icon(
                iconData,
                color: Colors.black,
                size: 11,
              ),
            )
          else
            Icon(
              iconData,
              color: DefaultTheme.BLACK,
              size: 11,
            ),
          SizedBox(
            width: 5,
          ),
          Text(
            title,
            style: TextStyle(fontSize: 11, color: DefaultTheme.BLACK),
          )
        ],
      ),
    );
  }
}
