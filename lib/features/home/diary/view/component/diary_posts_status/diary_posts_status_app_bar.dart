import 'dart:io';

import 'package:flutter/material.dart';
import 'package:get/get.dart';
import 'package:intl/intl.dart';
import 'package:message_za_lo/commons/constants/numeral.dart';
import 'package:message_za_lo/commons/constants/theme.dart';
import 'package:message_za_lo/commons/widgets/widgets.dart';
import 'package:message_za_lo/features/home/diary/controller/diary_controller.dart';
import 'package:message_za_lo/models/diary_posts_dto.dart';
import 'package:message_za_lo/services/firebase/firestore_helper.dart';
import 'package:message_za_lo/services/internet/internet_helper.dart';

class DiaryPostsStatusAppBar extends StatelessWidget {
  const DiaryPostsStatusAppBar(
      {Key? key,
      required this.size,
      required this.diaryController,
      required this.index})
      : super(key: key);
  final Size size;
  final DiaryController diaryController;
  final int index;

  @override
  Widget build(BuildContext context) {
    return Container(
      color: DefaultTheme.GREY.withOpacity(Numeral.OPACITY_COLOR_GREY),
      height: size.height * 0.1,
      width: size.width,
      padding: EdgeInsets.symmetric(horizontal: 10, vertical: 2),
      alignment: Alignment.bottomLeft,
      child: Row(
        crossAxisAlignment: CrossAxisAlignment.end,
        children: [
          GestureDetector(
              onTap: () {
                if ((diaryController.listXFileSelectPostsDiary.isNotEmpty ||
                        diaryController.textPostsController.text
                            .trim()
                            .isNotEmpty) &&
                    !diaryController.checkConfigPosts) {
                  DialogWidget.instance.openConfirmDialog(
                      context: context,
                      title: '',
                      description:
                          'Chưa tạo bài đăng xong. Thoát khỏi trang này',
                      function: () {
                        diaryController.clearListXFileSelectPostsDiary();
                        diaryController.setCheckShowHideIconPostsDiary(false);
                        Navigator.pop(context);
                        diaryController.textPostsController.text = '';
                      });
                } else if (diaryController.checkConfigPosts &&
                    (diaryController.textPostsController.text.trim() !=
                            diaryController
                                .listDiaryPostsDTO[index].postsContent ||
                        diaryController
                                .listDiaryPostsDTO[index].postsImageUrl !=
                            diaryController.listImageNetwork) &&
                    index != Numeral.VALUE_NONE_EXITS) {
                  DialogWidget.instance.openConfirmDialog(
                      context: context,
                      title: '',
                      description:
                          'Chưa tạo bài đăng xong. Thoát khỏi trang này',
                      function: () {
                        diaryController.clearListXFileSelectPostsDiary();
                        diaryController.setCheckShowHideIconPostsDiary(false);
                        Navigator.pop(context);
                        diaryController.textPostsController.text = '';
                      });
                } else {
                  Navigator.pop(context);
                  diaryController.clearListXFileSelectPostsDiary();
                  diaryController.setCheckShowHideIconPostsDiary(false);
                  diaryController.textPostsController.text = '';
                }
              },
              child: Padding(
                padding: const EdgeInsets.only(bottom: 4),
                child: Icon(
                  Icons.close,
                  size: 25,
                ),
              )),
          SizedBox(
            width: 15,
          ),
          Container(
            child: Column(
              crossAxisAlignment: CrossAxisAlignment.start,
              mainAxisAlignment: MainAxisAlignment.end,
              children: [
                Row(
                  children: [
                    Icon(
                      Icons.people,
                      size: 14,
                      color: DefaultTheme.BLACK,
                    ),
                    SizedBox(
                      width: 3,
                    ),
                    Text(
                      "Tất cả bạn bè",
                      style: DefaultTheme.TEXT_SUB_BUTTON_BLACK,
                    ),
                    Container(
                        margin: EdgeInsets.only(bottom: 2),
                        child: Icon(
                          Icons.arrow_drop_down_sharp,
                          color: DefaultTheme.BLACK,
                          size: 18,
                        ))
                  ],
                ),
                SizedBox(
                  height: 1,
                ),
                Text(
                  "Xem bởi bạn bè trên Zalo",
                  style: TextStyle(color: DefaultTheme.GREY, fontSize: 11.2),
                )
              ],
            ),
          ),
          Spacer(),
          Row(
            crossAxisAlignment: CrossAxisAlignment.center,
            children: [
              Container(
                margin: EdgeInsets.only(bottom: 5, right: 15),
                width: 52,
                height: 25,
                decoration: BoxDecoration(
                    color: DefaultTheme.THEME_COLOR,
                    borderRadius: BorderRadius.all(Radius.circular(50))),
                child: Row(
                  mainAxisAlignment: MainAxisAlignment.spaceBetween,
                  children: [
                    Container(
                      alignment: Alignment.center,
                      margin: EdgeInsets.only(left: 1),
                      width: 25,
                      height: 24,
                      decoration: BoxDecoration(
                          color: DefaultTheme.WHITE,
                          borderRadius: BorderRadius.all(Radius.circular(50))),
                      child: Text(
                        "Aa",
                        style: TextStyle(
                            color: DefaultTheme.THEME_COLOR, fontSize: 12),
                      ),
                    ),
                    Container(
                        margin: EdgeInsets.only(bottom: 2),
                        width: 26,
                        alignment: Alignment.center,
                        child: Icon(
                          Icons.cleaning_services,
                          size: 13,
                          color: DefaultTheme.WHITE.withOpacity(0.5),
                        )),
                  ],
                ),
              ),
              GetBuilder<DiaryController>(
                  init: DiaryController(),
                  builder: (controller) {
                    return (controller.checkShowHideIconPostsDiary)
                        ? GestureDetector(
                            onTap: () async {
                              FocusScope.of(context).requestFocus(FocusNode());
                              diaryController.listXFileSelectPostsDiary;
                              DateTime dateTimeNow = DateTime.now();
                              String dateFull =
                                  DateFormat('dd/MM/yyyy').format(dateTimeNow);
                              String dateFormat =
                                  DateFormat('dd/MM').format(dateTimeNow);
                              String hour = '';
                              String minutes = '';
                              List<File> listFile = [];
                              List<PostsImage> listPostsImage = [];
                              DialogWidget.instance
                                  .openLoadingDialog(context: context);
                              for (var item in diaryController
                                  .listXFileSelectPostsDiary) {
                                listFile.add(File(item.path));
                              }
                              listPostsImage = await FireStoreHelper()
                                  .uploadMultiFileToFirebaseStorage(
                                      listFile,
                                      context,
                                      Numeral
                                          .TYPE_UPDATE_PROFILE_IMAGE_POSTS_DIARY);
                              if (dateTimeNow.hour < 10) {
                                hour = "0${dateTimeNow.hour}";
                              } else {
                                hour = "${dateTimeNow.hour}";
                              }
                              if (dateTimeNow.minute < 10) {
                                minutes = "0${dateTimeNow.minute}";
                              } else {
                                minutes = "${dateTimeNow.minute}";
                              }
                              int sendingTime =
                                  dateTimeNow.millisecondsSinceEpoch;
                              String hourFormat = "$hour:$minutes";
                              bool checkInternet = await InternetHelper.instance
                                  .checkInternet(context);
                              if (checkInternet) {
                                if (diaryController.checkConfigPosts) {
                                  await FireStoreHelper().updateYourDiary(
                                    DiaryPostsDTO(
                                        dateFormat: dateFormat,
                                        dateFull: dateFull,
                                        hourFormat: hourFormat,
                                        postsComment: [],
                                        postsLike: [],
                                        postsImageUrl:
                                            diaryController.listImageNetwork,
                                        postsContent: controller
                                            .textPostsController.text
                                            .trim(),
                                        sendingTime: sendingTime),
                                  );
                                } else {
                                  await FireStoreHelper().postsYourDiary(
                                    DiaryPostsDTO(
                                        dateFormat: dateFormat,
                                        dateFull: dateFull,
                                        hourFormat: hourFormat,
                                        postsComment: [],
                                        postsLike: [],
                                        postsImageUrl: listPostsImage,
                                        postsContent: controller
                                            .textPostsController.text
                                            .trim(),
                                        sendingTime: sendingTime),
                                  );
                                }
                                diaryController.textPostsController.text = '';
                                diaryController
                                    .clearListXFileSelectPostsDiary();
                                Navigator.pop(context);
                              }
                              DialogWidget.instance.dismiss(context: context);
                            },
                            child: Container(
                              margin: EdgeInsets.only(bottom: 5),
                              child: Icon(
                                Icons.send_rounded,
                                size: 23,
                                color: DefaultTheme.THEME_COLOR,
                              ),
                            ),
                          )
                        : Container(
                            margin: EdgeInsets.only(bottom: 5),
                            child: Icon(
                              Icons.send_rounded,
                              size: 23,
                              color: DefaultTheme.THEME_COLOR.withOpacity(0.5),
                            ),
                          );
                  }),
            ],
          )
        ],
      ),
    );
  }
}
