import 'dart:io';

import 'package:flutter/material.dart';
import 'package:get/get.dart';
import 'package:message_za_lo/commons/constants/theme.dart';
import 'package:message_za_lo/commons/utils/get_size_image.dart';
import 'package:message_za_lo/features/home/diary/controller/diary_controller.dart';
import 'package:message_za_lo/features/home/diary/view/component/diary_posts_status/box_music_album_friend.dart';

class DiaryPostsStatusBody extends StatelessWidget {
  const DiaryPostsStatusBody(
      {Key? key, required this.diaryController, required this.size})
      : super(key: key);
  final DiaryController diaryController;
  final Size size;

  @override
  Widget build(BuildContext context) {
    return Column(
      children: [
        Padding(
          padding: const EdgeInsets.all(10),
          child: Row(
            children: [
              BoxMusicAlbumFriend(
                iconData: Icons.music_note,
                title: 'Nhạc',
                checkRotate: false,
              ),
              SizedBox(
                width: 5,
              ),
              BoxMusicAlbumFriend(
                iconData: Icons.image_outlined,
                title: 'Album',
                checkRotate: false,
              ),
              SizedBox(
                width: 5,
              ),
              BoxMusicAlbumFriend(
                iconData: Icons.loyalty_outlined,
                title: 'Với bạn bè',
                checkRotate: true,
              ),
            ],
          ),
        ),
        GetBuilder<DiaryController>(
            init: DiaryController(),
            builder: (controller) {
              return (diaryController.listXFileSelectPostsDiary.isNotEmpty)
                  ? _listImagePickerPostsDiaryWidget()
                  : (diaryController.listImageNetwork.isNotEmpty)
                      ? _listImageNetworkPostsDiaryWidget()
                      : SizedBox();
            }),
      ],
    );
  }

  Widget _listImagePickerPostsDiaryWidget() {
    return Container(
      padding: EdgeInsets.only(left: 10, right: 10),
      height: GetSizeImage.instance.getSizeImage(
          diaryController.listXFileSelectPostsDiary.length, size)['height'],
      child: GridView.builder(
          physics: NeverScrollableScrollPhysics(),
          gridDelegate: SliverGridDelegateWithMaxCrossAxisExtent(
              maxCrossAxisExtent: GetSizeImage.instance.getSizeImage(
                  diaryController.listXFileSelectPostsDiary.length,
                  size)['width']!,
              childAspectRatio: 1 / 1,
              crossAxisSpacing: 5,
              mainAxisSpacing: 5),
          itemCount: (diaryController.listXFileSelectPostsDiary.length < 9)
              ? diaryController.listXFileSelectPostsDiary.length
              : 9,
          itemBuilder: (context, index) {
            return (diaryController.listXFileSelectPostsDiary.length > 9 &&
                    index == 8)
                ? Stack(
                    children: [
                      Image.network(''),
                      SizedBox(
                        width: GetSizeImage.instance.getSizeImage(
                            diaryController.listXFileSelectPostsDiary.length,
                            size)['width']!,
                        height: GetSizeImage.instance.getSizeImage(
                            diaryController.listXFileSelectPostsDiary.length,
                            size)['height']!,
                        child: ClipRRect(
                          borderRadius: BorderRadius.circular(6.0),
                          child: Image.file(
                            File(diaryController
                                .listXFileSelectPostsDiary[index].path),
                            width: GetSizeImage.instance.getSizeImage(
                                diaryController
                                    .listXFileSelectPostsDiary.length,
                                size)['width']!,
                            height: GetSizeImage.instance.getSizeImage(
                                diaryController
                                    .listXFileSelectPostsDiary.length,
                                size)['height']!,
                            fit: BoxFit.cover,
                          ),
                        ),
                      ),
                      Positioned(
                          right: 2.5,
                          top: 3.2,
                          child: Icon(
                            Icons.cancel_outlined,
                            size: 20,
                            color: DefaultTheme.BLACK,
                          )),
                      Positioned(
                          right: 3,
                          top: 3,
                          child: InkWell(
                            onTap: () {
                              diaryController.decreaseXFilePostsDiary(
                                  diaryController
                                      .listXFileSelectPostsDiary[index]);
                            },
                            child: Icon(
                              Icons.cancel_outlined,
                              size: 20,
                              color: DefaultTheme.WHITE,
                            ),
                          )),
                      Container(
                        width: GetSizeImage.instance.getSizeImage(
                            diaryController.listXFileSelectPostsDiary.length,
                            size)['width']!,
                        height: GetSizeImage.instance.getSizeImage(
                            diaryController.listXFileSelectPostsDiary.length,
                            size)['height']!,
                        decoration: BoxDecoration(
                            color: DefaultTheme.GREY.withOpacity(0.5)),
                        child: Center(
                          child: Text(
                            '+${diaryController.listXFileSelectPostsDiary.length - 9}',
                            style: TextStyle(
                                color: DefaultTheme.WHITE, fontSize: 30),
                          ),
                        ),
                      ),
                    ],
                  )
                : Stack(
                    children: [
                      SizedBox(
                        width: GetSizeImage.instance.getSizeImage(
                            diaryController.listXFileSelectPostsDiary.length,
                            size)['width']!,
                        height: GetSizeImage.instance.getSizeImage(
                            diaryController.listXFileSelectPostsDiary.length,
                            size)['height']!,
                        child: ClipRRect(
                          borderRadius: BorderRadius.circular(6.0),
                          child: Image.file(
                            File(diaryController
                                .listXFileSelectPostsDiary[index].path),
                            width: GetSizeImage.instance.getSizeImage(
                                diaryController
                                    .listXFileSelectPostsDiary.length,
                                size)['width']!,
                            height: GetSizeImage.instance.getSizeImage(
                                diaryController
                                    .listXFileSelectPostsDiary.length,
                                size)['height']!,
                            fit: BoxFit.cover,
                          ),
                        ),
                      ),
                      Positioned(
                          right: 2.5,
                          top: 3.2,
                          child: Icon(
                            Icons.cancel_outlined,
                            size: 20,
                            color: DefaultTheme.BLACK,
                          )),
                      Positioned(
                          right: 3,
                          top: 3,
                          child: InkWell(
                            onTap: () {
                              diaryController.decreaseXFilePostsDiary(
                                  diaryController
                                      .listXFileSelectPostsDiary[index]);
                            },
                            child: Icon(
                              Icons.cancel_outlined,
                              size: 20,
                              color: DefaultTheme.WHITE,
                            ),
                          )),
                    ],
                  );
          }),
    );
  }

  Widget _listImageNetworkPostsDiaryWidget() {
    return Container(
      padding: EdgeInsets.only(left: 10, right: 10),
      height: GetSizeImage.instance.getSizeImage(
          diaryController.listImageNetwork.length, size)['height'],
      child: GridView.builder(
          physics: NeverScrollableScrollPhysics(),
          gridDelegate: SliverGridDelegateWithMaxCrossAxisExtent(
              maxCrossAxisExtent: GetSizeImage.instance.getSizeImage(
                  diaryController.listImageNetwork.length, size)['width']!,
              childAspectRatio: 1 / 1,
              crossAxisSpacing: 5,
              mainAxisSpacing: 5),
          itemCount: (diaryController.listImageNetwork.length < 9)
              ? diaryController.listImageNetwork.length
              : 9,
          itemBuilder: (context, index) {
            return (diaryController.listImageNetwork.length > 9 && index == 8)
                ? Stack(
                    children: [
                      Image.network(''),
                      SizedBox(
                        width: GetSizeImage.instance.getSizeImage(
                            diaryController.listImageNetwork.length,
                            size)['width']!,
                        height: GetSizeImage.instance.getSizeImage(
                            diaryController.listImageNetwork.length,
                            size)['height']!,
                        child: ClipRRect(
                          borderRadius: BorderRadius.circular(6.0),
                          child: Image.network(
                            diaryController.listImageNetwork[index].urlImage,
                            width: GetSizeImage.instance.getSizeImage(
                                diaryController
                                    .listImageNetwork.length,
                                size)['width']!,
                            height: GetSizeImage.instance.getSizeImage(
                                diaryController
                                    .listImageNetwork.length,
                                size)['height']!,
                            fit: BoxFit.cover,
                          ),
                        ),
                      ),
                      Positioned(
                          right: 2.5,
                          top: 3.2,
                          child: Icon(
                            Icons.cancel_outlined,
                            size: 20,
                            color: DefaultTheme.BLACK,
                          )),
                      Positioned(
                          right: 3,
                          top: 3,
                          child: InkWell(
                            onTap: () {
                              diaryController.setCheckShowHideIconPostsDiary(true);
                              diaryController.decreaseListImageNetwork(
                                  diaryController.listImageNetwork[index]);
                            },
                            child: Icon(
                              Icons.cancel_outlined,
                              size: 20,
                              color: DefaultTheme.WHITE,
                            ),
                          )),
                      Container(
                        width: GetSizeImage.instance.getSizeImage(
                            diaryController.listImageNetwork.length,
                            size)['width']!,
                        height: GetSizeImage.instance.getSizeImage(
                            diaryController.listImageNetwork.length,
                            size)['height']!,
                        decoration: BoxDecoration(
                            color: DefaultTheme.GREY.withOpacity(0.5)),
                        child: Center(
                          child: Text(
                            '+${diaryController.listImageNetwork.length - 9}',
                            style: TextStyle(
                                color: DefaultTheme.WHITE, fontSize: 30),
                          ),
                        ),
                      ),
                    ],
                  )
                : Stack(
                    children: [
                      SizedBox(
                        width: GetSizeImage.instance.getSizeImage(
                            diaryController.listImageNetwork.length,
                            size)['width']!,
                        height: GetSizeImage.instance.getSizeImage(
                            diaryController.listImageNetwork.length,
                            size)['height']!,
                        child: ClipRRect(
                          borderRadius: BorderRadius.circular(6.0),
                          child: Image.network(
                            diaryController.listImageNetwork[index].urlImage,
                            width: GetSizeImage.instance.getSizeImage(
                                diaryController.listImageNetwork.length,
                                size)['width']!,
                            height: GetSizeImage.instance.getSizeImage(
                                diaryController.listImageNetwork.length,
                                size)['height']!,
                            fit: BoxFit.cover,
                          ),
                        ),
                      ),
                      Positioned(
                          right: 2.5,
                          top: 3.2,
                          child: Icon(
                            Icons.cancel_outlined,
                            size: 20,
                            color: DefaultTheme.BLACK,
                          )),
                      Positioned(
                          right: 3,
                          top: 3,
                          child: InkWell(
                            onTap: () {
                              diaryController.setCheckShowHideIconPostsDiary(true);
                              diaryController.decreaseListImageNetwork(
                                  diaryController.listImageNetwork[index]);
                            },
                            child: Icon(
                              Icons.cancel_outlined,
                              size: 20,
                              color: DefaultTheme.WHITE,
                            ),
                          )),
                    ],
                  );
          }),
    );
  }
}
