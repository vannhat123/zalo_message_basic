import 'package:flutter/material.dart';
import 'package:get/get.dart';
import 'package:message_za_lo/commons/constants/numeral.dart';
import 'package:message_za_lo/commons/constants/theme.dart';
import 'package:message_za_lo/features/home/diary/controller/diary_controller.dart';
import 'package:message_za_lo/features/home/diary/view/component/diary_modal_bottom_sheet_comment.dart';
import 'package:message_za_lo/features/home/diary/view/component/diary_modal_bottom_sheet_config_post.dart';
import 'package:message_za_lo/models/diary_posts_dto.dart';
import 'package:message_za_lo/services/firebase/firestore_helper.dart';
import 'package:message_za_lo/services/shared_preferences/account_helper.dart';

class DiaryBoxPost extends StatelessWidget {
  const DiaryBoxPost(
      {Key? key,
      required this.size,
      required this.diaryPostsDTO,
      required this.diaryController,
      required this.index})
      : super(key: key);
  final Size size;
  final DiaryPostsDTO diaryPostsDTO;
  final DiaryController diaryController;
  final int index;

  Map<String, double> getSizeImage(int lengthListImage) {
    double width = 0;
    double height = 0;
    Map<String, double> result = {'width': width, 'height': height};
    if (lengthListImage == 1) {
      width = size.width;
      height = size.height * 0.3;
    } else if (lengthListImage == 2) {
      width = size.width * 0.5;
      height = size.height * 0.225;
    } else {
      width = size.width * 0.333;
      height = size.width * 0.33;
    }
    result['width'] = width;
    result['height'] = height;
    return result;
  }

  bool checkLikeYourPosts() {
    bool checkLike = false;
    for (var item in diaryPostsDTO.postsLike) {
      if (diaryController.profileDTO.fullName == item.likeUserName) {
        checkLike = true;
        break;
      } else {
        checkLike = false;
      }
    }
    return checkLike;
  }

  @override
  Widget build(BuildContext context) {
    return Container(
      margin:
          EdgeInsets.symmetric(vertical: Numeral.PADDING_LEFT_RIGHT_PRIMARY),
      child: Column(
        crossAxisAlignment: CrossAxisAlignment.start,
        children: [
          Row(
            crossAxisAlignment: CrossAxisAlignment.center,
            children: [
              Container(
                  margin: EdgeInsets.only(
                      left: Numeral.PADDING_LEFT_RIGHT_PRIMARY, right: 15),
                  height: 45,
                  width: 45,
                  child: CircleAvatar(
                    backgroundImage:
                        NetworkImage(AccountHelper.instance.getAvatarUrl()),
                  )),
              Container(
                child: Column(
                  crossAxisAlignment: CrossAxisAlignment.start,
                  children: [
                    Text(
                      diaryController.profileDTO.fullName,
                      style: DefaultTheme.TEXT_STYLE_POST_PRIMARY,
                    ),
                    SizedBox(
                      height: 2,
                    ),
                    Text(
                      "${diaryPostsDTO.hourFormat}  ${diaryPostsDTO.dateFull}",
                      style: DefaultTheme.TEXT_STYLE_POST_SUB,
                    )
                  ],
                ),
              ),
              const Spacer(),
              InkWell(
                onTap: () {
                  modalSheetSelectOptionConfigPostsWidget(context, size);
                },
                child: Container(
                    margin: EdgeInsets.only(bottom: 8),
                    decoration: BoxDecoration(
                        color: DefaultTheme.WHITE.withOpacity(0),
                        borderRadius: BorderRadius.all(Radius.circular(50))),
                    padding: EdgeInsets.all(4),
                    child: Text(
                      '•••',
                      style: TextStyle(color: DefaultTheme.GREY, fontSize: 18),
                    )),
              ),
              SizedBox(
                width: Numeral.PADDING_LEFT_RIGHT_PRIMARY,
              ),
            ],
          ),
          Padding(
            padding: const EdgeInsets.symmetric(
                horizontal: Numeral.PADDING_LEFT_RIGHT_PRIMARY, vertical: 8),
            child: Text(
              diaryPostsDTO.postsContent,
              style: DefaultTheme.TEXT_STYLE_POST_CONTENT,
            ),
          ),
          if (diaryPostsDTO.postsImageUrl.isNotEmpty)
            SizedBox(
              width: size.width,
              height:
                  getSizeImage(diaryPostsDTO.postsImageUrl.length)['height']!,
              child: GridView.builder(
                  physics: NeverScrollableScrollPhysics(),
                  gridDelegate: SliverGridDelegateWithMaxCrossAxisExtent(
                      maxCrossAxisExtent: getSizeImage(
                          diaryPostsDTO.postsImageUrl.length)['width']!,
                      childAspectRatio: 1 / 1,
                      crossAxisSpacing: 1,
                      mainAxisSpacing: 1),
                  itemCount: (diaryPostsDTO.postsImageUrl.length < 3)
                      ? diaryPostsDTO.postsImageUrl.length
                      : 3,
                  itemBuilder: (context, index) {
                    return (diaryPostsDTO.postsImageUrl.length > 3 &&
                            index == 2)
                        ? Stack(
                            children: [
                              SizedBox(
                                width: getSizeImage(diaryPostsDTO
                                    .postsImageUrl.length)['width']!,
                                height: getSizeImage(diaryPostsDTO
                                    .postsImageUrl.length)['height']!,
                                child: Image.network(
                                  diaryPostsDTO.postsImageUrl[index].urlImage,
                                  width: getSizeImage(diaryPostsDTO
                                      .postsImageUrl.length)['width']!,
                                  height: getSizeImage(diaryPostsDTO
                                      .postsImageUrl.length)['height']!,
                                  fit: BoxFit.cover,
                                ),
                              ),
                              Container(
                                width: getSizeImage(diaryPostsDTO
                                    .postsImageUrl.length)['width']!,
                                height: getSizeImage(diaryPostsDTO
                                    .postsImageUrl.length)['height']!,
                                decoration: BoxDecoration(
                                    color: DefaultTheme.GREY.withOpacity(0.5)),
                                child: Center(
                                  child: Text(
                                    '+${diaryPostsDTO.postsImageUrl.length - 3}',
                                    style: TextStyle(
                                        color: DefaultTheme.WHITE,
                                        fontSize: 30),
                                  ),
                                ),
                              ),
                            ],
                          )
                        : SizedBox(
                            width: getSizeImage(
                                diaryPostsDTO.postsImageUrl.length)['width']!,
                            height: getSizeImage(
                                diaryPostsDTO.postsImageUrl.length)['height']!,
                            child: Image.network(
                              diaryPostsDTO.postsImageUrl[index].urlImage,
                              width: getSizeImage(
                                  diaryPostsDTO.postsImageUrl.length)['width']!,
                              height: getSizeImage(diaryPostsDTO
                                  .postsImageUrl.length)['height']!,
                              fit: BoxFit.cover,
                            ),
                          );
                  }),
            ),
          Container(
              padding: EdgeInsets.symmetric(
                  vertical: 12, horizontal: Numeral.PADDING_LEFT_RIGHT_PRIMARY),
              decoration: BoxDecoration(
                  border: Border(
                      bottom: BorderSide(
                          width: Numeral.HEIGHT_DIVIDE_CONTAINER_DIARY,
                          color: DefaultTheme.GREY
                              .withOpacity(Numeral.OPACITY_COLOR_GREY)))),
              child: Row(
                children: [
                  GetBuilder<DiaryController>(
                      init: DiaryController(),
                      builder: (controller) {
                        return !checkLikeYourPosts()
                            ? GestureDetector(
                                onTap: () {
                                  FireStoreHelper().likePostsDiary(
                                      PostsLike(
                                          likeUserName: diaryController
                                              .profileDTO.fullName,
                                          likeImageUrl: diaryController
                                              .profileDTO.avatarUrl),
                                      diaryPostsDTO.sendingTime);
                                },
                                child: Icon(
                                  Icons.favorite_border_rounded,
                                  color: DefaultTheme.BLACK,
                                ),
                              )
                            : GestureDetector(
                                onTap: () {
                                  FireStoreHelper().removeLikePostsDiary(
                                      PostsLike(
                                          likeUserName: diaryController
                                              .profileDTO.fullName,
                                          likeImageUrl: diaryController
                                              .profileDTO.avatarUrl),
                                      diaryPostsDTO.sendingTime);
                                },
                                child: Icon(
                                  Icons.favorite,
                                  color: DefaultTheme.RED,
                                ),
                              );
                      }),
                  SizedBox(
                    width: 5,
                  ),
                  Text(
                    diaryPostsDTO.postsLike.length.toString(),
                    style: DefaultTheme.TEXT_STYLE_POST_PRIMARY_GREY,
                  ),
                  SizedBox(
                    width: 20,
                  ),
                  InkWell(
                    onTap: () {
                      Get.put(DiaryController()).setSize(size);
                      modalSheetAvatarWidget(context, size);
                    },
                    child: Icon(
                      Icons.messenger_outline,
                      size: 20,
                      color: DefaultTheme.BLACK,
                    ),
                  ),
                  SizedBox(
                    width: 5,
                  ),
                  Text(
                    diaryPostsDTO.postsComment.length.toString(),
                    style: DefaultTheme.TEXT_STYLE_POST_PRIMARY_GREY,
                  ),
                ],
              ))
        ],
      ),
    );
  }

  modalSheetAvatarWidget(BuildContext context, Size size) {
    showModalBottomSheet(
        isScrollControlled: true,
        context: context,
        backgroundColor: DefaultTheme.WHITE.withOpacity(0),
        shape: RoundedRectangleBorder(
          borderRadius: BorderRadius.only(
              topLeft: Radius.circular(15.0), topRight: Radius.circular(15.0)),
        ),
        builder: (context) {
          return DiaryModalBottomSheetComment(
              size: size, diaryController: diaryController, index: index);
        });
  }

  modalSheetSelectOptionConfigPostsWidget(BuildContext context, Size size) {
    showModalBottomSheet(
        isScrollControlled: true,
        context: context,
        backgroundColor: DefaultTheme.WHITE,
        shape: RoundedRectangleBorder(
          borderRadius: BorderRadius.only(
              topLeft: Radius.circular(15.0), topRight: Radius.circular(15.0)),
        ),
        builder: (context) {
          return DiaryModalBottomSheetConfigPost(
            size: size,
            diaryController: diaryController,
            index: index,
            diaryPostsDTO: diaryPostsDTO,
          );
        });
  }
}
