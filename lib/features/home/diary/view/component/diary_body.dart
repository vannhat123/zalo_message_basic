import 'package:flutter/material.dart';
import 'package:get/get.dart';
import 'package:message_za_lo/commons/constants/numeral.dart';
import 'package:message_za_lo/commons/constants/theme.dart';
import 'package:message_za_lo/commons/routes/image_asset.dart';
import 'package:message_za_lo/features/home/diary/controller/diary_controller.dart';
import 'package:message_za_lo/features/home/diary/view/component/diary_box_post.dart';
import 'package:message_za_lo/features/home/diary/view/component/diary_post_moment.dart';
import 'package:message_za_lo/features/home/diary/view/component/diary_posts_status/diary_posts_status.dart';
import 'package:message_za_lo/services/shared_preferences/account_helper.dart';

class DiaryBody extends StatelessWidget {
  const DiaryBody({Key? key, required this.diaryController}) : super(key: key);
  final DiaryController diaryController;

  @override
  Widget build(BuildContext context) {
    Size size = MediaQuery.of(context).size;
    return Column(
      children: [
        _selectPushImageVideoAlbum(context, size),
        _listInputMomentWidget(size),
        if (diaryController.listDiaryPostsDTO.isNotEmpty)
          Expanded(
            child: ListView.builder(
                itemCount: diaryController.listDiaryPostsDTO.length,
                itemBuilder: (context, index) {
                  return DiaryBoxPost(
                    size: size,
                    diaryPostsDTO: diaryController.listDiaryPostsDTO[index],
                    diaryController: diaryController,
                    index: index,
                  );
                }),
          )
        else
          Expanded(child: Center(child: Text("Không có bài viết nào"))),
      ],
    );
  }

  Widget _selectPushImageVideoAlbum(BuildContext context, Size size) {
    return Column(
      children: [
        SizedBox(
          height: 10,
        ),
        Row(
          crossAxisAlignment: CrossAxisAlignment.center,
          children: [
            Container(
                margin: EdgeInsets.only(
                    left: Numeral.PADDING_LEFT_RIGHT_PRIMARY, right: 15),
                height: 45,
                width: 45,
                child: CircleAvatar(
                  backgroundImage:
                      NetworkImage(AccountHelper.instance.getAvatarUrl()),
                )),
            GestureDetector(
              onTap: () {
                modalSheetPostsStatusWidget(context, size);
                Get.put(DiaryController()).setCheckConfigPosts(false);
              },
              child: Text(
                "Hôm nay bạn thế nào?",
                style: DefaultTheme.TEXT_SUB_BUTTON_GREY,
              ),
            ),
          ],
        ),
        Padding(
          padding: const EdgeInsets.all(Numeral.PADDING_LEFT_RIGHT_PRIMARY),
          child: Row(
            mainAxisAlignment: MainAxisAlignment.spaceBetween,
            children: [
              _boxImageTextSelect(
                  size, Icons.image, "Đăng ảnh", DefaultTheme.GREEN),
              _boxImageTextSelect(size, Icons.video_camera_back_rounded,
                  "Đăng video", DefaultTheme.RED.withOpacity(0.7)),
              _boxImageTextSelect(size, Icons.photo_album, "Tạo album",
                  DefaultTheme.THEME_COLOR),
            ],
          ),
        ),
      ],
    );
  }

  Widget _boxImageTextSelect(
      Size size, IconData iconData, String title, Color color) {
    return Container(
      alignment: Alignment.center,
      height: size.width * 0.071,
      width: size.width * 0.28,
      decoration: BoxDecoration(
        color: DefaultTheme.GREY.withOpacity(Numeral.OPACITY_COLOR_GREY),
        borderRadius: BorderRadius.all(Radius.circular(30)),
      ),
      child: Row(
        mainAxisAlignment: MainAxisAlignment.center,
        children: [
          Icon(
            Icons.image,
            color: color,
            size: Numeral.SIZE_ICON_IMAGE_VIDEO_ALBUM,
          ),
          SizedBox(
            width: 5,
          ),
          Text(
            title,
            style: DefaultTheme.TEXT_STYLE_IMAGE_VIDEO_ALBUM,
          )
        ],
      ),
    );
  }

  Widget _listInputMomentWidget(Size size) {
    return Container(
      decoration: BoxDecoration(
        border: Border.symmetric(
            horizontal: BorderSide(
                width: Numeral.HEIGHT_DIVIDE_CONTAINER_DIARY,
                color:
                    DefaultTheme.GREY.withOpacity(Numeral.OPACITY_COLOR_GREY))),
      ),
      child: Row(
        crossAxisAlignment: CrossAxisAlignment.center,
        mainAxisAlignment: MainAxisAlignment.start,
        children: [
          DiaryPostMoment(
              size: size,
              image: ImageAsset.IMAGE_SLIDER1,
              title: "Khoảnh khắc"),
          DiaryPostMoment(
              size: size, image: ImageAsset.IMAGE_SLIDER2, title: "Clarkkent"),
          DiaryPostMoment(
              size: size, image: ImageAsset.IMAGE_SLIDER3, title: "Hercules"),
        ],
      ),
    );
  }

  modalSheetPostsStatusWidget(BuildContext context, Size size) {
    showModalBottomSheet(
        enableDrag: false,
        isScrollControlled: true,
        context: context,
        backgroundColor: DefaultTheme.WHITE.withOpacity(0),
        shape: RoundedRectangleBorder(
          borderRadius: BorderRadius.only(
              topLeft: Radius.circular(15.0), topRight: Radius.circular(15.0)),
        ),
        builder: (context) {
          return DiaryPostsStatus(
            size: size,
            diaryController: diaryController,
            index: Numeral.VALUE_NONE_EXITS,
          );
        });
  }
}
