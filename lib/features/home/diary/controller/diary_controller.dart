import 'package:flutter/material.dart';
import 'package:get/get.dart';
import 'package:image_picker/image_picker.dart';
import 'package:message_za_lo/commons/constants/numeral.dart';
import 'package:message_za_lo/models/diary_posts_dto.dart';
import 'package:message_za_lo/models/profile_dto.dart';
import 'package:message_za_lo/services/firebase/firestore_helper.dart';
import 'package:message_za_lo/services/shared_preferences/account_helper.dart';

class DiaryController extends GetxController {
  List<DiaryPostsDTO> listDiaryPostsDTO = [];
  ProfileDTO profileDTO = ProfileDTO(
      dateBirth: '',
      fullName: '',
      gender: 0,
      phoneNumber: '',
      email: '',
      idDoc: 0,
      backgroundUrl: '',
      avatarUrl: '');
  double heightInputComment = 0.0;
  late Size size;
  bool checkInputComment = false;
  double heightTextMessage = 0;
  final textCommentController = TextEditingController();
  bool checkFocusChatController = false;
  double valueCheckWidthTextAndSetHeightTextField = 0;
  int numLines = Numeral.VALUE_DEFAULT_COUNT_LINE_CHAT;
  final keyContainerPrimary = GlobalKey();
  final keyIconSub = GlobalKey();
  final chatScrollController = ScrollController();
  int copyNumLines = 1;
  double valueHeightEnterAdd = 0;
  DateTime dateTimeNow = DateTime.now();
  String date = '';
  String hour = '';
  String minutes = '';
  int sendingTime = 0;
  String hourFormat = '';
  String email = AccountHelper.instance.getEmailAccount();
  List<XFile> listXFileSelectPostsDiary = [];
  final textPostsController = TextEditingController();
  bool checkShowHideIconPostsDiary = false;
  bool checkShowHideIconFavorite = false;
  List<PostsImage> listImageNetwork = [];
  bool checkConfigPosts = false;

  @override
  void onInit() {
    // TODO: implement onInit
    super.onInit();
    getAccount();
    getListPosts();
  }

  void getAccount() {
    FireStoreHelper.fireStore
        .collection('Profile')
        .snapshots()
        .listen((value) async {
      if (value.docs.isNotEmpty) {
        Stream<ProfileDTO> streamProfileDTO = await FireStoreHelper()
            .getProfileFirebase(AccountHelper.instance.getEmailAccount());
        streamProfileDTO.listen((event) {
          profileDTO = event;
          AccountHelper.instance.setAvatarUrl(profileDTO.avatarUrl);
          AccountHelper.instance.setBackgroundUrl(profileDTO.backgroundUrl);
          update();
        });
      }
    });
  }

  void setHeightInputComment(double value) {
    heightInputComment = value;
    update();
  }

  void setSize(Size valueSize) {
    size = valueSize;
    setHeightInputComment(size.height * 0.5);
    update();
  }

  void setCheckInputComment(bool value) {
    checkInputComment = value;
    update();
  }

  void setCheckFocusChatController(bool value) {
    checkFocusChatController = value;
    update();
  }

  void setValueCheckWidthTextAndSetHeightTextField(
      double fontSize, bool? checkEnter) {
    TextPainter textPainter = TextPainter()
      ..text = TextSpan(
          text: textCommentController.text,
          style: TextStyle(fontSize: fontSize))
      ..textDirection = TextDirection.ltr
      ..layout(minWidth: 0, maxWidth: double.infinity);
    valueCheckWidthTextAndSetHeightTextField = textPainter.size.width;
    double resultDivisionWidthTextAndWidthWidget =
        valueCheckWidthTextAndSetHeightTextField / (getSize() - 10 - 7);
    if (checkEnter == true) {
      if (valueHeightEnterAdd < 60) {
        valueHeightEnterAdd += 20;
      }
    }
    if (checkEnter == false) {
      if (valueHeightEnterAdd > 0) {
        valueHeightEnterAdd -= 20;
      }
    }
    if (resultDivisionWidthTextAndWidthWidget < 1) {
      if (heightTextMessage + valueHeightEnterAdd <= 60) {
        heightTextMessage = 0 + valueHeightEnterAdd;
      } else {
        heightTextMessage = 60;
      }
    }
    if (resultDivisionWidthTextAndWidthWidget >= 1 &&
        resultDivisionWidthTextAndWidthWidget < 2) {
      if (heightTextMessage + valueHeightEnterAdd <= 60) {
        heightTextMessage = 20 + valueHeightEnterAdd;
      } else {
        heightTextMessage = 60;
      }
    }
    if (resultDivisionWidthTextAndWidthWidget >= 2 &&
        resultDivisionWidthTextAndWidthWidget < 3) {
      if (heightTextMessage + valueHeightEnterAdd <= 60) {
        heightTextMessage = 40 + valueHeightEnterAdd;
      } else {
        heightTextMessage = 60;
      }
    }
    if (resultDivisionWidthTextAndWidthWidget >= 3) {
      if (heightTextMessage < 60) {
        heightTextMessage = 60;
      }
    }
    update();
  }

  double getSize() {
    final sizePrimary = keyContainerPrimary.currentContext!.size;
    final sizeSub = keyIconSub.currentContext?.size;
    if (sizePrimary != null && sizeSub != null) {
      return sizePrimary.width - sizeSub.width;
    } else {
      return 0;
    }
  }

  void getListPosts() {
    FireStoreHelper.fireStore
        .collection('Diary/$email/posts-diary')
        .snapshots()
        .listen((value) async {
      if (value.docs.isNotEmpty) {
        Stream<List<DiaryPostsDTO>> streamListDiaryPostsDTO =
            await FireStoreHelper().getListPostsDiary();
        streamListDiaryPostsDTO.listen((event) {
          listDiaryPostsDTO = event;
          update();
        });
      }
    });
  }

  void getPostsComment() {
    FireStoreHelper.fireStore
        .collection('Diary/$email/posts-diary')
        .snapshots()
        .listen((value) async {
      if (value.docs.isNotEmpty) {
        Stream<List<DiaryPostsDTO>> streamListDiaryPostsDTO =
            await FireStoreHelper().getListPostsDiary();
        streamListDiaryPostsDTO.listen((event) {
          listDiaryPostsDTO = event;
          update();
        });
      }
    });
  }

  void setListXFileSelectPostsDiary(List<XFile> list) {
    clearListXFileSelectPostsDiary();
    clearListImageNetWork();
    listXFileSelectPostsDiary.addAll(list);
    if (listXFileSelectPostsDiary.isEmpty &&
        textPostsController.text.trim().isEmpty) {
      setCheckShowHideIconPostsDiary(false);
    } else {
      setCheckShowHideIconPostsDiary(true);
    }
    update();
  }

  void clearListXFileSelectPostsDiary() {
    clearListImageNetWork();
    listXFileSelectPostsDiary.clear();
    update();
  }

  void decreaseXFilePostsDiary(XFile xFile) {
    if (listXFileSelectPostsDiary.isNotEmpty) {
      listXFileSelectPostsDiary.remove(xFile);
      if (listXFileSelectPostsDiary.isEmpty &&
          textPostsController.text.trim().isEmpty) {
        setCheckShowHideIconPostsDiary(false);
      } else {
        setCheckShowHideIconPostsDiary(true);
      }
    }
    update();
  }

  void setCheckShowHideIconPostsDiary(bool value) {
    checkShowHideIconPostsDiary = value;
    update();
  }

  void setCheckShowHideIconFavorite(int index) {
    for (var item in listDiaryPostsDTO[index].postsLike) {
      if (profileDTO.fullName == item.likeUserName) {
        checkShowHideIconFavorite = true;
        break;
      } else {
        checkShowHideIconFavorite = false;
      }
    }
    update();
  }

  void setDiaryPostsDTO(DiaryPostsDTO diaryPostsDTOParameter) {
    textPostsController.value = textPostsController.value
        .copyWith(text: diaryPostsDTOParameter.postsContent);
    for (var item in diaryPostsDTOParameter.postsImageUrl) {
      listImageNetwork.add(item);
    }
    update();
  }

  void decreaseListImageNetwork(PostsImage postsImage) {
    listImageNetwork.remove(postsImage);
    update();
  }

  void setCheckConfigPosts(bool value){
    checkConfigPosts = value;
    update();
  }

  void clearListImageNetWork(){
    listImageNetwork.clear();
    update();
  }
}
