import 'package:flutter/cupertino.dart';
import 'package:get/get.dart';
import 'package:message_za_lo/services/firebase/firestore_helper.dart';

class SignUpController extends GetxController {
  final phoneNumberController = TextEditingController();
  final nameController = TextEditingController();
  bool checkColorSubmitButtonName = false;
  bool checkColorSubmitButtonPhone = false;
  bool checkIconClearName = false;
  int countAccount = 0;

  // sign up form
  final emailController = TextEditingController();
  final passwordController = TextEditingController();
  bool checkObscureText = true;
  bool checkClearIconNumber = false;
  bool checkClearIconPassword = false;
  bool checkColorSignInButton = false;
  bool checkErrorPassword = false;

  void setCheckColorSignInButtonName(bool? value) {
    if (nameController.text.trim().isNotEmpty && value == true) {
      checkColorSubmitButtonName = true;
    } else {
      checkColorSubmitButtonName = false;
    }
    update();
  }

  void setCheckColorSignInButtonPhone(bool? value) {
    if (phoneNumberController.text.trim().isNotEmpty && value == true) {
      checkColorSubmitButtonPhone = true;
    } else {
      checkColorSubmitButtonPhone = false;
    }
    update();
  }

  void setCheckIconClearName(bool value) {
    checkIconClearName = value;
    update();
  }

  //sign up form
  void setValueCheckObscureText(bool? value) {
    if (value != null) {
      checkObscureText = value;
    } else {
      checkObscureText = !checkObscureText;
    }
    update();
  }

  void setValueCheckClearIconNumber(bool value) {
    checkClearIconNumber = value;
    update();
  }

  void setCheckClearIconPassword(bool value) {
    checkClearIconPassword = value;
    update();
  }

  void setCheckColorSignButton(bool? value) {
    if (emailController.text.trim().isNotEmpty &&
        passwordController.text.trim().isNotEmpty &&
        value == true) {
      checkColorSignInButton = true;
    } else {
      checkColorSignInButton = false;
    }
    update();
  }

  void setCheckErrorPassword(bool value) {
    checkErrorPassword = value;
    update();
  }

  Future<void> setRegisterFirstProfileToFirebase() async {
    FireStoreHelper().setProfileToFirebase(
        fullName: nameController.text,
        dateBirth: '',
        gender: 0,
        urlImage: 'https://thuthuatnhanh.com/wp-content/uploads/2019/07/hinh-anh-avatar-chibi-cuc-cute-de-thuong-cho-facebook-7-459x580.jpg',
        phoneNumber: phoneNumberController.text,
        email: emailController.text,
        countDoc: countAccount + 1, backgroundUrl: 'https://thuthuatnhanh.com/wp-content/uploads/2019/07/hinh-anh-avatar-chibi-cuc-cute-de-thuong-cho-facebook-7-459x580.jpg');
    FireStoreHelper().setCountAccount(countAccount + 1);
  }

  void getAccount() {
    FireStoreHelper.fireStore
        .collection('Profile')
        .snapshots()
        .listen((value) async {
      if (value.docs.isNotEmpty) {
        Stream<int> getCount = await FireStoreHelper().getCountAccount();
        getCount.listen((event) {
          countAccount = event;
          update();
        });
      }
    });
  }
}
