import 'package:equatable/equatable.dart';
import 'package:flutter/material.dart';
import 'package:flutter_bloc/flutter_bloc.dart';
import 'package:message_za_lo/commons/utils/form_validator.dart';
import 'package:message_za_lo/commons/widgets/widgets.dart';
import 'package:message_za_lo/services/firebase/firestore_helper.dart';
import 'package:message_za_lo/services/internet/internet_helper.dart';

part 'sign_up_state.dart';

class SignUpCubit extends Cubit<SignUpState> {
  SignUpCubit() : super(SignUpState.initial());

  void emailChanged(String value) {
    emit(state.copyWith(email: value));
  }

  void passwordChanged(String value) {
    emit(state.copyWith(password: value));
  }

  Future<void> registerEmailPassword(
      String email, String password, BuildContext context) async {
    if(password.trim().length <6){
      DialogWidget.instance.openErrorDialog(
          context: context,
          title: "Lỗi",
          description: "Vui lòng nhập mật khẩu có ít nhất 6 ký tự");
    }
    else if (!FormValidator.instance.isEmail(email.trim())) {
      DialogWidget.instance.openErrorDialog(
          context: context,
          title: "Lỗi",
          description: "Vui lòng nhập email hợp lệ");
    } else if (password.trim() != password) {
      DialogWidget.instance.openErrorDialog(
          context: context,
          title: "Lỗi",
          description: "Mật khẩu không được để khoảng trắng");
    } else {
      bool checkSignUp = false;
      bool checkInternet = await InternetHelper.instance.checkInternet(context);
      if (checkInternet) {
        emit(state.copyWith(status: SignUpStatus.submitting));
        await Future.delayed(const Duration(milliseconds: 1000));
        checkSignUp =
            await FireStoreHelper().signUpWithEmailPassword(email, password);
        DialogWidget.instance.dismiss(context: context);
        if (checkSignUp) {
          emit(state.copyWith(status: SignUpStatus.success));
        } else {
          emit(state.copyWith(status: SignUpStatus.error));
        }
      }
    }
  }
}
