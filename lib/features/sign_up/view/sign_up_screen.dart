import 'package:flutter/material.dart';
import 'package:message_za_lo/commons/widgets/widgets.dart';
import 'package:message_za_lo/features/sign_up/view/component/sign_up_body.dart';
import 'package:flutter_gen/gen_l10n/app_localizations.dart';

class SignUpScreen extends StatelessWidget {
  const SignUpScreen({Key? key}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return  Scaffold(
        appBar: AppBarWidget(
          title: AppLocalizations.of(context).create_account,
        ),
        body: SignUpBody());
  }
}
