import 'package:flutter/material.dart';
import 'package:message_za_lo/commons/constants/numeral.dart';
import 'package:message_za_lo/commons/constants/theme.dart';

class SignUpFooter extends StatelessWidget {
  const SignUpFooter({Key? key}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return Column(
      children: [
        Column(
          crossAxisAlignment: CrossAxisAlignment.center,
          children:  [
            const Text("Tiếp tục nghĩa là bạn đồng ý",
                style: DefaultTheme.TEXT_STYLE_SMALL_BLACK_COLOR),
            const SizedBox(height: 3,),
            Row(
              mainAxisAlignment: MainAxisAlignment.center,
              children: const [
                Text("với các ",
                    style: DefaultTheme.TEXT_STYLE_SMALL_BLACK_COLOR),
                Text("điều khoản",
                    style: TextStyle(
                      color: DefaultTheme.THEME_COLOR,
                      fontSize: Numeral.TEXT_SUB_SMALL,
                      fontWeight: FontWeight.normal,
                      decoration: TextDecoration.underline
                    )),
                Text(" sử dụng Zalo",
                    style: DefaultTheme.TEXT_STYLE_SMALL_BLACK_COLOR),
              ],
            )

          ],
        ),
      ],
    );
  }
}
