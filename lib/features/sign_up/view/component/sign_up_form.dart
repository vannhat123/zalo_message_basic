import 'package:flutter/material.dart';
import 'package:flutter_bloc/flutter_bloc.dart';
import 'package:get/get.dart';
import 'package:get/get_state_manager/src/simple/get_state.dart';
import 'package:message_za_lo/commons/constants/numeral.dart';
import 'package:message_za_lo/commons/constants/theme.dart';
import 'package:message_za_lo/commons/routes/Routes.dart';
import 'package:message_za_lo/commons/widgets/text_field_widget.dart';
import 'package:message_za_lo/commons/widgets/widgets.dart';
import 'package:flutter_gen/gen_l10n/app_localizations.dart';
import 'package:message_za_lo/features/sign_up/controller/sign_up_controller.dart';
import 'package:message_za_lo/features/sign_up/cubit/sign_up_cubit.dart';
import 'package:message_za_lo/services/firebase/firestore_helper.dart';

class SignUpForm extends StatelessWidget {
  const SignUpForm({Key? key}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    Size size = MediaQuery.of(context).size;
    return Scaffold(
        appBar: AppBarWidget(
          title: AppLocalizations.of(context).sign_up,
        ),
        body: _signUpFormWidget(context, size));
  }

  Widget _signUpFormWidget(BuildContext context, Size size) {
    return Container(
        child: GetBuilder<SignUpController>(
            init: SignUpController(),
            builder: (controller) {
              return BlocListener<SignUpCubit, SignUpState>(
                listener: (contextBloc, state) {
                  if (state.status == SignUpStatus.error) {
                    DialogWidget.instance.openErrorDialog(
                        context: context,
                        title: "Lỗi",
                        description: "Email đã được đăng đăng ký");
                    controller.setCheckErrorPassword(true);
                  }
                  if (state.status == SignUpStatus.success) {
                    Navigator.of(contextBloc).pushNamedAndRemoveUntil(
                        Routes.TAB_BAR, (route) => true);
                    controller.setCheckErrorPassword(false);
                    Get.put(SignUpController()).setRegisterFirstProfileToFirebase();
                  }
                  if (state.status == SignUpStatus.submitting) {
                    DialogWidget.instance
                        .openLoadingDialog(context: contextBloc);
                  }
                },
                child: Column(
                  children: [
                    Container(
                        margin: const EdgeInsets.only(
                            bottom: Numeral.MARGIN_APP_BAR),
                        color: DefaultTheme.GREY_BUTTON,
                        padding: const EdgeInsets.only(
                            bottom: 7,
                            top: 7,
                            left: Numeral.PADDING_LEFT_RIGHT_PRIMARY),
                        alignment: Alignment.centerLeft,
                        child: const Text(
                          "Bạn có thể đăng nhập bằng số điện thoại",
                          style: DefaultTheme.TEXT_STYLE_SUB_BLACK_NORMAL,
                        )),
                    Padding(
                      padding: const EdgeInsets.symmetric(
                          horizontal: Numeral.PADDING_LEFT_RIGHT_PRIMARY),
                      child: Column(
                        children: [
                          TextFieldWidget(
                            maxLength: Numeral.MAX_LENGTH_NUMBER_PHONE,
                            onChanged: (value) {
                              controller.setCheckErrorPassword(false);
                              if (value.isNotEmpty) {
                                controller.setValueCheckClearIconNumber(true);
                                controller.setCheckColorSignButton(true);
                              } else {
                                controller.setCheckColorSignButton(false);
                                controller.setValueCheckClearIconNumber(false);
                              }
                            },
                            suffixIcon: (controller.checkClearIconNumber)
                                ? InkWell(
                                    onTap: () {
                                      controller.setCheckErrorPassword(false);
                                      controller.emailController.text = '';
                                      controller
                                          .setValueCheckClearIconNumber(false);
                                      controller.setCheckColorSignButton(false);
                                    },
                                    child: const Icon(
                                      Icons.clear,
                                      size: 17,
                                    ),
                                  )
                                : const SizedBox(),
                            textInputType: TextInputType.text,
                            textEditingController: controller.emailController,
                            hintText: 'email',
                          ),
                          const SizedBox(
                            height: 20,
                          ),
                          TextFieldWidget(
                            maxLength: Numeral.MAX_LENGTH_PASSWORD,
                            onChanged: (value) {
                              controller.setCheckErrorPassword(false);
                              if (value.isNotEmpty) {
                                controller.setCheckClearIconPassword(true);
                                controller.setCheckColorSignButton(true);
                              } else {
                                controller.setCheckColorSignButton(false);
                                controller.setCheckClearIconPassword(false);
                              }
                            },
                            textInputType: TextInputType.text,
                            obscureText: controller.checkObscureText,
                            textEditingController:
                                controller.passwordController,
                            hintText: AppLocalizations.of(context).password,
                            suffixIcon: Container(
                              width: 85,
                              alignment: Alignment.centerRight,
                              child: Row(
                                children: [
                                  SizedBox(
                                    width: 20,
                                    child: (controller.checkClearIconPassword)
                                        ? InkWell(
                                            onTap: () {
                                              controller
                                                  .setCheckErrorPassword(false);
                                              controller
                                                  .passwordController.text = '';
                                              controller
                                                  .setCheckClearIconPassword(
                                                      false);
                                              controller
                                                  .setCheckColorSignButton(
                                                      false);
                                            },
                                            child: const Icon(
                                              Icons.clear,
                                              size: 17,
                                            ),
                                          )
                                        : const SizedBox(),
                                  ),
                                  const Spacer(),
                                  InkWell(
                                      onTap: () {
                                        controller
                                            .setValueCheckObscureText(null);
                                      },
                                      child: Container(
                                          margin: const EdgeInsets.only(
                                              right: Numeral
                                                  .PADDING_LEFT_RIGHT_PRIMARY),
                                          alignment: Alignment.centerRight,
                                          child: (controller.checkObscureText)
                                              ? const Text(
                                                  'HIỆN',
                                                  style: DefaultTheme
                                                      .TEXT_STYLE_HINT_FIELD,
                                                )
                                              : const Text(
                                                  'ẨN',
                                                  style: DefaultTheme
                                                      .TEXT_STYLE_HINT_FIELD,
                                                ))),
                                ],
                              ),
                            ),
                          ),
                          (controller.checkErrorPassword)
                              ? Container(
                                  margin: const EdgeInsets.only(top: 10),
                                  alignment: Alignment.centerLeft,
                                  child: const Text(
                                    "Có lỗi xảy ra",
                                    style:
                                        DefaultTheme.TEXT_STYLE_ERROR_PASSWORD,
                                    overflow: TextOverflow.ellipsis,
                                  ))
                              : const SizedBox(
                                  height: 5,
                                ),
                          InkWell(
                            onTap: () {
                              FocusScope.of(context).unfocus();
                            },
                            child: Container(
                              width: 1,
                                margin: const EdgeInsets.symmetric(
                                    vertical:
                                        Numeral.PADDING_LEFT_RIGHT_PRIMARY),
                                alignment: Alignment.centerLeft,
                                child: const Text(
                                  "",
                                  style: DefaultTheme
                                      .TEXT_STYLE_NORMAL_THEME_COLOR,
                                )),
                          ),
                          const SizedBox(
                            height: 20,
                          ),
                          (controller.checkColorSignInButton)
                              ? ButtonWidget(
                                  width: size.width * 0.5,
                                  height: size.width *
                                      Numeral.SIZE_HEIGHT_BUTTON_ONE,
                                  text: "Đăng ký",
                                  textStyle: DefaultTheme
                                      .TEXT_STYLE_NORMAL_WHITE_COLOR,
                                  buttonColor: DefaultTheme.THEME_COLOR,
                                  onTap: () {
                                    FocusScope.of(context)
                                        .requestFocus(FocusNode());
                                    context.read<SignUpCubit>().registerEmailPassword(controller.emailController.text, controller.passwordController.text, context);
                                  })
                              : ButtonWidget(
                                  width: size.width * 0.5,
                                  height: size.width *
                                      Numeral.SIZE_HEIGHT_BUTTON_ONE,
                                  text: "Đăng ký",
                                  textStyle:
                                      DefaultTheme.TEXT_STYLE_BUTTON_HINT,
                                  buttonColor: DefaultTheme
                                      .BACKGROUND_BUTTON_WIDGET_HIDE,
                                  onTap: () {}),
                        ],
                      ),
                    ),
                    const Spacer(),
                    InkWell(
                        onTap: () {
                          FocusScope.of(context).unfocus();
                        },
                        child: _signUpFormFooterWidget())
                  ],
                ),
              );
            }));
  }

  Widget _signUpFormFooterWidget() {
    return Container(
      margin: const EdgeInsets.only(bottom: Numeral.MARGIN_BOTTOM_FOOTER),
      child: const Text(
        "Các câu hỏi thường gặp",
        style: TextStyle(
            decoration: TextDecoration.underline,
            fontSize: Numeral.TEXT_SUB_NORMAL,
            color: DefaultTheme.GREY_TEXT_BUTTON),
      ),
    );
  }
}
