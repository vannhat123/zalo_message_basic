import 'package:flutter/material.dart';
import 'package:get/get.dart';
import 'package:get/get_state_manager/src/simple/get_state.dart';
import 'package:message_za_lo/commons/constants/numeral.dart';
import 'package:message_za_lo/commons/constants/theme.dart';
import 'package:message_za_lo/commons/routes/Routes.dart';
import 'package:message_za_lo/commons/widgets/text_field_widget.dart';
import 'package:message_za_lo/commons/widgets/widgets.dart';
import 'package:message_za_lo/features/sign_up/controller/sign_up_controller.dart';
import 'package:message_za_lo/features/sign_up/view/component/sign_up_footer.dart';

class SignUpBody extends StatelessWidget {
  const SignUpBody({Key? key}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    Size size = MediaQuery.of(context).size;
    return GetBuilder<SignUpController>(
        init: SignUpController(),
        builder: (controller) {
          return Column(
            children: [
              Container(
                  margin: const EdgeInsets.only(bottom: Numeral.MARGIN_APP_BAR),
                  color: DefaultTheme.GREY_BUTTON,
                  padding: const EdgeInsets.only(
                      bottom: 7,
                      top: 7,
                      left: Numeral.PADDING_LEFT_RIGHT_PRIMARY),
                  alignment: Alignment.centerLeft,
                  child: const Text(
                    "Sử dụng tên thật giúp bạn bè dễ dàng nhận ra bạn",
                    style: DefaultTheme.TEXT_STYLE_SUB_BLACK_NORMAL,
                  )),
              Padding(
                padding: const EdgeInsets.symmetric(
                    horizontal: Numeral.PADDING_LEFT_RIGHT_PRIMARY),
                child: TextFieldWidget(
                  maxLength: 25,
                  onChanged: (value) {
                    if (value.isNotEmpty) {
                      controller.setCheckIconClearName(true);
                      controller.setCheckColorSignInButtonName(true);
                    } else {
                      controller.setCheckIconClearName(false);
                      controller.setCheckColorSignInButtonName(false);
                    }
                  },
                  suffixIcon: (controller.checkColorSubmitButtonName)
                      ? InkWell(
                          onTap: () {
                            controller.nameController.text = '';
                            controller.setCheckColorSignInButtonName(false);
                          },
                          child: const Icon(
                            Icons.clear,
                            size: 17,
                          ),
                        )
                      : const SizedBox(),
                  textInputType: TextInputType.text,
                  textEditingController: controller.nameController,
                  hintText: 'Tên đầy đủ',
                ),
              ),
              const SizedBox(
                height: 35,
              ),
              (controller.checkColorSubmitButtonName)
                  ? ButtonWidget(
                      width: size.width * 0.5,
                      height: size.width * Numeral.SIZE_HEIGHT_BUTTON_ONE,
                      text: "Tiếp tục",
                      textStyle: DefaultTheme.TEXT_STYLE_NORMAL_WHITE_COLOR,
                      buttonColor: DefaultTheme.THEME_COLOR,
                      onTap: () async {
                        FocusScope.of(context).requestFocus(FocusNode());
                        Navigator.of(context).pushNamedAndRemoveUntil(
                            Routes.SIGN_UP_NUMBER, (route) => true);
                      })
                  : ButtonWidget(
                      width: size.width * 0.5,
                      height: size.width * Numeral.SIZE_HEIGHT_BUTTON_ONE,
                      text: "Tiếp tục",
                      textStyle: DefaultTheme.TEXT_STYLE_BUTTON_HINT,
                      buttonColor: DefaultTheme.BACKGROUND_BUTTON_WIDGET_HIDE,
                      onTap: () {}),
              const Spacer(),
              const SignUpFooter(),
              const SizedBox(
                height: Numeral.MARGIN_BOTTOM_FOOTER,
              ),
            ],
          );
        });
  }
}
