import 'package:flutter/material.dart';
import 'package:get/get.dart';
import 'package:message_za_lo/commons/constants/numeral.dart';
import 'package:message_za_lo/commons/constants/theme.dart';
import 'package:message_za_lo/commons/routes/Routes.dart';
import 'package:message_za_lo/commons/widgets/text_field_widget.dart';
import 'package:message_za_lo/commons/widgets/widgets.dart';
import 'package:message_za_lo/features/sign_up/controller/sign_up_controller.dart';
import 'package:message_za_lo/features/sign_up/view/component/sign_up_footer.dart';
import 'package:flutter_gen/gen_l10n/app_localizations.dart';

class SignUpNumber extends StatelessWidget {
  SignUpNumber({Key? key}) : super(key: key);
  final List<String> _listItem = ['VN', 'EU', 'CN', 'AE', 'PT'];

  @override
  Widget build(BuildContext context) {
    Size size = MediaQuery.of(context).size;
    return Scaffold(
      appBar: AppBarWidget(
        title: AppLocalizations.of(context).create_account,
      ),
      body: GetBuilder<SignUpController>(
          init: SignUpController(),
          builder: (controller) {
            return Column(
              children: [
                Container(
                    color: DefaultTheme.GREY_BUTTON,
                    padding: const EdgeInsets.only(
                        bottom: 7,
                        top: 7,
                        left: Numeral.PADDING_LEFT_RIGHT_PRIMARY),
                    alignment: Alignment.centerLeft,
                    child: const Text(
                      "Nhập số điện thoại của bạn",
                      style: DefaultTheme.TEXT_STYLE_SUB_BLACK_NORMAL,
                    )),
                _selectCountryInputNumberPhoneWidget(context, size, controller),
                const SizedBox(
                  height: 35,
                ),
                (controller.checkColorSubmitButtonPhone)
                    ? ButtonWidget(
                        width: size.width * 0.5,
                        height: size.width * Numeral.SIZE_HEIGHT_BUTTON_ONE,
                        text: "Tiếp tục",
                        textStyle: DefaultTheme.TEXT_STYLE_NORMAL_WHITE_COLOR,
                        buttonColor: DefaultTheme.THEME_COLOR,
                        onTap: () {
                          Get.put(SignUpController()).getAccount();
                          FocusScope.of(context).requestFocus(FocusNode());
                          Navigator.of(context).pushNamedAndRemoveUntil(
                              Routes.SIGN_UP_FORM, (route) => true);
                        })
                    : ButtonWidget(
                        width: size.width * 0.5,
                        height: size.width * Numeral.SIZE_HEIGHT_BUTTON_ONE,
                        text: "Tiếp tục",
                        textStyle: DefaultTheme.TEXT_STYLE_BUTTON_HINT,
                        buttonColor: DefaultTheme.BACKGROUND_BUTTON_WIDGET_HIDE,
                        onTap: () {}),
                const Spacer(),
                const SignUpFooter(),
                const SizedBox(
                  height: 30,
                ),
              ],
            );
          }),
    );
  }

  Widget _selectCountryInputNumberPhoneWidget(
      BuildContext context, Size size, SignUpController controller) {
    return Container(
      margin: const EdgeInsets.only(top: Numeral.MARGIN_APP_BAR),
      padding: const EdgeInsets.only(left: 14),
      height: 40,
      child: Row(
        children: [
          Container(
            alignment: Alignment.bottomCenter,
            decoration: const BoxDecoration(
                border: Border(
              bottom: BorderSide(width: 0.5, color: DefaultTheme.BLACK),
            )),
            height: 40,
            child: DropdownButton<String>(
              underline: DropdownButtonHideUnderline(child: Container()),
              value: _listItem[0],
              icon: const Icon(
                Icons.arrow_drop_down_sharp,
                size: 25,
              ),
              elevation: 20,
              style: const TextStyle(color: DefaultTheme.BLACK),
              onChanged: (String? value) {},
              items: _listItem.map<DropdownMenuItem<String>>((String value) {
                return DropdownMenuItem<String>(
                  value: value,
                  child: Text(value),
                );
              }).toList(),
            ),
          ),
          Expanded(
            child: TextFieldWidget(
              marginLeftRight: Numeral.PADDING_LEFT_RIGHT_PRIMARY,
              maxLength: Numeral.MAX_LENGTH_NUMBER_PHONE,
              onChanged: (value) {
                if (value.isNotEmpty) {
                  controller.setCheckIconClearName(true);
                  controller.setCheckColorSignInButtonPhone(true);
                } else {
                  controller.setCheckIconClearName(false);
                  controller.setCheckColorSignInButtonPhone(false);
                }
              },
              suffixIcon: (controller.checkColorSubmitButtonPhone)
                  ? InkWell(
                      onTap: () {
                        controller.phoneNumberController.text = '';
                        controller.setCheckColorSignInButtonPhone(false);
                      },
                      child: const Icon(
                        Icons.clear,
                        size: 17,
                      ),
                    )
                  : const SizedBox(),
              textInputType: TextInputType.number,
              textEditingController: controller.phoneNumberController,
              hintText: 'Số điện thoại',
            ),
          ),
        ],
      ),
    );
  }
}
