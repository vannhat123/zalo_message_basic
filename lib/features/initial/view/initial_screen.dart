import 'package:flutter/material.dart';
import 'package:message_za_lo/features/initial/view/component/initial_body.dart';

class InitialScreen extends StatefulWidget {
  const InitialScreen({Key? key}) : super(key: key);

  @override
  State<InitialScreen> createState() => _InitialScreenState();
}

class _InitialScreenState extends State<InitialScreen> {
  @override
  Widget build(BuildContext context) {
    return const Scaffold(
      body:InitialBody(),
    );
  }

}
