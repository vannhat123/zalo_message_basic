import 'package:carousel_slider/carousel_slider.dart';
import 'package:flutter/material.dart';
import 'package:message_za_lo/features/login/repository/list_slider.dart';
class LoginSlider extends StatelessWidget {
   const LoginSlider({Key? key}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    int _current = 0;
    final CarouselController _controller = CarouselController();
    Size size = MediaQuery.of(context).size;
    final List<Widget> imageSliders = ListSlider().imgList.map((item) {
      return Container(
        margin: const EdgeInsets.all(5.0),
        child: ClipRRect(
            borderRadius: const BorderRadius.all(Radius.circular(5.0)),
            child: Stack(
              children: [
                Image.asset(
                  item,
                  fit: BoxFit.cover,
                  width: size.width*0.8,
                ),
                Positioned(
                  bottom: 0.0,
                  left: 0.0,
                  right: 0.0,
                  child: Container(
                    decoration:  BoxDecoration(
                      gradient: LinearGradient(
                        colors: [
                          Color.fromARGB(200, 0, 0, 0),
                          Color.fromARGB(0, 0, 0, 0)
                        ],
                        begin: Alignment.bottomCenter,
                        end: Alignment.topCenter,
                      ),
                    ),
                    padding: const EdgeInsets.symmetric(
                        vertical: 1.0, horizontal: 20.0),
                  ),
                ),
              ],
            )),
      );
    }).toList();
    return Column(
      children: [
        Container(
          height: size.height * 0.25,
          alignment: Alignment.center,
          child: CarouselSlider(
            disableGesture:true,
            items: imageSliders,
            carouselController: _controller,
            options: CarouselOptions(
                viewportFraction: 1.0,
                enlargeCenterPage: false,
                autoPlay: true,
                aspectRatio: 16 / 12,
                onPageChanged: (index, reason) {
                  // setState(() {
                  //   _current = index;
                  // });
                }),
          ),
        ),
        Row(
          mainAxisAlignment: MainAxisAlignment.center,
          children: ListSlider().imgList.asMap().entries.map((entry) {
            return GestureDetector(
              onTap: () => _controller.animateToPage(entry.key),
              child: Container(
                width: 12.0,
                height: 12.0,
                margin: const EdgeInsets.symmetric(
                    vertical: 8.0, horizontal: 4.0),
                decoration: BoxDecoration(
                    shape: BoxShape.circle,
                    color: (Theme.of(context).brightness == Brightness.dark
                        ? Colors.white
                        : Colors.black)
                        .withOpacity(_current == entry.key ? 0.9 : 0.4)),
              ),
            );
          }).toList(),
        ),
      ],
    );
  }
}
