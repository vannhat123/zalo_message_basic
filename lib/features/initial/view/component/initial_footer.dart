import 'package:flutter/material.dart';
import 'package:message_za_lo/commons/constants/theme.dart';
class InitialFooter extends StatelessWidget {
  const InitialFooter({Key? key}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return Row(
      mainAxisAlignment: MainAxisAlignment.center,
      children:  [
        Container(
          height: 50,
          alignment: Alignment.center,
          decoration: BoxDecoration(
            border: Border(
              bottom: BorderSide(width: 1, color: DefaultTheme.BLACK),
            ),
          ),
          child: Text(
            "Tiếng Việt",
            style: DefaultTheme.TEXT_SUB_BUTTON_BLACK,
          ),
        ),
        SizedBox(
          width: 20,
        ),
        Container(
          decoration: BoxDecoration(
            border: Border(
              bottom: BorderSide(width: 1, color: DefaultTheme.BLACK.withOpacity(0)),
            ),
          ),
          alignment: Alignment.center,
          height: 50,
          child: Text(
            "English",
            style: DefaultTheme.TEXT_SUB_BUTTON_GREY,
          ),
        ),
      ],
    );
  }
}
