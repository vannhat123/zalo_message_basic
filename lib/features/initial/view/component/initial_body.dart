import 'package:flutter/material.dart';
import 'package:message_za_lo/commons/constants/numeral.dart';
import 'package:message_za_lo/commons/constants/theme.dart';
import 'package:message_za_lo/commons/routes/Routes.dart';
import 'package:message_za_lo/commons/widgets/widgets.dart';
import 'package:message_za_lo/features/initial/view/component/initial_footer.dart';
import 'package:message_za_lo/features/initial/view/component/login_slider.dart';
import 'package:flutter_gen/gen_l10n/app_localizations.dart';

class InitialBody extends StatelessWidget {
  const InitialBody({Key? key}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    Size size = MediaQuery.of(context).size;
    return Padding(
      padding: EdgeInsets.only(
          top: size.width * 0.13, bottom: Numeral.MARGIN_BOTTOM_FOOTER),
      child: Column(
        mainAxisAlignment: MainAxisAlignment.center,
        children: [
          Image.asset(
            "assets/slider/Logo-Zalo.webp",
            width: size.width * 0.22,
            fit: BoxFit.cover,
          ),
          SizedBox(
            height: size.width * 0.1,
          ),
          const LoginSlider(),
          Column(
            children: [
              Text(
                AppLocalizations.of(context).chat_convenient,
                style: DefaultTheme.TEXT_SUB_SLOGAN_BOLD,
              ),
              const SizedBox(
                height: 10,
              ),
              Padding(
                padding: EdgeInsets.symmetric(horizontal: size.width * 0.1),
                child: const Text(
                  "Cùng trao đổi, giữ liên lạc với gia đình, bạn bè và đồng nghiệp mọi lúc mọi nơi",
                  style: DefaultTheme.TEXT_SUB_SLOGAN_NORMAL,
                  textAlign: TextAlign.center,
                ),
              )
            ],
          ),
          const Spacer(),
          Expanded(child: _buttonLoginSignUpWidget(context)),
          const InitialFooter(),
        ],
      ),
    );
  }

  Widget _buttonLoginSignUpWidget(BuildContext context) {
    return Column(
      mainAxisAlignment: MainAxisAlignment.spaceBetween,
      children: [
        ButtonWidget(
            text: "Đăng nhập",
            textStyle: DefaultTheme.TEXT_SUB_BUTTON_WHITE,
            buttonColor: DefaultTheme.THEME_COLOR,
            onTap: () {
              Navigator.of(context)
                  .pushNamedAndRemoveUntil(Routes.LOGIN_FORM, (route) => true);
            }),
        ButtonWidget(
            text: "Đăng ký",
            textStyle: DefaultTheme.TEXT_SUB_BUTTON_BLACK,
            buttonColor:
                DefaultTheme.GREY.withOpacity(Numeral.OPACITY_COLOR_GREY),
            onTap: () {
              Navigator.of(context)
                  .pushNamedAndRemoveUntil(Routes.SIGN_UP, (route) => true);
            }),
      ],
    );
  }
}
