import 'package:firebase_core/firebase_core.dart';
import 'package:flutter/material.dart';
import 'package:flutter_bloc/flutter_bloc.dart';
import 'package:flutter_localizations/flutter_localizations.dart';
import 'package:get/get.dart';
import 'package:message_za_lo/bloc_observer.dart';
import 'package:message_za_lo/commons/constants/pages.dart';
import 'package:message_za_lo/commons/routes/Routes.dart';
import 'package:message_za_lo/features/account_detail/view/account_detail_screen.dart';
import 'package:message_za_lo/features/home/chat/view/chat_screen.dart';
import 'package:message_za_lo/features/home/tabbar/view/tabbar_screen.dart';
import 'package:message_za_lo/features/initial/view/initial_screen.dart';
import 'package:message_za_lo/features/login/view/login_screen.dart';
import 'package:message_za_lo/features/sign_up/cubit/sign_up_cubit.dart';
import 'package:message_za_lo/features/sign_up/view/component/sign_up_form.dart';
import 'package:message_za_lo/features/sign_up/view/component/sign_up_number.dart';
import 'package:message_za_lo/features/sign_up/view/sign_up_screen.dart';
import 'package:flutter_gen/gen_l10n/app_localizations.dart';
import 'package:message_za_lo/services/firebase/firestore_helper.dart';
import 'package:message_za_lo/services/shared_preferences/account_helper.dart';
import 'package:shared_preferences/shared_preferences.dart';

import 'features/login/cubit/login_cubit.dart';

late SharedPreferences sharedPrefs;
late Widget _startScreen;

Future<void> _getAuthorizeUser() async {
  String email = AccountHelper.instance.getEmailAccount();
  String password = AccountHelper.instance.getPasswordAccount();
  bool checkLogged;
  checkLogged = await FireStoreHelper().signInWithEmailPassword(email, password);
  if (checkLogged) {
    _startScreen = TabBarScreen();
  } else {
    _startScreen = const InitialScreen();
  }
}

Future<void> main() async {
  WidgetsFlutterBinding.ensureInitialized();
  await Firebase.initializeApp();
  sharedPrefs = await SharedPreferences.getInstance();
  Bloc.observer = AppBlocObserver();
  await _getAuthorizeUser();
  runApp(const MyApp());
}

class MyApp extends StatelessWidget {
  const MyApp({Key? key}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return MultiBlocProvider(
        providers: [
          BlocProvider<LoginCubit>(create: (context) => LoginCubit()),
          BlocProvider<SignUpCubit>(create: (context) => SignUpCubit()),
        ],
        child: GetMaterialApp(
          debugShowCheckedModeBanner: false,
          theme: ThemeData(
            primarySwatch: Colors.blue,
          ),
          localizationsDelegates: const [
            AppLocalizations.delegate,
            GlobalMaterialLocalizations.delegate,
            GlobalWidgetsLocalizations.delegate,
            GlobalCupertinoLocalizations.delegate,
          ],
          locale: const Locale('ja', ''),
          supportedLocales: const [
            Locale('en', ''),
            Locale('ja', ''),
          ],
          getPages: Pages.pages,
          routes: {
            Routes.INITIAL: (context) => const InitialScreen(),
            Routes.LOGIN_FORM: (context) => const LoginScreen(),
            Routes.SIGN_UP: (context) => const SignUpScreen(),
            Routes.SIGN_UP_NUMBER: (context) => SignUpNumber(),
            Routes.TAB_BAR: (context) => TabBarScreen(),
            Routes.SIGN_UP_FORM: (context) => SignUpForm(),
            Routes.CHAT: (context) => ChatScreen(),
            Routes.ACCOUNT_DETAIL: (context) => AccountDetailScreen(),
          },
          home: _startScreen,
        ));
  }
}
