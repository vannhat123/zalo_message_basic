import 'package:cloud_firestore/cloud_firestore.dart';
import 'package:equatable/equatable.dart';

class ProfileDTO extends Equatable {
  final String dateBirth;
  final String fullName;
  final int gender;
  final String avatarUrl;
  final String phoneNumber;
  final String email;
  final int idDoc;
  final String backgroundUrl;

  const ProfileDTO(
      {required this.dateBirth,
      required this.fullName,
      required this.gender,
      required this.avatarUrl,
      required this.phoneNumber,
      required this.email,
      required this.idDoc,
      required this.backgroundUrl});

  factory ProfileDTO.fromJson(Map<String, dynamic> json) {
    return ProfileDTO(
      dateBirth: json['date_birth'] ?? '',
      fullName: json['full_name'] ?? '',
      gender: json['gender'] ?? 0,
      avatarUrl: json['avatar_url'] ?? '',
      phoneNumber: json['phone_number'] ?? '',
      email: json['email'] ?? '',
      idDoc: json['id_doc'] ?? 0,
      backgroundUrl: json['background_url'] ?? '',
    );
  }

  Map<String, dynamic> toJson() {
    final Map<String, dynamic> data = <String, dynamic>{};
    data['date_birth'] = dateBirth;
    data['full_name'] = fullName;
    data['gender'] = gender;
    data['avatar_url'] = avatarUrl;
    data['phone_number'] = phoneNumber;
    data['email'] = email;
    data['background_url'] = backgroundUrl;
    return data;
  }

  ProfileDTO.fromObjectSnapshot(DocumentSnapshot<Object?> doc)
      : dateBirth = doc['date_birth'],
        fullName = doc['full_name'],
        gender = doc['gender'],
        avatarUrl = doc['avatar_url'],
        phoneNumber = doc['phone_number'],
        email = doc['email'],
        idDoc = doc['id_doc'],
        backgroundUrl = doc['background_url'];

  @override
  List<Object> get props =>
      [dateBirth, fullName, gender, avatarUrl, phoneNumber, backgroundUrl];
}
