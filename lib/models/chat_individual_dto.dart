import 'package:cloud_firestore/cloud_firestore.dart';
import 'package:equatable/equatable.dart';

class ChatIndividualDTO extends Equatable {
  final String date;
  final String emailSend;
  final String hourFormat;
  final String messageContent;
  final int sendingTime;

  const ChatIndividualDTO(
      {required this.date,
      required this.hourFormat,
      required this.sendingTime,
      required this.emailSend,
      required this.messageContent});

  factory ChatIndividualDTO.fromJson(Map<String, dynamic> json) {
    return ChatIndividualDTO(
        date: json['date'] ?? '',
        hourFormat: json['hour_format'] ?? '',
        sendingTime: json['sending_time'] ?? 0,
        emailSend: json['email_send'] ?? '',
        messageContent: json['message_content'] ?? '');
  }

  Map<String, dynamic> toJson() {
    final Map<String, dynamic> data = <String, dynamic>{};
    data['date'] = date;
    data['hourFormat'] = hourFormat;
    data['sending_time'] = sendingTime;
    data['email_send'] = emailSend;
    data['message_content'] = messageContent;
    return data;
  }

  ChatIndividualDTO.fromObjectSnapshot(DocumentSnapshot<Object?>  doc)
      : date = doc['date'],
        hourFormat = doc['hour_format'],
        sendingTime = doc['sending_time'],
        emailSend = doc['emailSend'],
        messageContent = doc['message_content'];

  @override
  List<Object> get props =>
      [date, hourFormat, sendingTime, emailSend, messageContent];
}
