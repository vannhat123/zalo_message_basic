import 'package:cloud_firestore/cloud_firestore.dart';
import 'package:equatable/equatable.dart';

class DiaryPostsDTO extends Equatable {
  final String dateFormat;
  final String dateFull;
  final String hourFormat;
  final List<PostsComment> postsComment;
  final List<PostsLike> postsLike;
  final List<PostsImage> postsImageUrl;
  final String postsContent;
  final int sendingTime;

  const DiaryPostsDTO(
      {required this.dateFormat,
      required this.dateFull,
      required this.hourFormat,
      required this.postsComment,
      required this.postsLike,
      required this.postsImageUrl,
      required this.postsContent,
      required this.sendingTime});

  DiaryPostsDTO.fromObjectSnapshot(DocumentSnapshot<Object?> doc)
      : dateFormat = doc['date_format'],
        dateFull = doc['date_full'],
        hourFormat = doc['hour_format'],
        postsComment = doc["posts_comment"] != null
            ? List<PostsComment>.from(
                doc["posts_comment"].map((x) => PostsComment.fromJson(x)))
            : [],
        postsLike = doc['posts_like'] != null
            ? List<PostsLike>.from(
                doc["posts_like"].map((x) => PostsLike.fromJson(x)))
            : [],
        postsImageUrl = doc['posts_image_url'] != null
            ? List<PostsImage>.from(
                doc["posts_image_url"].map((x) => PostsImage.fromJson(x)))
            : [],
        sendingTime = doc['sending_time'],
        postsContent = doc['posts_content'];

  Map<String, dynamic> toJson() {
    final Map<String, dynamic> data = <String, dynamic>{};
    data['date_format'] = dateFormat;
    data['date_full'] = dateFull;
    data['hour_format'] = hourFormat;
    data['posts_comment'] = postsComment;
    data['posts_like'] = postsLike.map((v) => v.toJson()).toList();
    data['posts_image_url'] = postsImageUrl.map((v) => v.toJson()).toList();
    ;
    data['posts_content'] = postsContent;
    data['sending_time'] = sendingTime;
    return data;
  }

  @override
  List<Object> get props =>
      [dateFormat, dateFull, hourFormat, postsComment, postsLike];
}

class PostsComment {
  final String commentContent;
  final String commentDate;
  final String commentHour;
  final String commentImageUrl;
  final String commentUserName;
  final String commentUserEmail;

  const PostsComment({
    required this.commentContent,
    required this.commentDate,
    required this.commentHour,
    required this.commentImageUrl,
    required this.commentUserName,
    required this.commentUserEmail,
  });

  factory PostsComment.fromJson(Map<String, dynamic> json) {
    return PostsComment(
      commentContent: json['comment_content'] ?? '',
      commentDate: json['comment_date'] ?? '',
      commentHour: json['comment_hour'] ?? '',
      commentImageUrl: json['comment_image_url'] ?? '',
      commentUserName: json['comment_user_name'] ?? '',
      commentUserEmail: json['comment_user_email'] ?? '',
    );
  }

  Map<String, dynamic> toJson() {
    final Map<String, dynamic> data = <String, dynamic>{};
    data['comment_content'] = commentContent;
    data['comment_date'] = commentDate;
    data['comment_hour'] = commentHour;
    data['comment_image_url'] = commentImageUrl;
    data['comment_user_name'] = commentUserName;
    data['comment_user_email'] = commentUserEmail;
    return data;
  }
}

class PostsLike {
  final String likeUserName;
  final String likeImageUrl;

  const PostsLike({required this.likeUserName, required this.likeImageUrl});

  factory PostsLike.fromJson(Map<String, dynamic> json) {
    return PostsLike(
      likeUserName: json['like_user_name'] ?? '',
      likeImageUrl: json['like_image_url'] ?? '',
    );
  }

  Map<String, dynamic> toJson() {
    final Map<String, dynamic> data = <String, dynamic>{};
    data['like_user_name'] = likeUserName;
    data['like_image_url'] = likeImageUrl;
    return data;
  }
}

class PostsImage {
  final String urlImage;
  final String urlFile;

  const PostsImage({required this.urlImage, required this.urlFile});

  factory PostsImage.fromJson(Map<String, dynamic> json) {
    return PostsImage(
      urlImage: json['url_image'] ?? '',
      urlFile: json['url_file'] ?? '',
    );
  }

  Map<String, dynamic> toJson() {
    final Map<String, dynamic> data = <String, dynamic>{};
    data['url_image'] = urlImage;
    data['url_file'] = urlFile;
    return data;
  }
}
