import 'package:message_za_lo/main.dart';

class AccountHelper {
  const AccountHelper._privateConstructor();

  //create instance
  static const AccountHelper _instance = AccountHelper._privateConstructor();

  static AccountHelper get instance => _instance;

  Future<void> setEmailPasswordAccount(String email, String password) async {
    await sharedPrefs.setString('EMAIL', email);
    await sharedPrefs.setString('PASSWORD', password);
  }

  String getPasswordAccount() {
    return sharedPrefs.getString('PASSWORD') ?? '';
  }

  String getEmailAccount() {
    return sharedPrefs.getString('EMAIL') ?? '';
  }

  void setAvatarUrl(String image) {
    sharedPrefs.setString('avatar_url', image);
  }

  void setBackgroundUrl(String image) {
    sharedPrefs.setString('background_url', image);
  }

  String getAvatarUrl() {
    return sharedPrefs.getString('avatar_url') ?? '';
  }

  String getBackgroundUrl() {
    return sharedPrefs.getString('background_url') ?? '';
  }
}
