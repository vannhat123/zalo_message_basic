import 'package:flutter/material.dart';
import 'package:message_za_lo/commons/widgets/widgets.dart';
import 'package:permission_handler/permission_handler.dart';

class PermissionHelper {
  const PermissionHelper._privateConstructor();

  static const PermissionHelper _instance =
      PermissionHelper._privateConstructor();

  static PermissionHelper get instance => _instance;

  Future<PermissionStatus> _requestWhenDeny(
      Permission permission, BuildContext context) async {
    PermissionStatus status = await permission.request();
    if (status == PermissionStatus.permanentlyDenied) {
      await _showDialogWhenDenied(context: context);
    }
    return status;
  }

  Future<void> requestPermission(
      {@required Function? onGranted,
      Permission permission = Permission.photos,
      required BuildContext context}) async {
    PermissionStatus status;
    status = await permission.status;
    switch (status) {
      case PermissionStatus.granted:
        break;
      case PermissionStatus.denied:
        status = await _requestWhenDeny(permission, context);
        break;
      case PermissionStatus.permanentlyDenied:
        await _showDialogWhenDenied(context: context);
        break;
      default:
        break;
    }
    if ((status.isGranted || status.isLimited) && onGranted != null) {
      await onGranted();
    }
  }

  Future<void> _showDialogWhenDenied({required BuildContext context}) async {
    return DialogWidget.instance.openConfirmDialog(
        context: context,
        title: "",
        description:"Dịch vụ chưa được cung cấp, vui lòng cấp quyền để truy cập",
        titleOK: "Mở cài đặt",
        function: () {
          openAppSettings();
        });
  }

}
