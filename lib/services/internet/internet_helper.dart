import 'dart:io';

import 'package:flutter/material.dart';
import 'package:flutter_gen/gen_l10n/app_localizations.dart';
import 'package:message_za_lo/commons/widgets/widgets.dart';

class InternetHelper {
  const InternetHelper._privateConstructor();

  static const InternetHelper _instance = InternetHelper._privateConstructor();

  static InternetHelper get instance => _instance;

  Future<bool> checkInternet(BuildContext context) async {
    try {
      final result = await InternetAddress.lookup('google.com');
      if (result.isNotEmpty && result[0].rawAddress.isNotEmpty) {
        return true;
      } else {
        return false;
      }
    } on SocketException catch (_) {
      DialogWidget.instance.openErrorDialog(
          context: context,
          title: AppLocalizations.of(context).error,
          description: AppLocalizations.of(context).error_net_work);
      return false;
    }
  }
}
