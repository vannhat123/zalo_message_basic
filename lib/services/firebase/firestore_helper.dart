import 'dart:io';
import 'package:cloud_firestore/cloud_firestore.dart';
import 'package:firebase_auth/firebase_auth.dart';
import 'package:firebase_storage/firebase_storage.dart';
import 'package:flutter/foundation.dart';
import 'package:flutter/material.dart';
import 'package:message_za_lo/commons/constants/numeral.dart';
import 'package:message_za_lo/models/chat_individual_dto.dart';
import 'package:message_za_lo/models/diary_posts_dto.dart';
import 'package:message_za_lo/models/profile_dto.dart';
import 'package:message_za_lo/services/shared_preferences/account_helper.dart';

class FireStoreHelper {
  static final FirebaseFirestore _fireStore = FirebaseFirestore.instance;
  static final FirebaseAuth _firebaseAuth = FirebaseAuth.instance;
  static final FirebaseStorage _firebaseStorage = FirebaseStorage.instance;
  String email = AccountHelper.instance.getEmailAccount();

  static FirebaseFirestore get fireStore => _fireStore;

  static FirebaseAuth get firebaseAuth => _firebaseAuth;

  Future<bool> signUpWithEmailPassword(String email, String password) async {
    try {
      await _firebaseAuth.createUserWithEmailAndPassword(
          email: email, password: password);
      await AccountHelper.instance.setEmailPasswordAccount(email, password);
      return true;
    } catch (e) {
      return false;
    }
  }

  Future<bool> signInWithEmailPassword(String email, String password) async {
    try {
      await _firebaseAuth.signInWithEmailAndPassword(
          email: email, password: password);
      await AccountHelper.instance.setEmailPasswordAccount(email, password);
      return true;
    } catch (e) {
      return false;
    }
  }

  Future<bool> logOut() async {
    try {
      await _firebaseAuth.signOut();
      return true;
    } catch (e) {
      return false;
    }
  }

  Stream<List<String>> getListSeenMessage(int shopId) {
    return _fireStore
        .collection('Message/Individual/Kalel')
        .doc(shopId.toString())
        .snapshots()
        .map(
      (value) {
        List<String> _listSeen = [];
        for (var element in value['seen']) {
          _listSeen.add(element);
        }
        return _listSeen.toList();
      },
    );
  }

  Query queryMessage(String email) {
    return _fireStore
        .collection("Message/Individual/$email")
        .orderBy('sending_time', descending: true);
  }

  Future<void> sendMessageToFirebase(
      ChatIndividualDTO chatIndividualDTO) async {
    CollectionReference collectionReference = _fireStore.collection(
        "Message/Individual/conmeo@gmail.com/concho@gmail.com/message_shared");
    await collectionReference.doc().set(chatIndividualDTO.toJson());
  }

  Stream<ProfileDTO> getProfileFirebase(String email) {
    return _fireStore.collection('Profile/1/$email').snapshots().map(
      (value) {
        ProfileDTO profileDTO = ProfileDTO(
            dateBirth: '',
            fullName: '',
            gender: 0,
            phoneNumber: '',
            email: '',
            idDoc: 0,
            avatarUrl: '',
            backgroundUrl: '');
        profileDTO = ProfileDTO.fromObjectSnapshot(value.docs[0]);
        return profileDTO;
      },
    );
  }

  Future<bool> updateProfileToFirebase(
      {required String fullName,
      required String dateBirth,
      required int gender,
      required String urlImage,
      required String urlBackground,
      required String email,
      required int type}) async {
    Map<String, dynamic> dateSendFirebase = {
      'full_name': fullName,
      'date_birth': dateBirth,
      'gender': gender
    };
    try {
      if (type == Numeral.TYPE_UPDATE_PROFILE_IMAGE_AVATAR) {
        await _fireStore
            .collection('Profile/1/$email')
            .doc('1')
            .update({'avatar_url': urlImage});
      } else if (type == Numeral.TYPE_UPDATE_PROFILE_IMAGE_BACKGROUND) {
        await _fireStore
            .collection('Profile/1/$email')
            .doc('1')
            .update({'background_url': urlBackground});
      } else {
        await _fireStore
            .collection('Profile/1/$email')
            .doc('1')
            .update(dateSendFirebase);
      }
      return true;
    } catch (e) {
      return false;
    }
  }

  Future<bool> setProfileToFirebase(
      {required String fullName,
      required String dateBirth,
      required int gender,
      required String urlImage,
      required String phoneNumber,
      required String email,
      required int countDoc,
      required String backgroundUrl}) async {
    Map<String, dynamic> dateSendFirebase = {
      'full_name': fullName,
      'date_birth': dateBirth,
      'gender': gender,
      'avatar_url': urlImage,
      'phone_number': phoneNumber,
      'email': email,
      'id_doc': countDoc,
      'background_url': backgroundUrl
    };
    try {
      await _fireStore
          .collection('Profile')
          .doc('1')
          .collection(email)
          .doc('1')
          .set(dateSendFirebase);
      return true;
    } catch (e) {
      return false;
    }
  }

  Stream<int> getCountAccount() {
    int count = 0;
    return _fireStore.collection('Profile').doc('count').snapshots().map(
      (value) {
        count = value['count_account'];
        return count;
      },
    );
  }

  Future<void> setCountAccount(int countAccount) async {
    await _fireStore
        .collection('Profile')
        .doc('count')
        .update({'count_account': countAccount});
  }

  //Image firebase storage

  Future<void> uploadFileToFirebaseStorage(
      File? file, BuildContext context, int typeImage) async {
    if (file == null) {
      ScaffoldMessenger.of(context).showSnackBar(
        const SnackBar(
          content: Text('No file was selected'),
        ),
      );
      return null;
    }
    UploadTask? uploadTask;
    List<String> convertFilePathToList = file.path.split('/');
    String urlImagePathClean = '';
    for (var item in convertFilePathToList) {
      if (item.contains('image')) {
        urlImagePathClean = item;
        break;
      }
    }
    Reference ref;
    if (typeImage == Numeral.TYPE_UPDATE_PROFILE_IMAGE_AVATAR) {
      ref = await _firebaseStorage
          .ref()
          .child('$email/avatar/$urlImagePathClean');
    } else {
      ref = await _firebaseStorage
          .ref()
          .child('$email/background/$urlImagePathClean');
    }

    if (kIsWeb) {
      //   uploadTask = ref.putData(await file.readAsBytes(), metadata);
    } else {
      uploadTask = ref.putFile(file);
    }
    final snapshot = await uploadTask!.whenComplete(() {});
    final url = await snapshot.ref.getDownloadURL();
    if (url.isNotEmpty) {
      if (typeImage == Numeral.TYPE_UPDATE_PROFILE_IMAGE_AVATAR) {
        AccountHelper.instance.setAvatarUrl(url);
        updateProfileToFirebase(
            fullName: '',
            dateBirth: '',
            gender: Numeral.VALUE_NONE_EXITS,
            urlImage: url,
            email: email,
            type: typeImage,
            urlBackground: '');
      } else if (typeImage == Numeral.TYPE_UPDATE_PROFILE_IMAGE_BACKGROUND) {
        AccountHelper.instance.setBackgroundUrl(url);
        updateProfileToFirebase(
            fullName: '',
            dateBirth: '',
            gender: Numeral.VALUE_NONE_EXITS,
            urlImage: '',
            email: email,
            type: typeImage,
            urlBackground: url);
      }
    } else {}
  }

  /// DIARY
  Future<void> commentPostsDiary(PostsComment postsComment,int sendingTime) async {
    CollectionReference collectionReference =
        _fireStore.collection("Diary/$email/posts-diary");
    await collectionReference.doc('$sendingTime').update({
      'posts_comment': FieldValue.arrayUnion([postsComment.toJson()])
    });
  }

  // like diary
  Future<void> likePostsDiary(PostsLike postsLike,int sendingTime) async {
    CollectionReference collectionReference =
    _fireStore.collection("Diary/$email/posts-diary");
    await collectionReference.doc('$sendingTime').update({
      'posts_like': FieldValue.arrayUnion([postsLike.toJson()])
    });
  }

  // remove like diary
  Future<void> removeLikePostsDiary(PostsLike postsLike,int sendingTime) async {
    CollectionReference collectionReference =
    _fireStore.collection("Diary/$email/posts-diary");
    await collectionReference.doc('$sendingTime').update({
      'posts_like': FieldValue.arrayRemove([postsLike.toJson()])
    });
  }

  // delete comment your diary
  Future<void> deleteCommentPostsDiary(PostsComment postsComment, int sendingTime)async{
    CollectionReference collectionReference =
    _fireStore.collection("Diary/$email/posts-diary");
    await collectionReference.doc('$sendingTime').update({
      'posts_comment': FieldValue.arrayRemove([postsComment.toJson()])
    });
  }

  Stream<List<DiaryPostsDTO>> getListPostsDiary() {
    return _fireStore
        .collection('Diary/$email/posts-diary')
      //  .snapshots().map(
        .orderBy('sending_time', descending: true).snapshots().map(
      (value) {
        List<DiaryPostsDTO> list = [];
        for (var item in value.docs) {
          if (item.data().isNotEmpty) {
            list.add(DiaryPostsDTO.fromObjectSnapshot(item));
          }
        }
        return list;
      },
    );
  }

  // post diary
  Future<void> postsYourDiary(DiaryPostsDTO diaryPostsDTO) async {
    CollectionReference collectionReference =
        _fireStore.collection("Diary/$email/posts-diary");
    await collectionReference
        .doc(diaryPostsDTO.sendingTime.toString())
        .set(diaryPostsDTO.toJson());
  }

  // update diary
  Future<void> updateYourDiary(DiaryPostsDTO diaryPostsDTO) async {
    Map<String,dynamic> postsImageUrl ;

    CollectionReference collectionReference =
    _fireStore.collection("Diary/$email/posts-diary");
    // await collectionReference
    //     .doc(diaryPostsDTO.sendingTime.toString())
    //     .update({'posts_content': diaryPostsDTO.postsContent, 'posts_image_url': diaryPostsDTO.postsImageUrl.toJson()});
  }

  // delete diary
  Future<void> deleteYourDiary(DiaryPostsDTO diaryPostsDTO) async {
    CollectionReference collectionReference =
        _fireStore.collection("Diary/$email/posts-diary");
    await collectionReference
        .doc(diaryPostsDTO.sendingTime.toString())
        .delete();
    if (diaryPostsDTO.postsImageUrl.isNotEmpty) {
      for (var item in diaryPostsDTO.postsImageUrl) {
        final desertRef =
            _firebaseStorage.ref().child('$email/posts-diary/${item.urlFile}');
        await desertRef.delete();
      }
    }
  }

  Future<List<PostsImage>> uploadMultiFileToFirebaseStorage(
      List<File> listFile, BuildContext context, int typeImage) async {
    List<UploadTask> listUploadTask = [];
    List<List<String>> listConvertFilePathToList = [];
    List<String> listUrlImagePathClean = [];
    List<Reference> listReference = [];
    List<TaskSnapshot> listSnapshot = [];
    List<PostsImage> listPostsImage = [];
    for (var item in listFile) {
      listConvertFilePathToList.add(item.path.split('/'));
      for (var item in item.path.split('/')) {
        if (item.contains('image')) {
          listUrlImagePathClean.add(item);
          break;
        }
      }
    }
    if (typeImage == Numeral.TYPE_UPDATE_PROFILE_IMAGE_POSTS_DIARY) {
      for (var item in listUrlImagePathClean) {
        listReference.add(
            await _firebaseStorage.ref().child('$email/posts-diary/$item'));
      }
    }

    if (kIsWeb) {
      //   uploadTask = ref.putData(await file.readAsBytes(), metadata);
    } else {
      for (int i = 0; i < listReference.length; i++) {
        listUploadTask.add(listReference[i].putFile(listFile[i]));
        listSnapshot.add(await listUploadTask[i].whenComplete(() {}));
        listPostsImage.add(PostsImage(
            urlImage: await listSnapshot[i].ref.getDownloadURL(),
            urlFile: listUrlImagePathClean[i]));
      }
    }
    return listPostsImage;
  }
}
